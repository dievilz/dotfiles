#
# $XDG_CONFIG_HOME/lf/config.sh
#


# ======== ICONS ==========
## This is the list for lf icons
## From https://github.com/mohkale/dotfiles/blob/master/core/profile
refreshLfIcons() \
{
	if [ -r "${XDG_CONFIG_HOME:-$HOME/.config}/lf/icons" ];
	then
		LF_ICONS=
		LF_ICONS=$(sed \
            -e 's/[ \t]*#.*$//'      \
            -e '/^[ \t]*#/d'         \
            -e '/^[ \t]*$/d'         \
            -e 's/[ \t]\+/=/g'       \
            -e 's/$/ /' 2> /dev/null \
			"${XDG_CONFIG_HOME:-$HOME/.config}/lf/icons")

		## Load icons from human readable config file
		LF_ICONS="$(echo "$LF_ICONS" | tr '\n' ':')"
		# LF_ICONS="${LF_ICONS//$'\n'/:}"
		export LF_ICONS
	fi
}
