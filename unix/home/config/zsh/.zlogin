#
# $ZDOTDIR/.zlogin  OR  ~/.zlogin
#


## Printing for debugging purposes if session is interactive
if \
[[ -t 1 ]] && \
[[ -o interactive ]];
then
	echo "Sourced: .zlogin"
else
	return   ## ...if not running interactively
fi

if [[ -o login ]];
then
	[[ -r "$HOME/.login" ]] && source "$HOME/.login"
fi
