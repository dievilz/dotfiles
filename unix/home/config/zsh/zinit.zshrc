#######################################
## dievilz' ZINIT RUN COMMANDS FILE  ##
#######################################


## Printing for debugging purposes if session is interactive
if \
[[ -t 1 ]] && \
[[ -o interactive ]];
then
	echo "Sourced: ===> \$ZDOTDIR/zinit.zshrc"
else
	return   ## ...if not running interactively
fi

export COMPLEMENTARY_ZSHRC_FOUND=1
export ZINIT_SOURCED=1


############################### PLUGINS & THEMES ###############################

## Path to your zinit installation.
ZINIT_HOME="${ZSH_PREFIX:?}/zinit"
[ -d "$ZINIT_HOME" ] && export ZINIT_HOME || return 1

if source "$ZINIT_HOME/zinit.zsh" 2> /dev/null;
then
	## Initial Zinit's hash definition, if configuring before loading Zinit:
	declare -A ZINIT

	        export ZINIT[BIN_DIR]="$ZINIT_HOME" #
	       export ZINIT[HOME_DIR]="${XDG_DATA_HOME:?}/zinit"
	                  export ZPFX="${ZINIT[HOME_DIR]}/polaris"
	    export ZINIT[PLUGINS_DIR]="${ZSH:?}/plugins"
	export ZINIT[COMPLETIONS_DIR]="${ZSH:?}/completions"
	   export ZINIT[SNIPPETS_DIR]="${ZSH:?}/snippets"
	 export ZINIT[ZCOMPDUMP_PATH]="${XDG_CACHE_HOME:?}/zsh/completions/zinit-zcompdump-${HOST}-${ZSH_VERSION}.zwc"
	  export ZINIT[COMPINIT_OPTS]="-C"
	  export ZINIT[MUTE_WARNINGS]=1
	export ZINIT[OPTIMIZE_OUT_DISK_ACCESSES]=

	        export ZINIT_BIN_DIR="$ZINIT_HOME" #
	       export ZINIT_HOME_DIR="${XDG_DATA_HOME:?}/zinit"
	                 export ZPFX="${ZINIT_HOME_DIR}/polaris"
	    export ZINIT_PLUGINS_DIR="${ZSH:?}/plugins"
	export ZINIT_COMPLETIONS_DIR="${ZSH:?}/completions"
	   export ZINIT_SNIPPETS_DIR="${ZSH:?}/snippets"
	 export ZINIT_ZCOMPDUMP_PATH="${XDG_CACHE_HOME:?}/zsh/completions/zinit-zcompdump-${HOST}-${ZSH_VERSION}.zwc"
	  export ZINIT_COMPINIT_OPTS="-C"
	  export ZINIT_MUTE_WARNINGS=1
	export ZINIT_OPTIMIZE_OUT_DISK_ACCESSES=


	## https://github.com/zdharma-continuum/zinit/discussions/651 ##################################
	setopt RE_MATCH_PCRE   # _fix-omz-plugin function uses this regex style

	# Workaround for zinit issue#504: remove subversion dependency. Function clones all files in plugin
	# directory (on github) that might be useful to zinit snippet directory. Should only be invoked
	# via zinit atclone"_fix-omz-plugin"
	_fix-omz-plugin() {
		if [[ ! -f ._zinit/teleid ]] then return 0; fi
		if [[ ! $(cat ._zinit/teleid) =~ "^OMZP::.*" ]] then return 0; fi
		local OMZP_NAME=$(cat ._zinit/teleid | sed -n 's/OMZP:://p')
		cd /tmp
		git clone --quiet --no-checkout --depth=1 --filter=tree:0 https://github.com/ohmyzsh/ohmyzsh
		cd ohmyzsh
		git sparse-checkout set --no-cone plugins/$OMZP_NAME
		git checkout --quiet
		cd ..
		local OMZP_PATH="ohmyzsh/plugins/$OMZP_NAME"
		local file
		for file in ohmyzsh/plugins/$OMZP_NAME/*~(.gitignore|*.plugin.zsh)(D);
		do
			local filename="${file:t}"
			echo "Copying $file to $(pwd)/$filename..."
			cp $file $filename
		done
		rm -rf ohmyzsh
	}
	## https://github.com/zdharma-continuum/zinit/discussions/651 ##################################


	### Added by Zinit's installer (if you source zinit.zsh after compinit)
		autoload -Uz _zinit
		(( ${+_comps} )) && _comps[zinit]=_zinit
	### End of Zinit installer's chunk

	## Load a few important annexes required without Turbo
	zinit light-mode compile'handler' for \
	  zdharma-continuum/zinit-annex-patch-dl \
	  zdharma-continuum/zinit-annex-readurl \
	  zdharma-continuum/zinit-annex-bin-gem-node \
	  zdharma-continuum/zinit-annex-submods \

	zinit lucid for \
	  if'command -v tmux > /dev/null 2>&1' \
	  atinit'
	   ZSH_TMUX_FIXTERM=false
	   ZSH_TMUX_AUTOSTART=false
	   ZSH_TMUX_AUTOCONNECT=false' \
	     OMZP::tmux \
	  \
	  wait'2' \
	     zdharma-continuum/declare-zsh \
	  \
	  wait'1' \
	     zdharma-continuum/zui \
	     psprint/zsh-navigation-tools \
	  \
	  wait'1' pick'z.sh' light-mode atinit'
	  [[ ! -d "${XDG_DATA_HOME:?}/z/z" ]] && mkdir -p "${XDG_DATA_HOME:?}/z"; \
	  [[ ! -r "${XDG_DATA_HOME:?}/z/z" ]] && touch "${XDG_DATA_HOME:?}/z/z"' \
	     knu/z \
	  \
	  wait'[[ -n "${ZLAST_COMMANDS[(r)cras*]}" ]]' \
	     zdharma-continuum/zplugin-crasis \
	  # \
	  # wait'1' bindmap"^R -> ^H" \
	  #    zdharma-continuum/history-search-multi-word \

	zinit wait lucid light-mode for \
	     OMZP::alias-finder \
	     OMZP::asdf \
	     OMZP::command-not-found \
	     OMZP::composer \
	     OMZP::extract \
	     OMZP::git-auto-fetch \
	  \
	  if'[[ $(uname -s) == "Darwin" ]]' \
	     OMZP::macos \
	     if'if [[ -d "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/" ]]; then [[ ! -e "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/music" ]] && curl -fSL -o "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/music" "https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/macos/music"; fi' \
	     if'if [[ -d "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/" ]]; then [[ ! -e "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/spotify" ]] && curl -fSL -o "$(find $ZINIT_SNIPPETS_DIR -maxdepth 1 -name "*macos" -type d)/spotify" "https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/macos/spotify"; fi' \
	     OMZP::pyenv \
	  atpull"%atclone" atclone"_fix-omz-plugin" \
	     OMZP::wd \
	     zsh-users/zsh-history-substring-search \
	     sei40kr/zsh-fast-alias-tips \
	  \
	  as'completion' \
	     OMZP::docker/completions/_docker \
	  \
	  from'gh-r' as'program' \
	     sei40kr/fast-alias-tips-bin \
	  \
	  atinit'ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay' \
	     zdharma-continuum/fast-syntax-highlighting \
	  \
	  atload'!_zsh_autosuggest_start' \
	     zsh-users/zsh-autosuggestions \
	  \
	  atpull'zinit creinstall -q .' blockf  \
	     zsh-users/zsh-completions \

	source "${ZLE_CUSTOM_SCRIPT:?}" custom ausg hist syhi

	# --------------------------------------------------------------------------

	case "$theme" in
		"")
			if command -v jot > /dev/null 2>&1; then
				theme=$(jot -w %i -r 1 1 7+1)  # NumOfResults, Min, (Max+1)
			fi
		;;
	esac

	case "$theme" \
	in
		"1") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel9k zinit ;;
		"2") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel10k zinit ;;
		"3") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --spaceship zinit ;;
		"4") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --geometry zinit ;;
		"5") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --hyperzsh zinit ;;
		"6") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --pygmalion local ;;
		"7") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --dievilz local ;;
		# *) source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --random zinit ;;
	esac
	unset theme

	# echo "ZTHEME_FOUND is: $ZTHEME_FOUND"
	# echo "ZSH_THEME is: $ZSH_THEME"

	if [[ $ZTHEME_FOUND -eq 0 ]];
	then
		echo "===> zinit.zshrc: Loading default Zsh theme"
		case "$default_theme" \
		in
			"1") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel9k omz ;;
			"2") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel10k omz ;;
			"3") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --spaceship omz ;;
			"4") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --geometry omz ;;
			"5") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --hyperzsh omz ;;
			"6") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --pygmalion local ;;
			"7") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --dievilz local ;;
			# *) ZSH_THEME=random ;;
		esac
		unset default_theme

		[[ -n $default_theme_custom ]] && custom=$default_theme_custom \
		&& unset default_theme_custom
	fi

	## Sourcing my custom Themes settings
	if [[ -n $custom ]] && [[ -n "$ZSH_THEME" ]]; then
		source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --"$ZSH_THEME" custom "$custom" && unset custom
	fi

	################### SYNTAX HIGHLIGHTING; Should be last ####################

	## Zinit already loaded these plugins: source "$ZLE_CUSTOM_SCRIPT" "zsh-users" "zsh-users" "zdharma"

	############################################################################
else
	echo "===> zinit.zshrc: zinit folder not found!"
fi

################################# END OF FILE ##################################
