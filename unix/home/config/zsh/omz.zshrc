#############################################
## dievilz' DEFAULT ZSH RUN COMMANDS FILE  ##
#############################################


## Printing for debugging purposes if session is interactive
if \
[[ -t 1 ]] && \
[[ -o interactive ]];
then
	echo "Sourced: ===> \$ZDOTDIR/omz.zshrc"
else
	return   ## ...if not running interactively
fi

export COMPLEMENTARY_ZSHRC_FOUND=1
export OMZ_SOURCED=1


############################# SET PLUGINS & THEMES #############################

## Which plugins would you like to load?
## Add wisely, as too many plugins slow down shell startup:
case "$(uname -s)" \
in
	"Darwin") plugins=(osx)
	;;
	"Linux")
		case "$LINUX" in
			"Arch"|"ArchLinux") plugins=(archlinux) ;;
			"Void"|"VoidLinux") plugins=(voidlinux) ;;
		esac
	;;
esac

plugins+=(
	alias-finder docker extract wd z fast-alias-tips
)
## Possible plugins: command-not-found dirhistory tiny-care-terminal
## web-search


# ------------------------------------------------------------------------------

case "$theme" in
	"")
		if command -v jot > /dev/null 2>&1; then
			theme=$(jot -w %i -r 1 1 7+1)  # NumOfResults, Min, (Max+1)
		fi
	;;
esac

case "$theme" \
in
	"1") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel9k zinit ;;
	"2") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel10k zinit ;;
	"3") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --spaceship zinit ;;
	"4") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --geometry zinit ;;
	"5") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --hyperzsh zinit ;;
	"6") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --pygmalion local ;;
	"7") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --dievilz local ;;
	# *) source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --random zinit ;;
esac
unset theme



################## COMPILATION OF {COMPLETION,PLUGINS,THEMES} ##################

## Sourcing my custom Configurator   OR   Oh My Zsh

if [[ -r "$ZSH/configure.zsh" ]];
then
	source "$ZSH/configure.zsh"

## Path to your "oh-my-zsh" installation.
elif [[ -r "$ZSH_PREFIX/oh-my-zsh/oh-my-zsh.sh" ]];
then
	## Uncomment one of the following lines to change the auto-update behavior
	# zstyle ':omz:update' mode disabled  # disable automatic updates
	# zstyle ':omz:update' mode reminder  # just remind me to update when it's time
	zstyle ':omz:update' mode auto        # update automatically without asking

	## Uncomment the following line to change how often to auto-update (in days).
	zstyle ':omz:update' frequency 10

	## Sourcing Oh My Zsh ##################################################
	source "${OMZ:=$ZSH_PREFIX/oh-my-zsh}/oh-my-zsh.sh"

	## Unsetopt Oh My Zsh predefined options
	unsetopt SHARE_HISTORY   ## Imports new lines from $HISTFILE, and append typed lines to $HISTFILE

	### Unalias Oh My Zsh predefined aliases
	## Inside /lib/directories.zsh
	unalias 1 &> /dev/null   # ='cd -1'
	unalias 2 &> /dev/null   # ='cd -2'
	unalias 3 &> /dev/null   # ='cd -3'
	unalias 4 &> /dev/null   # ='cd -4'
	unalias 5 &> /dev/null   # ='cd -5'
	unalias 6 &> /dev/null   # ='cd -6'
	unalias 7 &> /dev/null   # ='cd -7'
	unalias 8 &> /dev/null   # ='cd -8'
	unalias 9 &> /dev/null   # ='cd -9'
	unalias md &> /dev/null  # ='mkdir -p'
	unalias rd &> /dev/null  # =rmdir
	unalias lsa &> /dev/null # ='ls -lah'
	unalias l &> /dev/null   # ='ls -lah'
	unalias ll &> /dev/null  # ='ls -lh'
	unalias la &> /dev/null  # ='ls -lAh'

	## Inside /lib/correction.zsh
	unalias cp &> /dev/null
	unalias ebuild &> /dev/null
	unalias gist &> /dev/null
	unalias heroku &> /dev/null
	unalias man &> /dev/null
	unalias mkdir &> /dev/null
	unalias mv &> /dev/null
	unalias mysql &> /dev/null
	unalias sudo &> /dev/null
	unalias su &> /dev/null
fi

########################## THEMES (CUSTOM SETTINGS) ############################

echo "ZTHEME_FOUND is: $ZTHEME_FOUND"
echo "ZSH_THEME is: $ZSH_THEME"

if [[ $ZTHEME_FOUND -eq 0 ]];
then
	echo "===> omz.zshrc: Loading default Zsh theme"
	case "$default_theme" \
	in
		"1") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel9k omz ;;
		"2") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --powerlevel10k omz ;;
		"3") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --spaceship omz ;;
		"4") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --geometry omz ;;
		"5") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --hyperzsh omz ;;
		"6") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --pygmalion local ;;
		"7") source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --dievilz local ;;
		# *) ZSH_THEME=random ;;
	esac
	unset default_theme

	[[ -n $default_theme_custom ]] && custom=$default_theme_custom \
	&& unset default_theme_custom
fi

## Sourcing my custom Themes settings
if [[ -n $custom ]] && [[ -n "$ZSH_THEME" ]]; then
	source "${ZSH_THEMES_CUSTOM_SCRIPT:?}" --"$ZSH_THEME" custom "$custom" && unset custom
fi

##################### SYNTAX HIGHLIGHTING; Should be last ######################

source "${ZLE_CUSTOM_SCRIPT:?}" "zsh-users" "zsh-users" "zdharma"

source "${ZLE_CUSTOM_SCRIPT:?}" custom ausg hist syhi

############################# LAST CONFIGURATIONS ##############################

################################# END OF FILE ##################################
