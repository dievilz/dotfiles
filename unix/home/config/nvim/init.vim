"###############################################################################
"
" ~/.config/nvim/init.vim
"
" @dievilz' Nvim init file
"
" Source main vimrc config file + source Nvim plugins
"
"###############################################################################

scriptencoding utf-8


if argc() == 0
	" autocmd VimEnter * echo "Nr. of Colors: " . &t_Co
	autocmd VimEnter * echo "Sourced: init.vim"
else
	echo "Sourced: init.vim"
end

" ================================ SETTINGS ====================================

if filereadable(expand('~/.config/vim/vimrc'))
	source ~/.config/vim/vimrc

elseif filereadable(expand('~/.vim/vimrc'))
	source ~/.vim/vimrc

elseif filereadable(expand('~/.vimrc'))
	source ~/.vimrc
endif


" ================================ PLUGINS =====================================

if filereadable(expand('~/.config/nvim/plogins.vim'))
	source ~/.config/nvim/plogins.vim

elseif filereadable(expand('~/.nvim/plogins.vim'))
	source ~/.nvim/plogins.vim

elseif filereadable(expand('~/.plogins.vim'))
	source ~/.plogins.vim
endif

" ============================== END OF FILE ===================================
