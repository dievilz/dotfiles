#
# $XDG_CONFIG_HOME/bat/config.sh
#

# This is <bat> configuration file. Each line either contains a comment or
# a command-line option that you want to pass to <bat> by default. You can
# run <bat --help> to get a list of all possible configuration options.

## Force colorized output:
--color=always

## Enable this to use italic text on the terminal. This is not
## supported on all terminal emulators (like tmux, by default):
--italic-text=always

## Indentation: <bat> expands tabs to 4 spaces by itself, ignoring the pager.
--tabs=0  ## Use 0 to let tabs be consumed by the pager

## By default, <bat> pipes its own output to a pager (i.e. less) if the output
## is too large for one screen. If you would rather <bat> work like <cat> all
## the time, uncomment the following line to disable automatic paging: always/never
# --paging=never

## Uncomment the following line if you are using less version <= 551 and
## want to enable mouse scrolling support in <bat> when running inside
## tmux. This might disable text selection, unless you press shift:
# --pager="less --R --quit-if-one-screen --mouse" Default

## You can use the option to show and highlight non-printable characters.
## NOTE This option disables syntax highlighting:
## (https://github.com/sharkdp/bat/issues/414#issuecomment-438718247)
# --show-all

## Show file <header>, nice <grid>, line <numbers>, Git <changes>:
--style="changes"

## Specify desired highlighting theme (e.g. "TwoDark"). Run
## <bat --list-themes> for a list of all available themes.
--theme="Monokai Extended"

## Syntax mappings: map a certain filename pattern to a language.
##   Example 1: use the C++ syntax for Arduino .ino files
##   Example 2: Use ".gitignore"-style highlighting for ".ignore" files
--map-syntax "*.ino:C++"
--map-syntax ".ignore:Git Ignore"
--map-syntax ".aliases:Bourne Again Shell (bash)"
--map-syntax ".functions:Bourne Again Shell (bash)"


## ActionScript:as
## Apache Conf:envvars,htaccess,HTACCESS,htgroups,HTGROUPS,htpasswd,HTPASSWD,.htaccess,.HTACCESS,.htgroups,.HTGROUPS,.htpasswd,.HTPASSWD,/etc/apache2/**/*.conf,/etc/apache2/sites-*/**/*,httpd.conf
## AppleScript:applescript,script editor
## ARM Assembly:s,S
## AsciiDoc (Asciidoctor):adoc,ad,asciidoc
## ASP:asa
## Assembly (x86_64):yasm,nasm,asm,inc,mac
## Authorized Keys:authorized_keys,pub,authorized_keys2
## AWK:awk
## Batch File:bat,cmd
## BibTeX:bib
## Bourne Again Shell (bash):sh,bash,zsh,ash,.bash_aliases,.bash_completions,.bash_functions,.bash_login,.bash_logout,.bash_profile,.bash_variables,.bashrc,.profile,.textmate_init,.zlogin,.zlogout,.zprofile,.zshenv,.zshrc,PKGBUILD,ebuild,eclass,**/bat/config,/etc/profile
## C:c
## C#:cs,csx
## C++:cpp,cc,cp,cxx,c++,h,hh,hpp,hxx,h++,inl,ipp,*.h
## Cabal:cabal
## Clojure:clj,cljc,cljs,edn
## CMake:CMakeLists.txt,cmake
## CMake C Header:h.in
## CMake C++ Header:hh.in,hpp.in,hxx.in,h++.in
## CMakeCache:CMakeCache.txt
## CoffeeScript:coffee,Cakefile,coffee.erb,cson
## Comma Separated Values:csv,tsv
## Command Help:cmd-help,help
## CpuInfo:cpuinfo
## Crystal:cr
## CSS:css,css.erb,css.liquid
## D:d,di
## Dart:dart
## Diff:diff,patch
## Dockerfile:Dockerfile,dockerfile
## DotENV:.env,.env.dist,.env.local,.env.sample,.env.example,.env.test,.env.test.local,.env.testing,.env.dev,.env.development,.env.development.local,.env.prod,.env.production,.env.production.local,.env.dusk.local,.env.staging,.env.default,.env.defaults,.envrc,.flaskenv
## Elixir:ex,exs
## Elm:elm
## Email:eml,msg,mbx,mboxz,/var/spool/mail/*,/var/mail/*
## Erlang:erl,hrl,Emakefile,emakefile,escript
## F#:fs,fsi,fsx,*.fs
## Fish:fish
## Fortran (Fixed Form):f,F,f77,F77,for,FOR,fpp,FPP
## Fortran (Modern):f90,F90,f95,F95,f03,F03,f08,F08
## Fortran Namelist:namelist
## fstab:fstab,crypttab,mtab
## Git Attributes:attributes,gitattributes,.gitattributes,/Users/dievilz/.etc/git/attributes,/Users/dievilz/.config/git/attributes
## Git Commit:COMMIT_EDITMSG,MERGE_MSG,TAG_EDITMSG
## Git Config:gitconfig,.gitconfig,.gitmodules,/Users/dievilz/.etc/git/config,/Users/dievilz/.config/git/config
## Git Ignore:exclude,gitignore,.gitignore,/Users/dievilz/.etc/git/ignore,/Users/dievilz/.config/git/ignore
## Git Link:.git
## Git Log:gitlog
## Git Mailmap:.mailmap,mailmap
## Git Rebase Todo:git-rebase-todo
## GLSL:vs,gs,vsh,fsh,gsh,vshader,fshader,gshader,vert,frag,geom,tesc,tese,comp,glsl,mesh,task,rgen,rint,rahit,rchit,rmiss,rcall
## gnuplot:gp,gpl,gnuplot,gnu,plot,plt
## Go:go
## GraphQL:graphql,graphqls,gql
## Graphviz (DOT):dot,DOT,gv
## Groff/troff:groff,troff,1,2,3,4,5,6,7,8,9
## Groovy:groovy,gvy,gradle,Jenkinsfile
## group:group
## Haskell:hs
## Highlight non-printables:show-nonprintable
## hosts:hosts
## HTML:html,htm,shtml,xhtml
## HTML (ASP):asp
## HTML (EEx):html.eex,html.leex
## HTML (Erlang):yaws
## HTML (Jinja2):htm.j2,html.j2,xhtml.j2,xml.j2
## HTML (Rails):rails,rhtml,erb,html.erb
## HTML (Tcl):adp
## HTML (Twig):twig,html.twig
## HTTP Request and Response:http
## INI:ini,INI,inf,INF,reg,REG,lng,cfg,CFG,desktop,url,URL,.editorconfig,.hgrc,hgrc,**/systemd/**/*.conf,**/systemd/**/*.example,*.automount,*.device,*.dnssd,*.link,*.mount,*.netdev,*.network,*.nspawn,*.path,*.service,*.scope,*.slice,*.socket,*.swap,*.target,*.timer,*.hook
## Java:java,bsh
## Java Properties:properties
## Java Server Page (JSP):jsp
## JavaScript:htc
## JavaScript (Babel):js,mjs,jsx,babel,es6,cjs,*.pac
## JavaScript (Rails):js.erb
## Jinja2:j2,jinja2,jinja
## JQ:jq
## JSON:json,sublime-settings,sublime-menu,sublime-keymap,sublime-mousemap,sublime-theme,sublime-build,sublime-project,sublime-completions,sublime-commands,sublime-macro,sublime-color-scheme,ipynb,Pipfile.lock
## jsonnet:jsonnet,libsonnet,libjsonnet
## Julia:jl
## Known Hosts:known_hosts,known_hosts.old
## Kotlin:kt,kts
## LaTeX:tex,ltx
## Lean:lean
## Less:less,css.less
## Lisp:lisp,cl,clisp,l,mud,el,scm,ss,lsp,fasl
## Literate Haskell:lhs
## LiveScript:ls,Slakefile,ls.erb
## LLVM:ll
## log:log
## Lua:lua
## Makefile:make,GNUmakefile,makefile,Makefile,makefile.am,Makefile.am,makefile.in,Makefile.in,OCamlMakefile,mak,mk
## Manpage:man
## Markdown:md,mdown,markdown,markdn
## MATLAB:matlab
## MediaWiki:mediawiki,wikipedia,wiki
## MemInfo:meminfo
## NAnt Build File:build
## nginx:conf.erb,nginx.conf,mime.types,fastcgi_params,scgi_params,uwsgi_params,/etc/nginx/**/*.conf,/etc/nginx/sites-*/**/*,nginx.conf,mime.types
## Nim:nim,nims,nimble
## Ninja:ninja
## Nix:nix
## Objective-C:m
## Objective-C++:mm
## OCaml:ml,mli
## OCamllex:mll
## OCamlyacc:mly
## orgmode:org
## Pascal:pas,p,dpr
## passwd:passwd
## Perl:pl,pc,pm,pmc,pod,t
## PHP:php,php3,php4,php5,php7,phps,phpt,phtml
## Plain Text:txt
## PowerShell:ps1,psm1,psd1
## Protocol Buffer:proto,protodevel
## Protocol Buffer (TEXT):pb.txt,proto.text,textpb,pbtxt,prototxt
## Puppet:pp,epp
## PureScript:purs
## Python:py,py3,pyw,pyi,pyx,pyx.in,pxd,pxd.in,pxi,pxi.in,rpy,cpy,SConstruct,Sconstruct,sconstruct,SConscript,gyp,gypi,Snakefile,vpy,wscript,bazel,bzl
## QML:qml,qmlproject
## R:R,r,Rprofile
## Racket:rkt
## Rd (R Documentation):rd
## Rego:rego
## Regular Expression:re
## resolv:resolv.conf
## reStructuredText:rst,rest
## Robot Framework:robot,resource
## Ruby:rb,Appfile,Appraisals,Berksfile,Brewfile,capfile,cgi,Cheffile,config.ru,Deliverfile,Fastfile,fcgi,Gemfile,gemspec,Guardfile,irbrc,jbuilder,Podfile,podspec,prawn,rabl,rake,Rakefile,Rantfile,rbx,rjs,ruby.rail,Scanfile,simplecov,Snapfile,thor,Thorfile,Vagrantfile
## Ruby Haml:haml
## Ruby on Rails:rxml,builder
## Ruby Slim:slim,skim
## Rust:rs
## Salt State (SLS):sls
## Sass:sass
## Scala:scala,sbt,sc
## SCSS:scss
## SML:sml,cm,sig
## Solidity:sol
## SQL:sql,ddl,dml
## SQL (Rails):erbsql,sql.erb
## SSH Config:ssh_config,**/.ssh/config
## SSHD Config:sshd_config
## Strace:strace
## Stylus:styl,stylus
## Svelte:svlt,svelte
## Swift:swift
## syslog:syslog
## SystemVerilog:sv,svh,vh
## Tcl:tcl
## Terraform:tf,tfvars,hcl
## TeX:sty,cls
## Textile:textile
## TOML:toml,tml,Cargo.lock,Gopkg.lock,Pipfile,poetry.lock
## TypeScript:ts
## TypeScriptReact:tsx
## varlink:varlink
## Verilog:v,V
## VimL:vim,vimrc,gvimrc,.vimrc,.gvimrc,_vimrc,_gvimrc
## Vue Component:vue
## Vyper:vy
## XML:xml,xsd,xslt,tld,dtml,rng,rss,opml,svg,xaml
## YAML:yaml,yml,sublime-syntax,.clang-format
## Zig:zig
