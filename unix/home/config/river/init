#!/bin/sh
#
# $XDG_CONFIG_HOME/river/init
#
# See the river(1), riverctl(1), and rivertile(1) man pages for complete
# documentation.


############################################## SPAWNS ##############################################

riverctl keyboard-layout es

riverctl spawn "yambar"

riverctl spawn "wl-paste --watch cliphist store"

[[ -e $HOME/Pictures/Wallpapers/rotation/* ]] \
&& riverctl spawn "command -v swaybg > /dev/null 2>&1 && swaybg -m fill -i $HOME/Pictures/Wallpapers/rotation/lp"
# && riverctl spawn "command -v waybg > /dev/null 2>&1 && waybg  -m fill -R $HOME/Pictures/Wallpapers/rotation"


########################################### KEYBINDINGS ############################################

## Note: the "Super" modifier is also known as Logo, GUI, Windows, Mod4, etc.

## Super+Shift+Return to start an instance of foot (https://codeberg.org/dnkl/foot)
# riverctl map normal Mod4+Shift Return spawn foot

riverctl map normal Mod4 Space spawn "wofi --show run"

## Super+W to close the focused view
# riverctl map normal Super Q close
riverctl map normal Mod4+Shift W close

## Super+Shift+Q to exit river
# riverctl map normal Super+Shift E exit
riverctl map normal Mod4+Shift Q exit

## Super+F to toggle fullscreen
riverctl map normal Mod4 F toggle-fullscreen

## Super+J and Super+K to   FOCUS   the next/previous view in the layout stack
riverctl map normal Mod4 Tab focus-view next
riverctl map normal Mod4+Shift Tab focus-view previous

## Super+Period and Super+Comma to   FOCUS   the next/previous output
riverctl map normal Mod4 Period focus-output next
riverctl map normal Mod4+Shift Comma focus-output previous


############################ TILING MODE (DEFAULT) #############################

## Super+{Up,Right,Down,Left} to change layout orientation
riverctl map normal Mod4+Alt Up send-layout-cmd rivertile "main-location top"
riverctl map normal Mod4+Alt Down send-layout-cmd rivertile "main-location bottom"
riverctl map normal Mod4+Alt Right send-layout-cmd rivertile "main-location right"
riverctl map normal Mod4+Alt Left send-layout-cmd rivertile "main-location left"

## Super+H and Super+L to decrease/increase the ratio of the FOCUSED VIEW in rivertile(1)
riverctl map normal Mod4 H send-layout-cmd rivertile "main-ratio -0.05"
riverctl map normal Mod4 L send-layout-cmd rivertile "main-ratio +0.05"

## Super+Shift+H and Super+Shift+L to increment/decrement the view count of the FOCUSED VIEW in rivertile(1)
riverctl map normal Mod4+Shift Asterisk send-layout-cmd rivertile "main-count +1"
riverctl map normal Mod4+Shift Minus send-layout-cmd rivertile "main-count -1"

## Super+Shift+J and Super+Shift+K to    SWAP    the FOCUSED VIEW
## view with the next/previous view in the layout stack
riverctl map normal Mod4+Alt Tab swap next
riverctl map normal Mod4+Alt+Shift Tab swap previous

## Super+Return to bump the FOCUSED VIEW to the top of the layout stack
riverctl map normal Mod4 Return zoom

## Super+Shift+{Period,Comma} to send the FOCUSED VIEW to the next/previous output
riverctl map normal Mod4+Shift Colon send-to-output next
riverctl map normal Mod4+Shift Semicolon send-to-output previous


################################ FLOATING MODE #################################

## Super+Space to toggle float
riverctl map normal Mod4+Shift Space toggle-float

## Super+Alt+{H,J,K,L} to move views
riverctl map normal Mod4+Alt H move left 100
riverctl map normal Mod4+Alt J move down 100
riverctl map normal Mod4+Alt K move up 100
riverctl map normal Mod4+Alt L move right 100

## Super+Alt+Control+{H,J,K,L} to snap views to screen edges
riverctl map normal Mod4+Alt+Shift Up snap up
riverctl map normal Mod4+Alt+Shift Down snap down
riverctl map normal Mod4+Alt+Shift Left snap left
riverctl map normal Mod4+Alt+Shift Right snap right

## Super+Alt+Shift+{H,J,K,L} to resize views
riverctl map normal Mod4+Alt+Shift H resize horizontal -100
riverctl map normal Mod4+Alt+Shift J resize vertical 100
riverctl map normal Mod4+Alt+Shift K resize vertical -100
riverctl map normal Mod4+Alt+Shift L resize horizontal 100

################################################################################

## Super + Left Mouse Button to move views
riverctl map-pointer normal Mod4 BTN_LEFT move-view

## Super + Right Mouse Button to resize views
riverctl map-pointer normal Mod4 BTN_RIGHT resize-view

## Super + Middle Mouse Button to toggle float
riverctl map-pointer normal Mod4 BTN_MIDDLE toggle-float

for i in $(seq 1 9);
do
    tags=$((1 << ($i - 1)))

    ## Super+[1-9] to focus tag [0-8]
    riverctl map normal Control $i set-focused-tags $tags

    # ## Super+Shift+[1-9] to tag focused view with tag [0-8], i.e: move view to specified tag
    # riverctl map normal Control+Shift $i set-view-tags $tags

    # ## Super+Ctrl+[1-9] to toggle focus of tag [0-8]
    # riverctl map normal Mod4+Control $i toggle-focused-tags $tags

    # ## Super+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
    # riverctl map normal Mod4+Shift+Control $i toggle-view-tags $tags
done

# ## Super+0 to focus all tags
# ## Super+Shift+0 to tag focused view with all tags
# all_tags=$(((1 << 32) - 1))
# riverctl map normal Mod4 0 set-focused-tags $all_tags
# riverctl map normal Mod4+Shift 0 set-view-tags $all_tags

## Declare a passthrough mode. This mode has only a single mapping to return to
## normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

## Super+F11 to enter passthrough mode
riverctl map normal Mod4 F11 enter-mode passthrough

## Super+F11 to return to normal mode
riverctl map passthrough Mod4 F11 enter-mode normal

## Various media key mapping examples for both normal and locked mode which do
## not have a modifier
for mode in normal locked
do
    ## Eject the optical drive (well if you still have one that is)
    riverctl map $mode None XF86Eject spawn 'eject -T'

    ## Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'pamixer -i 5'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'pamixer -d 5'
    riverctl map $mode None XF86AudioMute         spawn 'pamixer --toggle-mute'

    ## Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
    riverctl map $mode None XF86AudioMedia spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPlay  spawn 'playerctl play-pause'
    riverctl map $mode None XF86AudioPrev  spawn 'playerctl previous'
    riverctl map $mode None XF86AudioNext  spawn 'playerctl next'

    ## Control screen backlight brightness with light (https://github.com/haikarainen/light)
    riverctl map $mode None XF86MonBrightnessUp   spawn 'light -A 5'
    riverctl map $mode None XF86MonBrightnessDown spawn 'light -U 5'
done

## Set background and border color
riverctl background-color 0x002b36
riverctl border-color-focused 0x93a1a1
riverctl border-color-unfocused 0x586e75

## Set keyboard repeat rate
riverctl set-repeat 50 300

## Make certain views start floating
riverctl float-filter-add app-id float
riverctl float-filter-add title "popup title with spaces"

## Set app-ids and titles of views which should use client side decorations
riverctl csd-filter-add app-id "gedit"

## Set the default layout generator to be rivertile and start it.
## River will send the process group of the init executable SIGTERM on exit.
riverctl default-layout rivertile
rivertile -view-padding 6 -outer-padding 6 &
