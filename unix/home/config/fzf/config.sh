#
# $XDG_CONFIG_HOME/fzf/fzf.plugin.sh
#

if [ -z "$FZF_DEFAULT_COMMAND" ];
then
	## You can use fd, ripgrep, or ag instead of the default find
	## command to traverse the file system while respecting .gitignore.
	if command -v fd > /dev/null 2>&1;
	then
		export FZF_DEFAULT_COMMAND_FD='fd --type f --hidden --follow --strip-cwd-prefix -E .git -E .cache -E Library -E Trash'
		export FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND_FD"

	elif command -v rg > /dev/null 2>&1;
	then
		export FZF_DEFAULT_COMMAND_RG='rg --files --hidden --follow --no-ignore-vcs --glob !{.git,.cache,Library,Trash}'
		export FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND_RG"

	elif command -v ag > /dev/null 2>&1;
	then
		export FZF_DEFAULT_COMMAND_AG='ag -l --hidden -g "" --ignore .git --ignore .cache --ignore Library --ignore Trash'
		export FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND_AG"

	elif command -v ack > /dev/null 2>&1;
	then
		export FZF_DEFAULT_COMMAND_ACK='ack -l --hidden -g "" --ignore .git --ignore .cache --ignore Library --ignore Trash'
		export FZF_DEFAULT_COMMAND="$FZF_DEFAULT_COMMAND_ACK"
	fi

	## To apply the command to CTRL-T as well
	export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
fi


alias fzfFd="export FZF_DEFAULT_COMMAND=$FZF_DEFAULT_COMMAND_FD; export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND"
alias fzfRg="export FZF_DEFAULT_COMMAND=$FZF_DEFAULT_COMMAND_RG; export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND"
alias fzfAg="export FZF_DEFAULT_COMMAND=$FZF_DEFAULT_COMMAND_AG; export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND"
alias fzfAck="export FZF_DEFAULT_COMMAND=$FZF_DEFAULT_COMMAND_ACK; export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND"


FZF_DEFAULT_OPTS="  --extended
  --exact
  --sort
  --no-height
  --no-reverse

  --bind ctrl-h:backward-char
  --bind ctrl-j:down
  --bind ctrl-k:up
  --bind ctrl-l:forward-char

  --bind ctrl-i:toggle-preview
  --bind alt-i:toggle-preview

  --bind ctrl-u:page-up
  --bind ctrl-p:preview-up
  --bind alt-u:preview-page-up

  --bind ctrl-d:page-down
  --bind ctrl-n:preview-down
  --bind alt-d:preview-page-down

  --bind shift-left:backward-word
  --bind shift-right:forward-word
  --bind ctrl-b:backward-word
  --bind ctrl-f:forward-word

  --bind alt-return:print-query
  --bind alt-bspace:clear-query

  --bind alt-j:accept
  --bind alt-k:accept-non-empty
  --bind alt-q:jump
  --bind ctrl-q:jump-accept

  --bind alt-a:select-all
  --bind alt-x:deselect-all

  --bind ctrl-s:toggle-search
  --bind ctrl-space:toggle+down
  --bind ctrl-o:top

  --history "$XDG_STATE_HOME"/fzf/history/default
  --history-size 1000000
  --bind alt-j:next-history
  --bind alt-k:previous-history

  --cycle
"

## Monokai Colors --------------------------------------------------------------
  # --color fg:223,bg:235,hl:208,fg+:229,bg+:237,hl+:167,border:237
  # --color info:246,prompt:214,pointer:214,marker:142,spinner:246,header:214

## Gruvbox Colors --------------------------------------------------------------
  # --color=bg+:#3c3836,bg:#32302f,spinner:#fb4934,hl:#928374,fg:#ebdbb2,
  # --color=header:#928374,info:#8ec07c,pointer:#fb4934,marker:#fb4934,fg+:#ebdbb2,prompt:#fb4934,hl+:#fb4934
# ---------------- -------------------------------------------- ----------------

if command -v bat > /dev/null 2>&1;
then
	FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS""  --bind 'f1:execute(bat {})'"
	FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS""  --bind 'f1:execute(bat {})'"
	FZF_CTRL_T_OPTS="--preview '(bat --style=numbers --color=always --line-range :500 {})' \
	  --preview-window 'up,50%,border-bottom,+{2}+3/3,~3'"

elif command -v less > /dev/null 2>&1;
then
	FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS""  --bind 'f1:execute(less {})'"
	FZF_CTRL_T_OPTS="--preview '(cat {})' --preview-window 'up,60%,border-bottom,+{2}+3/3,~3'"
fi

export FZF_CTRL_T_OPTS

case $(uname -s) \
in
	"Darwin")
		FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS""
  --bind 'ctrl-y:execute-silent(echo {} | pbcopy)'
		"
	;;
	"Linux")
		FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS""
  --bind 'ctrl-y:execute-silent(echo {} | clip)'
		"
	;;
esac

export FZF_DEFAULT_OPTS

# ---------------- -------------------------------------------- ----------------

export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3"

# ---------------- -------------------------------------------- ----------------



# # 1. Search for text in files using Ripgrep
# # 2. Interactively restart Ripgrep with reload action
# # 3. Open the file in Vim
# RG_PREFIX="rg --hidden --follow --column --line-number --no-heading --smart-case \
#            --glob '!{.git,.cache,Library,Trash}' "
# INITIAL_QUERY="${*:-}"
# IFS=: read -ra selected < <(
#   FZF_DEFAULT_COMMAND="$RG_PREFIX $(printf %q "$INITIAL_QUERY")" \
#   fzf --ansi \
#       --disabled --query "$INITIAL_QUERY" \
#       --bind "change:reload:sleep 0.1; $RG_PREFIX {q} || true" \
#       --delimiter : \
#       --preview 'bat --color=always {1} --highlight-line {2}' \
#       --preview-window 'up,50%,border-bottom,+{2}+3/3,~3'
# )
# [ -n "${selected[0]}" ] && vim "${selected[0]}" "+${selected[1]}"



# # Switch between Ripgrep launcher mode (CTRL-R) and fzf filtering mode (CTRL-F)
# RG_PREFIX="rg --hidden --follow --column --line-number --no-heading --smart-case \
#            --glob '!{.git,.cache,Library,Trash}' "
# INITIAL_QUERY="${*:-}"
# IFS=: read -ra selected < <(
#   FZF_DEFAULT_COMMAND="$RG_PREFIX $(printf %q "$INITIAL_QUERY")" \
#   fzf --ansi \
#       --color "hl:-1:underline,hl+:-1:underline:reverse" \
#       --disabled --query "$INITIAL_QUERY" \
#       --bind "change:reload:sleep 0.1; $RG_PREFIX {q} || true" \
#       --bind "ctrl-f:unbind(change,ctrl-f)+change-prompt(2. fzf> )+enable-search+clear-query+rebind(ctrl-r)" \
#       --bind "ctrl-r:unbind(ctrl-r)+change-prompt(1. ripgrep> )+disable-search+reload($RG_PREFIX {q} || true)+rebind(change,ctrl-f)" \
#       --prompt '1. Ripgrep> ' \
#       --delimiter : \
#       --header '╱ CTRL-R (Ripgrep mode) ╱ CTRL-F (fzf mode) ╱' \
#       --preview 'bat --color=always {1} --highlight-line {2}' \
#       --preview-window 'up,50%,border-bottom,+{2}+3/3,~3'
# )
# [ -n "${selected[0]}" ] && vim "${selected[0]}" "+${selected[1]}"
