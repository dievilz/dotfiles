#
# /opt/shell/plugins/asdf.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: asdf"
# echo "Sourced: /opt/shell/plugins/asdf.plugin.sh"


## Find where asdf should be installed
if command -v fd > /dev/null 2>&1;
then
	ASDF_LOCATION="$(fd --full-path "asdf-vm/asdf.sh" "/" --type f \
		-E "bin" -E "boot" -E "cdrom" -E "dev" -E "etc" -E "lib*" \
		-E "lost+found" -E "media" -E "mnt" -E "proc" -E "root" \
		-E "run" -E "sbin" -E "snap" -E "srv" -E "sys" -E "tmp"\
		-E "var" -E "www" \
		-E ".vol" -E "cores" -E "private" -E "Applications" \
		-E "Library" -E "System" -E "Volumes" \
		-E ".dotfiles" -E ".config" -E ".cache" -E ".run" -E ".state" -x echo {//})"
else
	printf "%b" "\033[38;31masdf.plugin.zsh: error: fd not found!\033[0m\n" \
	"Cannot begin to look for the asdf-vm dir without a way to filter the " \
	"folders where it should not be at. Please download the fd program to " \
	"be able to use this plugin. Exiting...\n"
fi

if [ -z "$ASDF_LOCATION" ];
then
	printf "%b" "\033[38;31masdf.plugin.zsh: error: asdf-vm folder not" \
	"found!\033[0m\nExiting...\n"
fi

################################################################################

{
	[ -r "$ASDF_LOCATION/asdf.sh" ] && \
	ASDF_DIR="$ASDF_LOCATION"
} 2> /dev/null
{
	[ -d "$ASDF_LOCATION/completions" ] && \
	ASDF_COMPLETIONS="$ASDF_LOCATION/completions"
} 2> /dev/null

if brew --prefix asdf > /dev/null 2>&1;
then
	{
		[ -r "$(brew --prefix asdf)/libexec/asdf.sh" ] && \
		ASDF_DIR="$(brew --prefix asdf)"
	} 2> /dev/null
	{
		[ -d "$(brew --prefix asdf)/completions" ] && \
		ASDF_COMPLETIONS="$(brew --prefix asdf)/completions"
	} 2> /dev/null
fi

################################################################################

if [ -z "$ASDF_DIR" ];
then
	printf "%b" "\033[38;31masdf.plugin.zsh: error: asdf-vm folder not" \
	"found!\033[0m\nExiting...\n"
fi

if [ -z "$ASDF_COMPLETIONS" ];
then
	printf "%b" "\033[38;31masdf.plugin.zsh: error: asdf completions " \
	"folder not found!\033[0m\nExiting...\n"
fi

## Load command
if [ -r "$ASDF_DIR/asdf.sh" ];
then
	## Load completions
	case "$(ps -p $$ | grep -Eo -m 1 '\b\w{0,6}sh|koi')" \
	in
		"zsh")
			source "$ASDF_DIR/asdf.sh" # && printf "%b\033[0m" "\033[38;32masdf.plugin.zsh: asdf.sh sourced!\n"
			[[ -o interactive ]] && fpath=(${ASDF_DIR}/completions $fpath) 2> /dev/null
		;;
		"bash")
			source "$ASDF_DIR/asdf.sh" # && printf "%b\033[0m" "\033[38;32masdf.plugin.zsh: asdf.sh sourced!\n"
			[[ "$-" == *i* ]] && source "$ASDF_COMPLETIONS/asdf.bash" 2> /dev/null
		;;
		*"sh")
			. "$ASDF_DIR/asdf.sh"
		;;
	esac
else
	printf "%b" "\033[38;31masdf.plugin.zsh: error: asdf.sh not found!\n" \
	"Exiting...\033[0m\n"
fi
