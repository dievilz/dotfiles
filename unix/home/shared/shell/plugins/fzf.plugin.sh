#
# /opt/shell/plugins/fzf.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: fzf"
# echo "Sourced: /opt/shell/plugins/fzf.plugin.sh"


## Check for Homebrew install
if [ -d /usr/local/opt/fzf ];
then
	fzfCompletions=/usr/local/opt/fzf/shell
	fzfKeybindings=/usr/local/opt/fzf/shell

## Check for Homebrew install #2
elif [ -d /opt/homebrew/opt/fzf ];
then
	fzfCompletions=/opt/homebrew/opt/fzf/shell
	fzfKeybindings=/opt/homebrew/opt/fzf/shell

## Check for MacPorts install
elif [ -f /opt/local/bin/port ];
then
	fzfCompletions=/opt/local/share/zsh/site-functions/fzf
	fzfKeybindings=/opt/local/share/fzf/shell

## Check for MacPorts install #2
elif [ -f /opt/ports/bin/port ];
then
	fzfCompletions=/opt/ports/share/zsh/site-functions/fzf
	fzfKeybindings=/opt/ports/share/fzf/shell

## Check for NetBSD Pkgin install
elif [ -f /opt/pkg/bin/pkgin ];
then
	fzfCompletions=/opt/pkg/share/zsh/site-functions/fzf
	fzfKeybindings=/opt/pkg/share/fzf/shell

## Check for Fink install
elif [ -f /sw/bin/fink ];
then
	fzfCompletions=/sw/opt/fzf/shell
	fzfKeybindings=/sw/opt/fzf/shell

elif [ -d /usr/share/fzf ];
then
	fzfCompletions=/usr/share/fzf
	fzfKeybindings=/usr/share/fzf

elif [ -d /usr/local/share/examples/fzf ];
then
	fzfCompletions=/usr/local/share/examples/fzf
	fzfKeybindings=/usr/local/share/examples/fzf
fi



case "$(ps -p $$ | grep -E -m 1 -o '\b\w{0,6}sh|koi')" \
in
	"zsh")
		[[ -o interactive && "$DISABLE_FZF_AUTO_COMPLETION" != "true" ]] \
		&& . "${fzfCompletions}/completion.zsh" 2> /dev/null

		[[ "$DISABLE_FZF_KEY_BINDINGS" != "true" ]] && . "${fzfKeybindings}/key-bindings.zsh" 2> /dev/null
	;;
	"bash")
		[[ "$-" == *i* && "$DISABLE_FZF_AUTO_COMPLETION" != "true" ]] \
		&& . "${fzfCompletions}/completion.bash" 2> /dev/null

		[[ "$DISABLE_FZF_KEY_BINDINGS" != "true" ]] && . "${fzfKeybindings}/key-bindings.bash" 2> /dev/null
	;;
esac
