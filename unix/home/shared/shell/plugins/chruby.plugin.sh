#
# /opt/shell/plugins/chruby.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: chruby"
# echo "Sourced: /opt/shell/plugins/chruby.plugin.sh"


## Find where chruby should be installed
if [ -d /usr/local/opt/chruby/share/chruby ];
then
	[ -r /usr/local/opt/chruby/share/chruby/chruby.sh ] \
	&& . /usr/local/opt/chruby/share/chruby/chruby.sh 2> /dev/null

	[ -r /usr/local/opt/chruby/share/chruby/auto.sh ] \
	&& . /usr/local/opt/chruby/share/chruby/auto.sh 2> /dev/null
fi
