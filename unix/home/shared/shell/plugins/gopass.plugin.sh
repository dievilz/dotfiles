#
# /opt/shell/plugins/fzf.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: gopass"
# echo "Sourced: /opt/shell/plugins/gopass.plugin.sh"


## Load completions
case "$(ps -p $$ | grep -Eo -m 1 '\b\w{0,6}sh|koi')" \
in
	"zsh")
		[[ -o interactive ]] && eval $(gopass completion zsh 2> /dev/null) 2> /dev/null
	;;
	"bash")
		[[ "$-" == *i* ]] && eval $(gopass completion bash 2> /dev/null) 2> /dev/null
	;;
esac
