#
# /opt/shell/plugins/php-version.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: php-version"
# echo "Sourced: /opt/shell/plugins/php-version.plugin.sh"


## Find where php-version should be installed
[ -e "$HOME/Dev/Repos/php-version/php-version.sh" ] \
&& . "$HOME/Dev/Repos/php-version/php-version.sh" 2> /dev/null
