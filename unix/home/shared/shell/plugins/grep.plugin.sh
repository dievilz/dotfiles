#
# /opt/shell/plugins/grep.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: grep"
# echo "Sourced: /opt/shell/plugins/grep.plugin.sh"


case "$(ps -p $$ | grep -Eo -m 1 '\b\w{0,6}sh|koi')" \
in
	"zsh")
		__GREP_CACHE_FILE="$XDG_CACHE_HOME"/zsh/grep-alias

		## See if there's a cache file modified in the last day
		__GREP_ALIAS_CACHES=("$__GREP_CACHE_FILE"(Nm-1))
		if [[ -n "$__GREP_ALIAS_CACHES" ]];
		then
			source "$__GREP_CACHE_FILE"
		else
			grep-flags-available() {
				command grep "$@" "" &>/dev/null <<< ""
			}

			## Ignore these folders (if the necessary grep flags are available)
			EXC_FOLDERS="{.bzr,CVS,.git,.hg,.svn,.idea,.tox}"

			## Check for --exclude-dir, otherwise check for --exclude. If --exclude
			## isn't available, --color won't be either (they were released at the same
			## time (v2.5): https://git.savannah.gnu.org/cgit/grep.git/tree/NEWS?id=1236f007
			if grep-flags-available --color=auto --exclude-dir=.cvs;
			then
				GREP_OPTIONS="--color=auto --exclude-dir=$EXC_FOLDERS"
			elif grep-flags-available --color=auto --exclude=.cvs;
			then
				GREP_OPTIONS="--color=auto --exclude=$EXC_FOLDERS"
			fi

			if [[ -n "$GREP_OPTIONS" ]];
			then
				## Export grep, egrep and fgrep settings
				alias grep="grep $GREP_OPTIONS"
				alias egrep="grep -E $GREP_OPTIONS"
				alias fgrep="grep -F $GREP_OPTIONS"

				## Write to cache file if cache directory is writable
				if [[ -w "$XDG_CACHE_HOME/zsh" ]]; then
					alias -L grep egrep fgrep >| "$__GREP_CACHE_FILE"
				fi
			fi

			## Clean up
			unset GREP_OPTIONS EXC_FOLDERS
			unfunction grep-flags-available
		fi

		unset __GREP_CACHE_FILE __GREP_ALIAS_CACHES
	;;
	"bash")
		true
	;;
	*)
		true
	;;
esac
