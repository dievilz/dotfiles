#
# /opt/shell/plugins/java.plugin.sh
#

## Printing for debugging purposes if session is interactive
[ -t 1 ] && \
echo "Plugin: java"
# echo "Sourced: /opt/shell/plugins/java.plugin.sh"


## Find where java should be installed
if [ -d $ASDF_DATA_DIR ];
then
	[ -d "$ASDF_DATA_DIR/plugins/java/set-java-home.zsh" ] \
	&& . "$ASDF_DATA_DIR/plugins/java/set-java-home.zsh" 2> /dev/null
fi
