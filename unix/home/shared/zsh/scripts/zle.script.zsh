#!/bin/zsh
#
# $ZDOTDIR/scripts/zle.script.zsh  OR  ~/.zle.script.zsh
#
# Zsh Line Editor and Line Buffer Source Manager for syntax hiliting, auto-suggestions, history-search, etc.

## Printing for debugging purposes if session is interactive
[[ -o interactive ]] && echo "Sourced: ===> $(realpath $0 2> /dev/null) $1 $2 $3 $4"

## echo $ZSH
## echo $ZSH_SYSTEM

zleSearchPlugins() \
{
	if [[ -z $AUTOSUGGESTION_ENABLED ]]; then
		case "$1" in
			"zsh-users")
				[[ -r "$ZSH/plugins/zsh-users/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh" ]] \
				&& AUTOSUGGESTION="$ZSH/plugins/zsh-users/zsh-autosuggestions"
				## Zplugin location
				[[ -r "$ZSH/plugins/zsh-users---zsh-autosuggestions/zsh-autosuggestions.plugin.zsh" ]] \
				&& AUTOSUGGESTION="$ZSH/plugins/zsh-users---zsh-autosuggestions"

				[[ -r "$ZSH_SYSTEM/zsh-users/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh" ]] \
				&& AUTOSUGGESTION="$ZSH_SYSTEM/zsh-users/zsh-autosuggestions"
			;;
		esac
	fi
	if [[ -z $HISTORY_SEARCH_ENABLED ]]; then
		case "$2" in
			"zdharma")
				[[ -r "$ZSH/plugins/zdharma/history-search-multi-word/history-search-multi-word.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH/plugins/zdharma/history-search-multi-word"
				## Zplugin location
				[[ -r "$ZSH/plugins/zdharma---history-search-multi-word/history-search-multi-word.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH/plugins/zdharma---history-search-multi-word"

				[[ -r "$ZSH_SYSTEM/zdharma/history-search-multi-word/history-search-multi-word.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH_SYSTEM/zdharma/history-search-multi-word"
			;;
			"zsh-users")
				[[ -r "$ZSH/plugins/zsh-users/zsh-history-substring-search/zsh-history-substring-search.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH/plugins/zsh-users/zsh-history-substring-search"
				## Zplugin location
				[[ -r "$ZSH/plugins/zsh-users---zsh-history-substring-search/zsh-history-substring-search.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH/plugins/zsh-users---zsh-history-substring-search"

				[[ -r "$ZSH_SYSTEM/zsh-users/zsh-history-substring-search/zsh-history-substring-search.plugin.zsh" ]] \
				&& HISTORY_SEARCH="$ZSH_SYSTEM/zsh-users/zsh-history-substring-search"
			;;
		esac
	fi
	if [[ -z $SYNTAX_HILITE_ENABLED ]]; then
		case "$3" in
			"zdharma")
				[[ -r "$ZSH/plugins/zdharma/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH/plugins/zdharma/fast-syntax-highlighting"
				## Zplugin location
				[[ -r "$ZSH/plugins/zdharma---fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH/plugins/zdharma---fast-syntax-highlighting"

				[[ -r "$ZSH_SYSTEM/zdharma/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH_SYSTEM/zdharma/fast-syntax-highlighting"
			;;
			"zsh-users")
				[[ -r "$ZSH/plugins/zsh-users/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH/plugins/zsh-users/zsh-syntax-highlighting"
				## Zplugin location
				[[ -r "$ZSH/plugins/zsh-users---zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH/plugins/zsh-users---zsh-syntax-highlighting"

				[[ -r "$ZSH_SYSTEM/zsh-users/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh" ]] \
				&& SYNTAX_HILITE="$ZSH_SYSTEM/zsh-users/zsh-syntax-highlighting"
			;;
		esac
	fi
}

zleLoadPlugins() \
{
	if [[ -z $AUTOSUGGESTION_ENABLED ]]; then
		case "$1" in
			"zsh-users")
				if [[ -n "$AUTOSUGGESTION" ]]; then
					source "$AUTOSUGGESTION"/zsh-autosuggestions.plugin.zsh ||
					source "$AUTOSUGGESTION"/zsh-autosuggestions.zsh

					export AUTOSUGGESTION_ENABLED=1
				else
					export AUTOSUGGESTION_ENABLED=0
				fi
			;;
		esac
	fi
	if [[ -z $HISTORY_SEARCH_ENABLED ]]; then
		case "$2" in
			"zdharma")
				if [[ -n "$HISTORY_SEARCH" ]]; then
					source "$HISTORY_SEARCH"/history-search-multi-word.plugin.zsh ||
					source "$HISTORY_SEARCH"/history-search-multi-word.zsh

					export HISTORY_SEARCH_ENABLED=1
				else
					export HISTORY_SEARCH_ENABLED=0
				fi
			;;
			"zsh-users")
				if [[ -n "$HISTORY_SEARCH" ]]; then
					source "$HISTORY_SEARCH"/zsh-history-substring-search.plugin.zsh ||
					source "$HISTORY_SEARCH"/zsh-history-substring-search.zsh

					export HISTORY_SEARCH_ENABLED=1
				else
					export HISTORY_SEARCH_ENABLED=0
				fi
			;;
		esac
	fi
	if [[ -z $SYNTAX_HILITE_ENABLED ]]; then
		case "$3" in
			"zdharma")
				if [[ -n "$SYNTAX_HILITE" ]]; then
					source "$SYNTAX_HILITE"/fast-syntax-highlighting.plugin.zsh ||
					source "$SYNTAX_HILITE"/fast-syntax-highlighting.zsh

					export SYNTAX_HILITE_ENABLED=1
				else
					export SYNTAX_HILITE_ENABLED=0
				fi
			;;
			"zsh-users")
				if [[ -n "$SYNTAX_HILITE" ]]; then
					source "$SYNTAX_HILITE"/zsh-syntax-highlighting.plugin.zsh ||
					source "$SYNTAX_HILITE"/zsh-syntax-highlighting.zsh

					export SYNTAX_HILITE_ENABLED=1
				else
					export SYNTAX_HILITE_ENABLED=0
				fi
			;;
		esac
	fi
}

zleAdjustPlugins_Autosuggest() \
{
	## Enable asynchronous fetching of suggestions.
	ZSH_AUTOSUGGEST_USE_ASYNC=1
	## For some reason, the offered completion winds up having the same color as
	## the terminal background color (when using a dark profile). Therefore, we
	## switch to gray.
	## See https://github.com/zsh-users/zsh-autosuggestions/issues/182.
	# ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=gray'
	ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=238'
}

zleAdjustPlugins_History() \
{
	zmodload zsh/terminfo # bind UP and DOWN arrow keys to history substring search

	zle -l | grep -q 'history-substring-search-up'   && bindkey '^[[A' history-substring-search-up
	zle -l | grep -q 'history-substring-search-down' && bindkey '^[[B' history-substring-search-down
	zle -l | grep -q 'history-substring-search-up'   && bindkey "${terminfo[kcuu1]}" history-substring-search-up
	zle -l | grep -q 'history-substring-search-down' && bindkey "${terminfo[kcud1]}" history-substring-search-down

	zle -l | grep -q 'history-substring-search-up'   && bindkey -M emacs '^P' history-substring-search-up
	zle -l | grep -q 'history-substring-search-down' && bindkey -M emacs '^N' history-substring-search-down
	zle -l | grep -q 'history-substring-search-up'   && bindkey -M vicmd 'k' history-substring-search-up
	zle -l | grep -q 'history-substring-search-down' && bindkey -M vicmd 'j' history-substring-search-down

	## Color in which to highlight matched, searched text (default bg=17 on 256-color terminals)
	zstyle ":history-search-multi-word" highlight-color "fg=yellow,bold"
	## Number of entries to show (default is $LINES/3)
	zstyle ":history-search-multi-word" page-size "11"
	## Effect on active history entry. Try: standout, bold, bg=blue (default underline)
	zstyle ":plugin:history-search-multi-word" active "underline"
	## Whether to check paths for existence and mark with magenta (default true)
	zstyle ":plugin:history-search-multi-word" check-paths "yes"
	## Whether pressing Ctrl-C or ESC should clear entered query
	zstyle ":plugin:history-search-multi-word" clear-on-cancel "no"
	## Whether to perform syntax highlighting (default true)
	zstyle ":plugin:history-search-multi-word" synhl "yes"
	##
	zstyle ":plugin:history-search-multi-word" reset-prompt-protect "1" \

	typeset -gA HSMW_HIGHLIGHT_STYLES
	## Sets path key – paths that exist will be highlighted with background magenta, foreground white, bold:
	HSMW_HIGHLIGHT_STYLES[path]="bg=magenta,fg=white,bold"
	## Enable coloring of options of form "-o" and "--option", with cyan:
	HSMW_HIGHLIGHT_STYLES[double-hyphen-option]="fg=cyan"
	HSMW_HIGHLIGHT_STYLES[single-hyphen-option]="fg=cyan"
	## 256 colors to highlight command separators (e.g., ;, &&, and ||):
	HSMW_HIGHLIGHT_STYLES[commandseparator]="fg=241,bg=17"
}

zleAdjustPlugins_SyntaxHilite() \
{
	typeset -gA FAST_HIGHLIGHT
	FAST_HIGHLIGHT[git-cmsg-len]=100;

	typeset -gA ZSH_HIGHLIGHT_STYLES
	ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
	ZSH_HIGHLIGHT_PATTERNS+=('rm -rf *' 'fg=white,bold,bg=red')
	ZSH_HIGHLIGHT_STYLES[path]='fg=blue'
	ZSH_HIGHLIGHT_STYLES[path_pathseparator]='fg=cyan'
	ZSH_HIGHLIGHT_STYLES[alias]='fg=cyan'
	ZSH_HIGHLIGHT_STYLES[builtin]='fg=cyan'
	ZSH_HIGHLIGHT_STYLES[function]='fg=cyan'
	ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=yellow'
	ZSH_HIGHLIGHT_STYLES[redirection]='fg=magenta'
	ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=cyan,bold'
	ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=magenta,bold'
	ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=yellow,bold'
}

main_zlescript() \
{
	# echo "$2"
	# echo "$3"
	# echo "$4"

	alias zleVars='
printf "\033[38;35mIs Autosuggestions Enabled?:\033[0m %s\n" "$AUTOSUGGESTION_ENABLED";
printf "\033[38;35mAutosuggestions plugin:     \033[0m %s\n" "$AUTOSUGGESTION";
printf "\033[38;35mIs History Search Enabled?: \033[0m %s\n" "$HISTORY_SEARCH_ENABLED";
printf "\033[38;35mHistory Search plugin:      \033[0m %s\n" "$HISTORY_SEARCH";
printf "\033[38;35mIs Syntax Hilite Enabled?:  \033[0m %s\n" "$SYNTAX_HILITE_ENABLED";
printf "\033[38;35mSyntax Hilite plugin:       \033[0m %s\n" "$SYNTAX_HILITE";
echo'

	case "$1" in
		"search")
			zleSearchPlugins "$2" "$3" "$4"
		;;
		"load")
			zleLoadPlugins "$2" "$3" "$4"
		;;
		"zsh-users"|"zdharma")
			zleSearchPlugins "$1" "$2" "$3"
			zleLoadPlugins "$1" "$2" "$3"
		;;
		"custom")
			if [[ "$#" -gt 0 ]]; then
				shift
				while [[ -n "$1" ]]; do
					case "$1" in
						"ausg") zleAdjustPlugins_Autosuggest
						;;
						"hist") zleAdjustPlugins_History
						;;
						"syhi") zleAdjustPlugins_SyntaxHilite
						;;
					esac
					shift
				done
			fi
		;;
		*)
			printf "\033[38;31m%s\033[0m\n" "===> zle.script.zsh: error: Invalid option! Aborting..."
	esac
}

main_zlescript "$@"; return
