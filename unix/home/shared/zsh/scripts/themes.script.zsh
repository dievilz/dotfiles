#!/bin/zsh
#
# $ZDOTDIR/scripts/themes.script.zsh  OR  ~/.themes.script.zsh
#
# @dievilz's script for sourcing themes for Zsh.

## Printing for debugging purposes if session is interactive
[[ -o interactive ]] && echo "Sourced: ===> $(realpath $0 2> /dev/null) $1 $2 $3"


set_pwrlvl9k_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait'!' lucid depth=1
			zinit light powerlevel9k/powerlevel9k && ZTHEME_FOUND=1
		;;
		"zgen") zgen load powerlevel9k/powerlevel9k && ZTHEME_FOUND=1
		;;
		"zplug") zplug "powerlevel9k/powerlevel9k", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=powerlevel9k
	else
		export ZTHEME_FOUND=0
	fi
}

set_pwrlvl9k_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return

	case "$1" in
		"random"|"") PWRLVL=$(jot -w %i -r 1 1 4+1) ;;
		*) PWRLVL="$1" ;;
	esac

	# echo "$PWRLVL"
	case "$PWRLVL" in
		"1")
			source "${ZSH_CUSTOM:?}/themes/pl9k/default.pl9k.zsh" || \
			source "${ZDOTDIR:?}/themes/pl9k/default.pl9k.zsh" || \
			print_error_custom_config
		;;
		"2")
			source "${ZSH_CUSTOM:?}/themes/pl9k/custom.pl9k.zsh" || \
			source "${ZDOTDIR:?}/themes/pl9k/custom.pl9k.zsh" || \
			print_error_custom_config
		;;
		"3")
			source "${ZSH_CUSTOM:?}/themes/pl9k/dievilz.pl9k.zsh" || \
			source "${ZDOTDIR:?}/themes/pl9k/dievilz.pl9k.zsh" || \
			print_error_custom_config
		;;
		"3")
			source "${ZSH_CUSTOM:?}/themes/pl9k/mavam.pl9k.zsh" || \
			source "${ZDOTDIR:?}/themes/pl9k/mavam.pl9k.zsh" || \
			print_error_custom_config
		;;
		*) print_error_custom_config
		;;
	esac
	unset PWRLVL
}
# ==============================================================================


set_pwrlvl10k_zshtheme() \
{
	# zle -lL zle-line-init > /dev/null && zle -D zle-line-init

	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait'!' lucid atload'true; _p9k_precmd' nocd
			zinit light dievilz/powerlevel10k && ZTHEME_FOUND=1
		;;
		"zgen") zgen load dievilz/powerlevel10k && ZTHEME_FOUND=1
		;;
		"zplug") zplug "dievilz/powerlevel10k", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=dievilz/powerlevel10k
	else
		export ZTHEME_FOUND=0
	fi
}

set_pwrlvl10k_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return

	case "$1" in
		"random"|"") PWRLVL=$(jot -w %i -r 1 1 2+1) ;;
		*) PWRLVL="$1" ;;
	esac

	# echo "$PWRLVL"
	case "$PWRLVL" in
		"1")
			source "${ZSH_CUSTOM:?}/themes/p10k/default.p10k.zsh" || \
			source "${ZDOTDIR:?}/themes/p10k/default.p10k.zsh" || \
			print_error_custom_config
		;;
		"2")
			source "${ZSH_CUSTOM:?}/themes/p10k/dievilz-icons.p10k.zsh" || \
			source "${ZDOTDIR:?}/themes/p10k/dievilz-icons.p10k.zsh" || \
			print_error_custom_config
		;;
		"3")
			source "${ZSH_CUSTOM:?}/themes/p10k/dievilz.p10k.zsh" || \
			source "${ZDOTDIR:?}/themes/p10k/dievilz.p10k.zsh" || \
			print_error_custom_config
		;;
		*) print_error_custom_config
		;;
	esac
	unset PWRLVL
}
# ==============================================================================


set_spaceship_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit")
			zinit for \
				atclone" ./starship init zsh > init.zsh
				./starship completions zsh > _starship" \
				from'gh-r' nocompile sbin src'init.zsh' \
				starship/starship && ZTHEME_FOUND=1
		;;
		"zgen") zgen load spaceship/spaceship && ZTHEME_FOUND=1
		;;
		"zplug") zplug "spaceship/spaceship", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=spaceship
	else
		export ZTHEME_FOUND=0
	fi
}

set_spaceship_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return
}
# ==============================================================================


set_geometry_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit wait'0' lucid atload'geometry::prompt' light-mode for \
			geometry/geometry && ZTHEME_FOUND=1
		;;
		"zgen") zgen load geometry/geometry && ZTHEME_FOUND=1
		;;
		"zplug") zplug "geometry/geometry", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=geometry
	else
		export ZTHEME_FOUND=0
	fi
}

set_geometry_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return
}
# ==============================================================================


set_hyperzsh_zshtheme() \
{
	case "$1" in
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait depth=1; zinit light hyperzsh/hyperzsh && ZTHEME_FOUND=1
		;;
		"zgen") zgen load hyperzsh/hyperzsh && ZTHEME_FOUND=1
		;;
		"zplug") zplug "hyperzsh/hyperzsh", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=hyperzsh
	else
		export ZTHEME_FOUND=0
	fi
}

set_hyperzsh_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return
}
# ==============================================================================


set_pygmalion_zshtheme() \
{
	case "$1" in
		"local")
			# unset foundPygmalionTheme;
			# unset foundPygmIter;

			# foundPygmalionTheme="$(
			# 	find /opt -name "*pygmalion.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	find /usr -name "*pygmalion.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	find $HOME -name "*pygmalion.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	[ $(uname -s) != "Darwin" ] && \
			# 	find /home -name "*pygmalion.theme.zsh" \! -type d -quit 2> /dev/null;)";

			# for foundPygmIter in $(echo $foundPygmalionTheme > /dev/null | tr "\n" " "); do
			# 	[ -r "$foundPygmIter" ] && source "$foundPygmIter" && ZTHEME_FOUND=1 && break;
			# done

			if [ -f "${ZSH:?}/themes/pygmalion.theme.zsh" ]; then
				source "${ZSH:?}/themes/pygmalion.theme.zsh" && ZTHEME_FOUND=1

			elif [ -f "${ZSH_CUSTOM:?}/themes/pygmalion.theme.zsh" ]; then
				source "${ZSH_CUSTOM:?}/themes/pygmalion.theme.zsh" && ZTHEME_FOUND=1

			elif [ -f "${ZDOTDIR:?}/themes/pygmalion.theme.zsh" ]; then
				source "${ZDOTDIR:?}/themes/pygmalion.theme.zsh" && ZTHEME_FOUND=1
			fi
		;;
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait depth=1; zinit light OMZ::pygmalion && ZTHEME_FOUND=1
		;;
		"zgen") zgen load OMZ::pygmalion && ZTHEME_FOUND=1
		;;
		"zplug") zplug "OMZ::pygmalion", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=pygmalion
	else
		export ZTHEME_FOUND=0
	fi
}

set_pygmalion_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return
}
# ==============================================================================


set_dievilz_zshtheme() \
{
	case "$1" in
		"local")
			# unset foundDievilzTheme;
			# unset foundDvlzIter;

			# foundDievilzTheme="$(
			# 	find /opt -name "*dievilz.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	find /usr -name "*dievilz.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	find $HOME -name "*dievilz.zsh.theme" \! -type d -quit 2> /dev/null ||
			# 	[ $(uname -s) != "Darwin" ] && \
			# 	find /home -name "*dievilz.theme.zsh" \! -type d -quit 2> /dev/null;)";

			# for foundDvlzIter in $(echo $foundDievilzTheme > /dev/null | tr "\n" " "); do
			# 	[ -r "$foundDvlzIter" ] && source "$foundDvlzIter" && ZTHEME_FOUND=1 && break;
			# done

			if [ -f "${ZSH:?}/themes/dievilz.theme.zsh" ]; then
				source "${ZSH:?}/themes/dievilz.theme.zsh" && ZTHEME_FOUND=1

			elif [ -f "${ZSH_CUSTOM:?}/themes/dievilz.theme.zsh" ]; then
				source "${ZSH_CUSTOM:?}/themes/dievilz.theme.zsh" && ZTHEME_FOUND=1

			elif [ -f "${ZDOTDIR:?}/themes/dievilz.theme.zsh" ]; then
				source "${ZDOTDIR:?}/themes/dievilz.theme.zsh" && ZTHEME_FOUND=1
			fi
		;;
		"omz") ZTHEME_FOUND=1
		;;
		"zinit") zinit ice wait depth=1; zinit light dievilz/zsh-theme && ZTHEME_FOUND=1
		;;
		"zgen") zgen load dievilz/zsh-theme && ZTHEME_FOUND=1
		;;
		"zplug") zplug "dievilz/zsh-theme", as:theme && ZTHEME_FOUND=1
		;;
	esac

	## This is so the variable ZSH_THEME can be set after deciding which
	## plugin manager is gonna be used
	if [[ ${ZTHEME_FOUND:-0} -eq 1 ]]; then
		export ZSH_THEME=dievilz
	else
		export ZTHEME_FOUND=0
	fi
}

set_dievilz_zshtheme_custom() \
{
	[[ $ZTHEME_FOUND -eq 0 ]] && return
}
# ==============================================================================


################################################################################


print_error_custom_config() \
{
	printf "===> themes.script.zsh: Custom config not found or doesn't exist...\n"
}


################################################################################


# ------------------------------------------------------------------------------

usage_zshthemes() {
echo
echo "Zsh Theme Sourcing Script"
printf "SYNOPSIS: ./themes.script.zsh [<theme> [<plugin_manager>|<custom> [<config_number>|<random>]]][-h]\n"
printf "./themes.script.zsh [-<theme>|--<theme> [omz|zgen|zinit|zplug]] \n"
printf "./themes.script.zsh [-<theme>|--<theme> [custom [<config_number>|<random>]]] \n"
cat <<-'EOF'

OPTIONS:
   -p9,--powerlevel9k     Powerlevel9k theme by @bhilburn
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -p10,--powerlevel10k,--dievilz/powerlevel10k
                              Powerlevel9k theme by @romkatv, modified by @dievilz
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -spa,--spaceship       Spaceship theme by @
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -geo,--geometry        Geometry theme by @
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -hyp,--hyperzsh        Hyperzsh theme by @
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -pyg,--pygmalion       Pygmalion theme by Oh-My-Zsh
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -dvlz,--dievilz         Dievilz theme by @dievilz
      <plugin_manager>        Choose {omz,zgen,zinit or zplug} as plugin manager
      custom                  Source a custom configuration

   -h,--help              Show this menu


   All options and option-arguments are mutually exclusive.

   If a custom configuration was selected as "random" or no one at all, the script will choose a config randomly.

EOF
}

main_zshthemes() \
{
	case $1 in
		"-h"|"--help")
			usage_zshthemes
			return 0
		;;
		"-p9"|"--powerlevel9k")
			case "$2" in
				"custom")
					set_pwrlvl9k_zshtheme_custom "$3"
				;;
				*)
					set_pwrlvl9k_zshtheme "$2"
				;;
			esac
		;;
		"-p10"|"--powerlevel10k"|"--dievilz/powerlevel10k")
			case "$2" in
				"custom")
					set_pwrlvl10k_zshtheme_custom "$3"
				;;
				*)
					set_pwrlvl10k_zshtheme "$2"
				;;
			esac
		;;
		"-spa"|"--spaceship")
			case "$2" in
				"custom")
					set_spaceship_zshtheme_custom "$3"
				;;
				*)
					set_spaceship_zshtheme "$2"
				;;
			esac
		;;
		"-geo"|"--geometry")
			case "$2" in
				"custom")
					set_geometry_zshtheme_custom "$3"
				;;
				*)
					set_geometry_zshtheme "$2"
				;;
			esac
		;;
		"-hyp"|"--hyperzsh")
			case "$2" in
				"custom")
					set_hyperzsh_zshtheme_custom "$3"
				;;
				*)
					set_hyperzsh_zshtheme "$2"
				;;
			esac
		;;
		"-pyg"|"--pygmalion")
			case "$2" in
				"custom")
					set_pygmalion_zshtheme_custom "$3"
				;;
				*)
					set_pygmalion_zshtheme "$2"
				;;
			esac
		;;
		"-dvl"|"--dievilz")
			case "$2" in
				"custom")
					set_dievilz_zshtheme_custom "$3"
				;;
				*)
					set_dievilz_zshtheme "$2"
				;;
			esac
		;;
		"random")
			export ZSH_THEME=random
		;;
		*)
			usage_zshthemes
			return 127
		;;
	esac
}

main_zshthemes "$@"
