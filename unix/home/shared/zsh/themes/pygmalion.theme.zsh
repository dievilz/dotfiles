# Yay! High voltage and arrows!
# https://github.com/ohmyzsh/ohmyzsh/blob/master/themes/pygmalion.zsh-theme

## Enable colors and predefined variables
autoload -U colors  && colors
## Load the theme system
autoload -U promptinit && promptinit

zsh_prompt_setup_ip_hostname() \
{
	## To see IPs in prompt
	unalias ip 2> /dev/null
	unalias hostname 2> /dev/null
	unalias ifconfig 2> /dev/null

	unset ZSH_PROMPT_IP_HOSTNAME
	case "$(uname -s)" \
	in
		"Darwin")
			ZSH_PROMPT_IP_INTERFACE="$(route get default | grep interface | awk '{print $2}')"
			ZSH_PROMPT_IP_HOSTNAME="$(ifconfig "$ZSH_PROMPT_IP_INTERFACE" | awk '/inet / {print $2}')"

			ZSH_PROMPT_IP_HOSTNAME_CMD="NetBSD ifconfig"
		;;
		*"Linux"*|"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*)
			########### Linux commands
			## Kernel.org iproute2' ip
			if command -v ip > /dev/null 2>&1;
			then
			### TO-DO: THIS IS NOT WORKING ON DEBIAN <------------------------------------------------------
				# strings "$(command -v ip > /dev/null 2>&1)" 2> /dev/null | grep -iq 'iproute2|iproute' && \
				ZSH_PROMPT_IP_HOSTNAME="$(ip -o -4 addr show scope global | awk '{print $4}' | tr '\n' ' ' | awk '{print $1, $2}')"
				# $(ip -o -6 addr show scope global | awk '{print $4}')"
				# $(ip -o -a addr show scope global | awk '{print $4}' | tr '\n' ' ')"
				ZSH_PROMPT_IP_HOSTNAME_CMD="Kernel.org iproute2 ip -o -4 addr show scope global | awk '{print \$4}'"

			# elif command -v hostname > /dev/null 2>&1;
			# then
			# 	###### hostname -i returns the IP address associated with the system's hostname.
			# 	###### hostname -I returns a list of all IP addresses associated with the system.

			# ## GNU inetutils (GNU cross-platform impl)
			# 	string "$(command -v hostname > /dev/null 2>&1)" 2> /dev/null | grep -iq 'gnu|inetutils' && \
			# 	ZSH_PROMPT_IP_HOSTNAME="$(hostname -i | awk '{print $1, $2}')"
			# 	ZSH_PROMPT_IP_HOSTNAME_CMD="GNU inetutils hostname -i | awk '{print \$1, \$2}'"

			# ## net-tools (Debian-based)
			# 	string "$(command -v hostname > /dev/null 2>&1)" 2> /dev/null | grep -iq 'net-tools' && \
			# 	ZSH_PROMPT_IP_HOSTNAME="$(hostname -I | awk '{print $1, $2}')"
			# 	ZSH_PROMPT_IP_HOSTNAME_CMD="net-tools hostname -I | awk '{print \$1, \$2}'"
			fi
		;;
		# *)
		# 	command -v ifconfig > /dev/null 2>&1 && \
		# 	ZSH_PROMPT_IP_HOSTNAME="$(ifconfig -a | grep 'inet ' | grep -v '127.0.0.1' | awk '{print $2}' | tr '\n' ' ')"
		# 	##                        ifconfig -a | grep 'inet6 ' | grep -v '::1' | awk '{print $2}' | tr '\n' ' ')
		# 	ZSH_PROMPT_IP_HOSTNAME_CMD="ifconfig -a | grep 'inet ' | grep -v '127.0.0.1' | awk '{print \$2}' | tr '\\\n' ' '"
		# ;;
	esac

	if [ -z $ZSH_PROMPT_IP_HOSTNAME ]; then
		## Good ol' ifconfig
		command -v ifconfig > /dev/null 2>&1 && \
		ZSH_PROMPT_IP_HOSTNAME="$(ifconfig -a | grep 'inet ' | grep -v '127.0.0.1' | awk '{print $2}' | tr '\n' ' ')"
		ZSH_PROMPT_IP_HOSTNAME_CMD="ifconfig -a | grep 'inet ' | grep -v '127.0.0.1' | awk '{print \$2}' | tr '\\\n' ' '"
	fi
	export ZSH_PROMPT_IP_HOSTNAME="$IP_ICON $ZSH_PROMPT_IP_HOSTNAME"
	export ZSH_PROMPT_IP_HOSTNAME_CMD
}

prompt_setup_pygmalion() \
{
	setopt localoptions promptsubst extendedglob

	HOME_ICON="$(printf '\uF07C  ')"
	USER_ICON="$(printf '\uF415 ')"
	HOST_ICON="$(printf '\uF109 ')"
	IP_ICON="$(printf '\uF0AC')"
	CLOCK_ICON="$(printf ' \uF017')"
	VCS_ICON="$(printf ' \uE725')"

	ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[yellow]%}"
	ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
	ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[009]%}⚡%{$reset_color%}"
	ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[green]%}"

	zsh_prompt_setup_ip_hostname

	## OG:       user@host:~/.oh-my-zsh/themes|master⚡  ⇒ ll
	# custom_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}%m%{$reset_color%}%{$fg[red]%}:%{$reset_color%}%{$fg[cyan]%}%0~%{$reset_color%}%{$fg[red]%}|%{$reset_color%}'

	## PERSONAL: user@host:~/.oh-my-zsh/themes|master⚡  ⇒ ll (cwd in blue instead of cyan)
	# custom_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}%m%{$reset_color%}%{$fg[red]%}:%{$reset_color%}%{$fg[blue]%}%0~%{$reset_color%}%{$fg[red]%}|%{$reset_color%}'

	## WORK:     user@<00.00.00.00> <00.00.00.00> :~/.oh-my-zsh/themes $  on master ✘ ll
	##               %{$fg[magenta]%}%n%{$reset_color%}@%{$fg[yellow]%}$(hostname -I | awk '"'{print \$1, \$2}')"')%{$reset_color%}:%{%B%F{238}%}${PWD/#$HOME/~%B}%{$reset_color%}%{$fg[cyan]%} %(!.#.$)%{$reset_color%} $(git_prompt_info)
	# custom_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}$(hostname -I | awk '"'{print \$1, \$2}')"')%{$reset_color%}%{$fg[red]%}:%{$reset_color%}%{$fg[blue]%}%0~%{$reset_color%}%{$fg[cyan]%} %(!.#.$)%{$reset_color%} '

	## HYBRID:   user@host <00.00.00.00> <00.00.00.00> :~/.oh-my-zsh/themes|master⚡
	## ⇒ ll
	  custom_prompt='%{$fg[red]%}:%{$reset_color%}%{$fg[blue]%}%0~%{$reset_color%}%{$fg[red]%}|%{$reset_color%}'

	base_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}%m%{$reset_color%} %F{027}$ZSH_PROMPT_IP_HOSTNAME%f%{$reset_color%}'

	# OG: post_prompt='%{$fg[cyan]%}⇒%{$reset_color%} '
	# CUSTOM: post_prompt='%(?.%F{040}%#%f.%F{196}%#%f) '
	post_prompt='
%(?.%{$fg[cyan]%}⇒%f.%{$fg[red]%}⇒%f)'

	base_prompt_nocolor=${base_prompt//\%\{[^\}]##\}}
	post_prompt_nocolor=${post_prompt//\%\{[^\}]##\}}

	autoload -U add-zsh-hook
	add-zsh-hook precmd prompt_pygmalion_precmd
}

prompt_pygmalion_precmd(){
	setopt localoptions nopromptsubst extendedglob

	## Load built-in version control system
	autoload -Uz vcs_info
	zstyle ':vcs_info:*' enable git svn

	local gitinfo=$(git_prompt_info)
	local gitinfo_nocolor=${gitinfo//\%\{[^\}]##\}}
	local exp_nocolor="$(print -P \"${base_prompt_nocolor}${gitinfo_nocolor}${post_prompt_nocolor}\")"
	local prompt_length=${#exp_nocolor}

	PROMPT="
${custom_prompt}\$(git_prompt_info)
${base_prompt}${post_prompt} "

	RPROMPT='%{'$'\033[1A''%}' # one line up '\e[
	RPROMPT+='%F{red}%(?..↵ %? )%f  %F{cyan}%D{%Y-%m-%d %I:%M:%S %p}%f'
	RPROMPT+='%{'$'\033[1B''%}' # one line down '\e[
}

## By @romkatv. See https://www.zsh.org/mla/users/2019/msg00633.html
zle-line-init() \
{
	emulate -L zsh

	[[ $CONTEXT == start ]] || return 0

	while true;
	do
		zle .recursive-edit
		local -i ret=$?
		[[ $ret == 0 && $KEYS == $'\4' ]] || break
		[[ -o ignore_eof ]] || exit 0
	done

	local saved_prompt=$PROMPT
	local saved_rprompt=$RPROMPT

	## How you want to show the buffered prompt
	PROMPT='
%F{196}%(?..↵ %?
)%f%F{blue}%0~%f  %F{magenta}%n%F{cyan}@%f%F{yellow}%m%f
%F{cyan}%D{%Y-%m-%d %I:%M:%S %p}%f %(?.%F{040}%#%f.%F{196}%#%f) '

	RPROMPT=''
	# RPROMPT='%F{cyan}%D{%Y-%m-%d %I:%M:%S %p}%f'

	zle .reset-prompt
	PROMPT=$saved_prompt
	RPROMPT=$saved_rprompt

	if (( ret )); then
		zle .send-break
	else
		zle .accept-line
	fi
	return ret
}





# The git prompt's git commands are read-only and should not interfere with
# other processes. This environment variable is equivalent to running with `git
# --no-optional-locks`, but falls back gracefully for older versions of git.
# See git(1) for and git-status(1) for a description of that flag.
#
# We wrap in a local function instead of exporting the variable directly in
# order to avoid interfering with manually-run git commands by the user.
function __git_prompt_git() {
	GIT_OPTIONAL_LOCKS=0 command git "$@"
}

function git_prompt_info() {
	# If we are on a folder not tracked by git, get out.
	# Otherwise, check for hide-info at global and local repository level
	if ! __git_prompt_git rev-parse --git-dir &> /dev/null \
	|| [[ "$(__git_prompt_git config --get oh-my-zsh.hide-info 2>/dev/null)" == 1 ]]; then
		return 0
	fi

	local ref
	ref=$(__git_prompt_git symbolic-ref --short HEAD 2> /dev/null) \
	|| ref=$(__git_prompt_git describe --tags --exact-match HEAD 2> /dev/null) \
	|| ref=$(__git_prompt_git rev-parse --short HEAD 2> /dev/null) \
	|| return 0

	# Use global ZSH_THEME_GIT_SHOW_UPSTREAM=1 for including upstream remote info
	local upstream
	if (( ${+ZSH_THEME_GIT_SHOW_UPSTREAM} )); then
		upstream=$(__git_prompt_git rev-parse --abbrev-ref --symbolic-full-name "@{upstream}" 2>/dev/null) \
		&& upstream=" -> ${upstream}"
	fi

	echo "${ZSH_THEME_GIT_PROMPT_PREFIX}${ref:gs/%/%%}${upstream:gs/%/%%}$(parse_git_dirty)${ZSH_THEME_GIT_PROMPT_SUFFIX}"
}

# Checks if working tree is dirty
function parse_git_dirty() {
	local STATUS
	local -a FLAGS
	FLAGS=('--porcelain')
	if [[ "$(__git_prompt_git config --get oh-my-zsh.hide-dirty)" != "1" ]]; then
		if [[ "${DISABLE_UNTRACKED_FILES_DIRTY:-}" == "true" ]]; then
			FLAGS+='--untracked-files=no'
		fi
		case "${GIT_STATUS_IGNORE_SUBMODULES:-}" in
			git)
				# let git decide (this respects per-repo config in .gitmodules)
			;;
			*)
				# if unset: ignore dirty submodules
				# other values are passed to --ignore-submodules
				FLAGS+="--ignore-submodules=${GIT_STATUS_IGNORE_SUBMODULES:-dirty}"
			;;
		esac
		STATUS=$(__git_prompt_git status ${FLAGS} 2> /dev/null | tail -n 1)
	fi
	if [[ -n $STATUS ]]; then
		echo "$ZSH_THEME_GIT_PROMPT_DIRTY"
	else
		echo "$ZSH_THEME_GIT_PROMPT_CLEAN"
	fi
}

# Gets the difference between the local and remote branches
function git_remote_status() {
	local remote ahead behind git_remote_status git_remote_status_detailed
	remote=${$(__git_prompt_git rev-parse --verify ${hook_com[branch]}@{upstream} --symbolic-full-name 2>/dev/null)/refs\/remotes\/}
	if [[ -n ${remote} ]]; then
		ahead=$(__git_prompt_git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
		behind=$(__git_prompt_git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)

		if [[ $ahead -eq 0 ]] && [[ $behind -eq 0 ]]; then
			git_remote_status="$ZSH_THEME_GIT_PROMPT_EQUAL_REMOTE"
		elif [[ $ahead -gt 0 ]] && [[ $behind -eq 0 ]]; then
			git_remote_status="$ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE"
			git_remote_status_detailed="$ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE_COLOR$ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE$((ahead))%{$reset_color%}"
		elif [[ $behind -gt 0 ]] && [[ $ahead -eq 0 ]]; then
			git_remote_status="$ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE"
			git_remote_status_detailed="$ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE_COLOR$ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE$((behind))%{$reset_color%}"
		elif [[ $ahead -gt 0 ]] && [[ $behind -gt 0 ]]; then
			git_remote_status="$ZSH_THEME_GIT_PROMPT_DIVERGED_REMOTE"
			git_remote_status_detailed="$ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE_COLOR$ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE$((ahead))%{$reset_color%}$ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE_COLOR$ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE$((behind))%{$reset_color%}"
		fi

		if [[ -n $ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_DETAILED ]]; then
			git_remote_status="$ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_PREFIX${remote:gs/%/%%}$git_remote_status_detailed$ZSH_THEME_GIT_PROMPT_REMOTE_STATUS_SUFFIX"
		fi

		echo $git_remote_status
	fi
}

# Outputs the name of the current branch
# Usage example: git pull origin $(git_current_branch)
# Using '--quiet' with 'symbolic-ref' will not cause a fatal error (128) if
# it's not a symbolic ref, but in a Git repo.
function git_current_branch() {
	local ref
	ref=$(__git_prompt_git symbolic-ref --quiet HEAD 2> /dev/null)
	local ret=$?
	if [[ $ret != 0 ]]; then
		[[ $ret == 128 ]] && return  # no git repo.
		ref=$(__git_prompt_git rev-parse --short HEAD 2> /dev/null) || return
	fi
	echo ${ref#refs/heads/}
}


# Gets the number of commits ahead from remote
function git_commits_ahead() {
	if __git_prompt_git rev-parse --git-dir &>/dev/null; then
		local commits="$(__git_prompt_git rev-list --count @{upstream}..HEAD 2>/dev/null)"
		if [[ -n "$commits" && "$commits" != 0 ]]; then
			echo "$ZSH_THEME_GIT_COMMITS_AHEAD_PREFIX$commits$ZSH_THEME_GIT_COMMITS_AHEAD_SUFFIX"
		fi
	fi
}

# Gets the number of commits behind remote
function git_commits_behind() {
	if __git_prompt_git rev-parse --git-dir &>/dev/null; then
		local commits="$(__git_prompt_git rev-list --count HEAD..@{upstream} 2>/dev/null)"
		if [[ -n "$commits" && "$commits" != 0 ]]; then
			echo "$ZSH_THEME_GIT_COMMITS_BEHIND_PREFIX$commits$ZSH_THEME_GIT_COMMITS_BEHIND_SUFFIX"
		fi
	fi
}

# Outputs if current branch is ahead of remote
function git_prompt_ahead() {
	if [[ -n "$(__git_prompt_git rev-list origin/$(git_current_branch)..HEAD 2> /dev/null)" ]]; then
		echo "$ZSH_THEME_GIT_PROMPT_AHEAD"
	fi
}

# Outputs if current branch is behind remote
function git_prompt_behind() {
	if [[ -n "$(__git_prompt_git rev-list HEAD..origin/$(git_current_branch) 2> /dev/null)" ]]; then
		echo "$ZSH_THEME_GIT_PROMPT_BEHIND"
	fi
}

# Outputs if current branch exists on remote or not
function git_prompt_remote() {
	if [[ -n "$(__git_prompt_git show-ref origin/$(git_current_branch) 2> /dev/null)" ]]; then
		echo "$ZSH_THEME_GIT_PROMPT_REMOTE_EXISTS"
	else
		echo "$ZSH_THEME_GIT_PROMPT_REMOTE_MISSING"
	fi
}

# Formats prompt string for current git commit short SHA
function git_prompt_short_sha() {
	local SHA
	SHA=$(__git_prompt_git rev-parse --short HEAD 2> /dev/null) && echo "$ZSH_THEME_GIT_PROMPT_SHA_BEFORE$SHA$ZSH_THEME_GIT_PROMPT_SHA_AFTER"
}

# Formats prompt string for current git commit long SHA
function git_prompt_long_sha() {
	local SHA
	SHA=$(__git_prompt_git rev-parse HEAD 2> /dev/null) && echo "$ZSH_THEME_GIT_PROMPT_SHA_BEFORE$SHA$ZSH_THEME_GIT_PROMPT_SHA_AFTER"
}

function git_prompt_status() {
	[[ "$(__git_prompt_git config --get oh-my-zsh.hide-status 2>/dev/null)" = 1 ]] && return

	# Maps a git status prefix to an internal constant
	# This cannot use the prompt constants, as they may be empty
	local -A prefix_constant_map
	prefix_constant_map=(
		'\?\? '     'UNTRACKED'
		'A  '       'ADDED'
		'M  '       'ADDED'
		'MM '       'MODIFIED'
		' M '       'MODIFIED'
		'AM '       'MODIFIED'
		' T '       'MODIFIED'
		'R  '       'RENAMED'
		' D '       'DELETED'
		'D  '       'DELETED'
		'UU '       'UNMERGED'
		'ahead'     'AHEAD'
		'behind'    'BEHIND'
		'diverged'  'DIVERGED'
		'stashed'   'STASHED'
	)

	# Maps the internal constant to the prompt theme
	local -A constant_prompt_map
	constant_prompt_map=(
		'UNTRACKED' "$ZSH_THEME_GIT_PROMPT_UNTRACKED"
		'ADDED'     "$ZSH_THEME_GIT_PROMPT_ADDED"
		'MODIFIED'  "$ZSH_THEME_GIT_PROMPT_MODIFIED"
		'RENAMED'   "$ZSH_THEME_GIT_PROMPT_RENAMED"
		'DELETED'   "$ZSH_THEME_GIT_PROMPT_DELETED"
		'UNMERGED'  "$ZSH_THEME_GIT_PROMPT_UNMERGED"
		'AHEAD'     "$ZSH_THEME_GIT_PROMPT_AHEAD"
		'BEHIND'    "$ZSH_THEME_GIT_PROMPT_BEHIND"
		'DIVERGED'  "$ZSH_THEME_GIT_PROMPT_DIVERGED"
		'STASHED'   "$ZSH_THEME_GIT_PROMPT_STASHED"
	)

	# The order that the prompt displays should be added to the prompt
	local status_constants
	status_constants=(
		UNTRACKED ADDED MODIFIED RENAMED DELETED
		STASHED UNMERGED AHEAD BEHIND DIVERGED
	)

	local status_text
	status_text="$(__git_prompt_git status --porcelain -b 2> /dev/null)"

	# Don't continue on a catastrophic failure
	if [[ $? -eq 128 ]]; then
		return 1
	fi

	# A lookup table of each git status encountered
	local -A statuses_seen

	if __git_prompt_git rev-parse --verify refs/stash &>/dev/null; then
		statuses_seen[STASHED]=1
	fi

	local status_lines
	status_lines=("${(@f)${status_text}}")

	# If the tracking line exists, get and parse it
	if [[ "$status_lines[1]" =~ "^## [^ ]+ \[(.*)\]" ]]; then
		local branch_statuses
		branch_statuses=("${(@s/,/)match}")
		for branch_status in $branch_statuses; do
			if [[ ! $branch_status =~ "(behind|diverged|ahead) ([0-9]+)?" ]]; then
				continue
			fi
			local last_parsed_status=$prefix_constant_map[$match[1]]
			statuses_seen[$last_parsed_status]=$match[2]
		done
	fi

	# For each status prefix, do a regex comparison
	for status_prefix in ${(k)prefix_constant_map}; do
		local status_constant="${prefix_constant_map[$status_prefix]}"
		local status_regex=$'(^|\n)'"$status_prefix"

		if [[ "$status_text" =~ $status_regex ]]; then
			statuses_seen[$status_constant]=1
		fi
	done

	# Display the seen statuses in the order specified
	local status_prompt
	for status_constant in $status_constants; do
		if (( ${+statuses_seen[$status_constant]} )); then
			local next_display=$constant_prompt_map[$status_constant]
			status_prompt="$next_display$status_prompt"
		fi
	done

	echo $status_prompt
}

# Outputs the name of the current user
# Usage example: $(git_current_user_name)
function git_current_user_name() {
	__git_prompt_git config user.name 2>/dev/null
}

# Outputs the email of the current user
# Usage example: $(git_current_user_email)
function git_current_user_email() {
	__git_prompt_git config user.email 2>/dev/null
}

# Output the name of the root directory of the git repository
# Usage example: $(git_repo_name)
function git_repo_name() {
	local repo_path
	if repo_path="$(__git_prompt_git rev-parse --show-toplevel 2>/dev/null)" && [[ -n "$repo_path" ]]; then
		echo ${repo_path:t}
	fi
}

## Delete any remaining precmd theme hooks
# for prompt_precmd in $(add-zsh-hook -L | grep -Eio 'prompt_.*_precmd');
for prompt_precmd in $(add-zsh-hook -L | grep -Eio '\bprompt_[a-zA-Z0-9_]+');
do add-zsh-hook -d precmd $prompt_precmd 2> /dev/null; done

prompt_setup_pygmalion

if [[ ${ZSH_TRANSIENT_PROMPT:-1} -eq 1 ]]; then
	zle -N zle-line-init
fi

echo "Theme: (oh my zsh) pygmalion ($(realpath $0 2> /dev/null))"





####################################################################################################
# ORIGINAL PYGMALION PROMPT CODE
####################################################################################################

# # Yay! High voltage and arrows!

# prompt_setup_pygmalion(){
#   setopt localoptions extendedglob

#   ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[green]%}"
#   ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
#   ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[yellow]%}⚡%{$reset_color%}"
#   ZSH_THEME_GIT_PROMPT_CLEAN=""

#   base_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}%m%{$reset_color%}%{$fg[red]%}:%{$reset_color%}%{$fg[cyan]%}%0~%{$reset_color%}%{$fg[red]%}|%{$reset_color%}'
#   post_prompt='%{$fg[cyan]%}⇒%{$reset_color%}  '

#   base_prompt_nocolor=${base_prompt//\%\{[^\}]##\}}
#   post_prompt_nocolor=${post_prompt//\%\{[^\}]##\}}

#   autoload -U add-zsh-hook
#   add-zsh-hook precmd prompt_pygmalion_precmd
# }

# prompt_pygmalion_precmd(){
#   setopt localoptions nopromptsubst extendedglob

#   local gitinfo=$(git_prompt_info)
#   local gitinfo_nocolor=${gitinfo//\%\{[^\}]##\}}
#   local exp_nocolor="$(print -P \"${base_prompt_nocolor}${gitinfo_nocolor}${post_prompt_nocolor}\")"
#   local prompt_length=${#exp_nocolor}

#   PROMPT="${base_prompt}\$(git_prompt_info)${post_prompt}"
# }

# prompt_setup_pygmalion
