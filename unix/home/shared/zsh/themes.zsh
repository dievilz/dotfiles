#
# /opt/zsh/themes.zsh
#

function is_theme() \
{
	local base_dir=$1
	local name=$2
	builtin test -f "$base_dir/$name.zsh-theme" || \
	builtin test -f "$base_dir/$name.theme.zsh"
}

function theme \
{
	: "${1:-$ZSH_THEME}"

	## Use random theme if none provided
	# if [[ -n "${1:=random}" ]]; then
	if [[ -n "$1" ]]; then
		if is_theme "${ZSH_CUSTOM:-$ZSH/custom}/themes/$1" "$1"; then
			source "${ZSH_CUSTOM:-$ZSH/custom}/themes/$1/$1.zsh-theme" || \
			source "${ZSH_CUSTOM:-$ZSH/custom}/themes/$1/$1.theme.zsh"
			echo "Theme:  $1"

		elif is_theme "${ZSH_CUSTOM:-$ZSH/custom}/themes" "$1"; then
			source "${ZSH_CUSTOM:-$ZSH/custom}/themes/$1.zsh-theme" || \
			source "${ZSH_CUSTOM:-$ZSH/custom}/themes/$1.theme.zsh"
			echo "Theme:  $1"

		elif is_theme "$ZSH/themes/$1" "$1"; then
			source "$ZSH/themes/$1/$1.zsh-theme" || \
			source "$ZSH/themes/$1/$1.theme.zsh"
			echo "Theme:  $1"

		elif is_theme "$ZSH/themes" "$1"; then
			source "$ZSH/themes/$1.zsh-theme" || \
			source "$ZSH/themes/$1.theme.zsh"
			echo "Theme:  $1"

		else
			echo "Theme  '$1' not found"
			export ZTHEME_FOUND=0
		fi
	fi
}

function _theme \
{
	_arguments "1: :($(lstheme))"
}

compdef _theme theme

function lstheme \
{
	# Resources:
	# http://zsh.sourceforge.net/Doc/Release/Expansion.html#Modifiers
	# http://zsh.sourceforge.net/Doc/Release/Expansion.html#Glob-Qualifiers
	{
		# Show themes inside $ZSH_CUSTOM (in any subfolder)
		# Strip $ZSH_CUSTOM/themes/ and $ZSH_CUSTOM/ from the name, so that it matches
		# the value that should be written in $ZSH_THEME to load the theme.
		if [[ -d $ZSH_CUSTOM ]];
		then
			print -l "$ZSH_CUSTOM"/**/*.zsh-theme(.N:r:gs:"$ZSH_CUSTOM"/themes/:::gs:"$ZSH_CUSTOM"/:::)
			print -l "$ZSH_CUSTOM"/*.zsh-theme(.N:r:gs:"$ZSH_CUSTOM"/themes/:::gs:"$ZSH_CUSTOM"/:::)
		fi

		if [[ -d $ZSH ]];
		then
			# Show themes inside $ZSH, stripping the head of the path.
			print -l "$ZSH"/themes/**/*.zsh-theme(.N:t:r)
			print -l "$ZSH"/themes/*.zsh-theme(.N:t:r)
		fi
	} | sort -u | fmt -w $COLUMNS
}
