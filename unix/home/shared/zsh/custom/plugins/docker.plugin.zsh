
if (( ! $+commands[docker] )); then
  return
fi

# Standarized $0 handling
# https://zdharma-continuum.github.io/Zsh-100-Commits-Club/Zsh-Plugin-Standard.html
# 0="${${ZERO:-${0:#$ZSH_ARGZERO}}:-${(%):-%N}}"
# 0="${${(M)0:#/*}:-$PWD/$0}"

if [[ -f "$ZSH/completions/_docker" ]];
then
	ZSH_DOCKER_COMP_FILE="$ZSH/completions/_docker"

elif [[ -f "$ZSH_SYSTEM/completions/_docker" ]];
then
	ZSH_DOCKER_COMP_FILE="$ZSH_SYSTEM/completions/_docker"

elif [[ -f "$XDG_CACHE_HOME/zsh/completions/_docker" ]];
then
	ZSH_DOCKER_COMP_FILE="$XDG_CACHE_HOME/zsh/completions/_docker"
else
	echo "Docker completion file not found..."
fi

# If the completion file doesn't exist yet, we need to autoload it and
# bind it to `docker`. Otherwise, compinit will have already done that.
if [[ -z $ZSH_DOCKER_COMP_FILE ]];
then
  typeset -g -A _comps
  autoload -Uz _docker
  _comps[docker]=_docker
else
fi

{
	[ ! -d "$XDG_CACHE_HOME/zsh/completions" ] && mkdir -pv "$XDG_CACHE_HOME/zsh/completions";
	# `docker completion` is only available from 23.0.0 on
	# docker version returns `Docker version 24.0.2, build cb74dfcd85`
	# with `s:,:` remove the comma after the version, and select third word of it
	if zstyle -t ':omz:plugins:docker' legacy-completion || \
	 ! is-at-least 23.0.0 ${${(s:,:z)"$(command docker --version)"}[3]};
	then
		command cp -v "${ZSH_DOCKER_COMP_FILE}" "$XDG_CACHE_HOME/zsh/completions/_docker";
	else
		command docker completion zsh | tee "$XDG_CACHE_HOME/zsh/completions/_docker" > /dev/null;
	fi;
} &|
