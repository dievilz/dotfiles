#!/usr/bin/env sh
# dotenv posix

#
# ~/.asdf-versions.sh
#

if [ ! -w "$HOME" ]; then
	echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
fi

[ -e "$HOME/.asdf-versions" ] && rm -fv "$HOME/.asdf-versions"
export ASDF_DEFAULT_TOOL_VERSIONS_FILENAME=".asdf_versions"

##### Always latest ####################################################
asdf global golang latest

# asdf global python latest

# asdf global mariadb latest

# asdf global deno latest

# asdf global ruby latest

##### Always latest LTS ################################################
## Production applications should only use Active LTS or Maintenance LTS
## releases. If an even-numbered release above is not marked as LTS, then
## it has not entered “Active LTS” and is not recommended for Production.
#                                #  Devel | Active | Maint. |   EOL
asdf global nodejs latest:20     # 23/Abr | 23/Oct | 24/Oct | 26/May |Recommended
# asdf global nodejs latest:18   # 22/Abr | 22/Oct | 23/Oct | 25/May |Safest bet
# asdf global nodejs latest:16   # 21/Abr | 21/Oct | 22/Oct | 23/Oct |Near EOL

##### Specific versions ################################################
# asdf global yarn 1.22.19

# asdf global java adoptopenjdk-8.0.362+9
# asdf global java adoptopenjdk-11.0.19+7
asdf global java adoptopenjdk-17.0.10+7

# asdf global maven 3.6.3
asdf global maven 3.9.6

# if [ -w "$HOME" ];
# then
# 	printf "\n%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
# fi
# echo

source "$HOME/$ASDF_DEFAULT_TOOL_VERSIONS_FILENAME"
