#!/usr/bin/env bash
# ----------------------------------------------------------------------------
# A command line tool to wrap git command to enforce a custom git commit message
# Convention based on "AngularJS Git Commit Message Conventions" described at
#
#    Https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y
#
# Author: Emac Shen
# Version: 0.1.1-20210131
#
# Author: Diego Villarreal <dievilz>
# Version: 0.2.0-20230711
# ----------------------------------------------------------------------------

## Check whether `git` executable exists
GITX_VERSION=`git version | awk '{print $3}'`
if [[ -z "$GITX_VERSION" ]] ; then
	echo "Error: cannot find git executable"
	exit 1
fi

## Skip if it's not a `git commit` command
if [[ "$1" != "commit" ]] ; then
	exec git "$@"
fi

## Skip if it contains any of the following options:
## -m/--message, -c/--reuse-message, -C/--reedit-message, -F/--file, -t/--template
if [[ ( "$*" =~ "-m" ) || ( "$*" =~ "--message" ) || ( "$*" =~ "-c" ) || ( "$*" =~ "--reuse-message" ) || ( "$*" =~ "-C" ) || ( "$*" =~ "--reedit-message" ) || ( "$*" =~ "-F" ) || ( "$*" =~ "--file" ) || ( "$*" =~ "-t" ) || ( "$*" =~ "--template" ) ]] ; then
	exec git "$@"
fi

## Loop until a non-empty value is read
loop_read() {
	local _result=$1
	local _prompt=$2
	read -p "$_prompt" $_result
	if [[ -z "${!_result}" ]] ; then
		loop_read "$@"
	fi
}

## Prompt for ticket (skippable)...
read -p "Ticket (press ENTER to skip): " GITX_TICKET
if [[ -n "$GITX_TICKET" ]] ; then
	GITX_TICKET="[$GITX_TICKET]"
fi

## Prompt for type...
loop_read GITX_TYPE_IDX "type (0:feat, 1:fix, 2:style, 3:refactor, 4:perf, 5: test, 6:revert, 7:docs, 8:build, 9:ci, 10:chore): "
case "$GITX_TYPE_IDX" in
	0) GITX_TYPE="feat" ;;
	1) GITX_TYPE="fix" ;;
	2) GITX_TYPE="style" ;;
	3) GITX_TYPE="refactor" ;;
	4) GITX_TYPE="perf" ;;
	5) GITX_TYPE="test" ;;
	6) GITX_TYPE="revert" ;;
	7) GITX_TYPE="docs" ;;
	8) GITX_TYPE="build" ;;
	9) GITX_TYPE="ci" ;;
	10) GITX_TYPE="chore" ;;
	*) echo "Error: invalid type"
		exit 1
	;;
esac

## Prompt for scope (skipable)...
read -p "Scope (press ENTER to skip): " GITX_SCOPE
if [[ -n "$GITX_SCOPE" ]] ; then
	GITX_SCOPE="($GITX_SCOPE)"
fi

## Prompt for subject...
loop_read GITX_SUBJECT "Subject: "

## Prompt for meta (skipable)...
read -p "Meta (press ENTER to skip): " GITX_META
if [[ -n "$GITX_META" ]] ; then
	GITX_META="#$GITX_META"
fi

## Prompt for body (skipppable)...
read -p "Body (Enter $ to end): " -d '$' GITX_BODY

### Compose full commit message ($GITX_MESSAGE) in the following format:

## [<ticket>]<type>(<scope>): <subject> #<meta>
## Header limit: ----------- 50-72 chars ---->|    F
## --------------- blank line -----------------      O
## <body>                                              R
## --------------- blank line -----------------          M
## <footer>                                                A
## ----------------- last line (end of message)              T
## Body limit: ------------ 72-100 chars ---->|

GITX_MESSAGE="${GITX_TICKET}${GITX_TYPE}${GITX_SCOPE}: $GITX_SUBJECT ${GITX_META}"
if [[ -n "$GITX_BODY" ]] ; then
	GITX_MESSAGE="$GITX_MESSAGE

$GITX_BODY"
fi

## Append the full commit message (-m $GITX_MESSAGE) to the original command
git "$@" -m "$GITX_MESSAGE"
