package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	if len(os.Args) < 2 {
		usage()
	}

	info, err := os.Lstat(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(1)
	}

	realPath, err := filepath.EvalSymlinks(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(1)
	}

	fmt.Println(realPath)
}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: %s PATH\n", filepath.Base(os.Args[0]))
	os.Exit(1)
}
