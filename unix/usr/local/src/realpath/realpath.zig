const std = @import("std");

pub fn main() !void {
	var gpa = std.heap.GeneralPurposeAllocator(.{}){};
	const allocator = gpa.allocator();

	var args = std.process.args();
	_ = args.next(); // Skip program name

	const path = args.next() orelse {
		std.debug.print("usage: {s} PATH\n", .{std.fs.path.basename(args.first().?)});
		return error.InvalidUsage;
	};

	const real_path = try std.fs.realpathAlloc(allocator, path);
	defer allocator.free(real_path);

	std.debug.print("{s}\n", .{real_path});
}
