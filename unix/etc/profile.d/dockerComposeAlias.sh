#!/usr/bin/sh

if command -v docker-compose > /dev/null 2>&1; then
	alias dockerCompose="docker-compose"
	alias dockerComposeAlias="docker-compose"
fi

## If macOS has the docker binary, there is a high chance that
## is because of Docker Desktop, which has docker compose plugin...
## ...Which is not the case for OSes different than macOS/Win

case $(uname -s) in
	Darwin)
		if command -v docker > /dev/null 2>&1; then
			alias dockerCompose="docker compose"
			alias dockerComposeAlias="docker compose"
		fi
	;;
	*)
		if docker compose version > /dev/null 2>&1; then
			alias dockerCompose="docker compose"
			alias dockerComposeAlias="docker compose"
		fi
	;;
esac
