#
# /etc/pacman.conf
#
# See the pacman.conf(5) manpage for option and repository directives

#
# GENERAL OPTIONS
#
[options]
### The following paths are commented out with their default values listed.
### If you wish to use different paths, uncomment and update the paths.
# RootDir     = /
# DBPath      = /var/lib/pacman/
# CacheDir    = /var/cache/pacman/pkg/
# LogFile     = /var/log/pacman.log
# GPGDir      = /etc/pacman.d/gnupg/
# HookDir     = /etc/pacman.d/hooks/
# HoldPkg     = pacman glibc
# XferCommand = /usr/bin/curl -C - -f %u > %o
# XferCommand = /usr/bin/wget --passive-ftp -c -O %o %u
# CleanMethod = KeepInstalled
# UseDelta    = 0.7
Architecture = auto

### Pacman won't upgrade packages listed in IgnorePkg and members of IgnoreGroup
# IgnorePkg   =
# IgnoreGroup =

# NoUpgrade   =
# NoExtract   =

### Misc options
# UseSyslog
Color
# NoProgressBar
CheckSpace
VerbosePkgLists
ParallelDownloads = 3
ILoveCandy

DisableDownloadTimeout



### By default, pacman accepts packages signed by keys that its local keyring
### trusts (see pacman-key and its man page), as well as unsigned packages.
SigLevel    = Required DatabaseOptional
LocalFileSigLevel = Optional
# RemoteFileSigLevel = Required

### NOTE: You must run `pacman-key --init` before first using pacman; the local
### keyring can then be populated with the keys of all official Arch Linux
### packagers with `pacman-key --populate archlinux`.

#
# REPOSITORIES
#   - can be defined here or included from another file
#   - pacman will search repositories in the order defined here
#   - local/custom mirrors can be added here or in separate files
#   - repositories listed first will take precedence when packages
#     have identical names, regardless of version number
#   - URLs will have $repo replaced by the name of the current repo
#   - URLs will have $arch replaced by the name of the architecture
#
# Repository entries are of the format:
#       [repo-name]
#       Server = ServerName
#       Include = IncludePath
#
# The header [repo-name] is crucial - it must be present and
# uncommented to enable the repo.
#

### The gremlins repositories are disabled by default. To enable, uncomment the
### repo name header and Include lines. You can add preferred servers immediately
### after the header, and they will be used before the default mirrors.

[system]
Include = /etc/pacman.d/mirrorlist

[world]
Include = /etc/pacman.d/mirrorlist

[galaxy]
Include = /etc/pacman.d/mirrorlist

# If you want to run 32 bit applications on your x86_64 system,
# enable the lib32 repositories as required here.

# [lib32]
# Include = /etc/pacman.d/mirrorlist
#
# An example of a custom package repository.  See the pacman manpage for
# tips on creating your own repositories.
# [custom]
# SigLevel = Optional TrustAll
# Server = file:///home/custompkgs

[universe]
Server = https://universe.artixlinux.org/$arch
Server = https://mirror1.artixlinux.org/universe/$arch
Server = https://mirror.pascalpuffke.de/artix-universe/$arch
Server = https://artixlinux.qontinuum.space/artixlinux/universe/os/$arch
Server = https://mirror1.cl.netactuate.com/artix/universe/$arch
Server = https://ftp.crifo.org/artix-universe/

[omniverse]
Server = https://artix.sakamoto.pl/omniverse/$arch
Server = https://eu-mirror.artixlinux.org/omniverse/$arch
Server = https://omniverse.artixlinux.org/$arch

### First sync repos now with universe... next install <artix-archlinux-support>
### If an error shows about gpg keys and shii, just use pacman-key --refresh-keys
### That should fix that error, then wget full pacman config file from my dotfiles
### Finally run `pacman-key --populate archlinux`, sync repos again and that's it.
