# Contribuir a dotfiles

Valoramos enormemente las contribuciones a este proyecto y te invitamos a unirte para perfeccionarlo y expandir su impacto. La colaboración es el núcleo de nuestra misión, y estas directrices están diseñadas para garantizar un proceso eficiente, organizado y alineado con los objetivos del proyecto, fomentando la innovación y la sostenibilidad.

1. [Código de Conducta](#coc)
2. [Consultas y Problemas](#question)
3. [Reportes de Errores](#issue)
4. [Propuestas de Nuevas Funcionalidades](#feature)
5. [Directrices para Enviar Contribuciones](#submit)
6. [Normas de Codificación](#rules)
7. [Estándares para Mensajes de Commit](#commit)

## 1. Código de Conducta

Este proyecto se rige por un Código de Conducta cuyo objetivo es promover un entorno respetuoso para todos los participantes. Fomentamos una colaboración constructiva y profesional, independientemente del nivel de experiencia o conocimiento técnico de los colaboradores. Te recomendamos encarecidamente leer el [Código de Conducta](CODE_OF_CONDUCT.md) antes de contribuir.

## 2. Consultas y Problemas

En caso de que encuentres dificultades o tengas preguntas sobre el funcionamiento del proyecto, te sugerimos revisar inicialmente la documentación existente y las preguntas frecuentes (FAQ). Si no encuentras una solución en estas fuentes, abre un "issue" en el repositorio detallando tu consulta. Incluye información relevante como el entorno de trabajo, la versión del software y cualquier error específico.

Proporcionar un reporte detallado ayudará a los mantenedores y otros colaboradores a abordar tu consulta de manera más eficiente, reduciendo posibles retrasos y mejorando la calidad de las respuestas.

## 3. Reportes de Errores

- Completa la plantilla de "issues" proporcionada, asegurándote de incluir detalles claros y precisos. Esto facilita la replicación del problema y la identificación de su causa raíz.
- Incluye una descripción exhaustiva del error, junto con los pasos necesarios para reproducirlo. Esto ayuda a los mantenedores a identificar rápidamente la causa del problema.
- Proporciona, si es posible, capturas de pantalla, registros de error y ejemplos de código relacionados con el problema reportado. Estos detalles adicionales son cruciales para diagnosticar el problema de manera eficiente y proponer soluciones efectivas.

## 4. Propuestas de Nuevas Funcionalidades

- Justifica cómo la nueva funcionalidad contribuirá al avance del proyecto. Explica su relevancia y los beneficios esperados. Intenta incluir un análisis breve del impacto potencial en los usuarios finales.
- Proporciona un esquema detallado sobre la implementación propuesta, incluyendo ejemplos, dependencias y cualquier posible impacto en el rendimiento del sistema. Detalla cómo esta funcionalidad complementa las existentes.
- Si es aplicable, compara tu propuesta con alternativas existentes para destacar sus ventajas. Esto ayuda a los revisores a comprender mejor su valor y contribución al proyecto.

## 5. Directrices para Enviar Contribuciones

1. Crea una nueva rama descriptiva utilizando un comando como `git checkout -b mi-nueva-funcion`. Asegúrate de elegir un nombre de rama que sea claro y representativo del cambio.
3. Realiza los cambios necesarios y redacta mensajes de commit claros y concisos, siguiendo los estándares previamente definidos.
4. Envía un "pull request" (PR) bien estructurado y documentado. Incluye una descripción clara del cambio, el propósito del mismo y cualquier información adicional relevante para los revisores. Recuerda abordar cualquier retroalimentación recibida de manera colaborativa.

## 6. Normas de Codificación

- Respeta las convenciones de estilo definidas por el proyecto para garantizar consistencia en el código. Un estilo uniforme mejora la legibilidad y reduce las barreras de entrada para nuevos colaboradores.
- Escribe código claro, eficiente y adecuadamente documentado. El objetivo es que otros desarrolladores puedan comprender rápidamente tus contribuciones, incluso si no están familiarizados con todos los detalles del proyecto.
- Asegúrate de incluir pruebas que validen los cambios realizados. Las pruebas ayudan a garantizar que el sistema siga funcionando correctamente después de implementar tus modificaciones.
- Revisa tu código para identificar y corregir errores antes de enviar el PR. Utiliza herramientas de análisis estático o linters para asegurar la calidad del código.

## <a name="commit"></a> 7. Estándares para Mensajes de Commits

### Aplicación de los Lineamientos para Mensajes de Commits personalizados por @dievilz

Estos lineamientos están concebidos para complementar y/o extender la especificación de [Commits Convencionales](https://www.conventionalcommits.org/en/v1.0.0/). Su propósito es mantener un historial de cambios claro y consistente, facilitando la colaboración entre desarrolladores y asegurando la transparencia en el desarrollo del proyecto. Para garantizar la adhesión a estos estándares, recomendamos herramientas como [cmt](https://github.com/smallhadroncollider/cmt) de @smallhadroncollider, el script shell [gitx](https://github.com/emac/gitx) de @emac o la herramienta [gocmt](https://github.com/dievilz/gocmt) de @dievilz.

Estos lineamientos derivan de una combinación de prácticas y recursos especializados de la industria. Además de facilitar la colaboración, ayudan a mantener un historial claro, lo que resulta fundamental para la sostenibilidad del proyecto a largo plazo. Los enlaces relevantes están disponibles al [final de esta sección](#commit-links) para consulta adicional.

### Estructura y Composición del Mensaje de Commit

Un mensaje de commit debe estructurarse en tres secciones separadas por una línea en blanco. Esta estructura ayuda a distinguir entre el propósito general del cambio, detalles técnicos y consideraciones adicionales

1. **Encabezado**
2. **Cuerpo** (opcional)
3. **Pie** (opcional).

```
[<ticket>] <tipo>(<alcance>): <tema> #<meta>
(Límite de Encabezado: 50-72 caracteres) ->|        F
          <línea vacía>                      \        O
             <cuerpo>                          |        R
(Límite del Cuerpo: 72-100 caracteres) ------->|          M
          <línea vacía>                        |            A
              <pie>                            |              T
(Límite del Pie: 72-100 caracteres) ---------->|                O
<Última línea vacía (final de mensaje)>
```

### Explicación del Encabezado

El encabezado es una línea concisa que sintetiza el cambio realizado. Debe ser claro, corto y que comunique el propósito del commit de manera directa. Idealmente, resume la esencia del cambio en términos claros y sencillos.

Consiste de 5 partes que se detallaran a continuación:
`[<ticket>] <tipo>(<alcance>): <tema> #<meta>`

#### Tipo (Obligatorio)

El **tipo** es la parte del encabezado que debe usar una palabra o abreviatura única extraída de una ontología, según la naturaleza del proyecto.
Asegúrate de utilizar las siguientes categorías (en inglés), cada una diseñada para reflejar el propósito exacto del cambio. Esto ayuda a mantener una coherencia en el historial del proyecto y facilita la navegación por los registros de cambios:

* **`mod`:**          Los cambios cotidianos en el código que NO son: correcciones de ningún tipo, cambios de estilo, refactorización o cualquier otra cosa. Este debería ser el tipo más común mostrado en el gráfico de commits. Si no te gusta, también se puede usar `edit`, pero usa sólo uno de forma consistente.
* **`blob`:**         Un archivo recién creado. Es mejor cuando sólo se crea un archivo vacío en lugar de un archivo casi terminado. Se puede incluir el metatag _#wip_. Puede usarse también al cambiar los permisos de archivos.
* **`tree`:**         Como el anterior, pero para directorios. Puede incluir la metatag _#wip_. Puede usarse también al cambiar los permisos de directorios.
* **`fix`:**          Para bugs o _hot fixes_.
* **`style`:**        Ajustes estéticos que no alteran el comportamiento funcional del código (espaciado, comentarios, indentación, faltas de puntuacion, errores tipográficos, etc). Esto incluye mejoras de legibilidad y consistencia visual del código, facilitando su mantenimiento.
* **`refactor`:**     Reformulaciones del código sin impacto en la funcionalidad, orientadas a mejorar la claridad, estructura y mantenibilidad del código base (cambios a bloques de código, funciones o rutinas, estructura del código). Estos cambios deberían simplificar el funcionamiento y podrían mejorar el rendimiento. No arreglan errores ni añaden nuevas funcionalidades.
* **`feat`:**         Introducción de una nueva funcionalidad. Este tipo de commit señala cambios importantes que aumentan las capacidades del sistema, alineándose con los objetivos generales del proyecto. Se debe utilizar este tipo solamente cuando se haga un merge de una _feature branch_ o para cambios en un conjunto de varios archivos que sólo contienen nuevas funcionalidades. Puede ser usado para un _squash commit_ también.
* **`docs`:**         Cambios exclusivamente a la documentación o archivos de no-código.
* **`test`:**         Incorporación o modificación de archivos de pruebas.
* **`build`:**        Cambios en el proceso de construcción/compilación/empaquetado o herramientas auxiliares como la generación de documentación o dependencias externas.
* **`devops/ci-cd`:** Cambios en la configuración de **_Integración/Entrega Continua_**, archivos, scripts, etc. Se puede usar `devops` para un espectro más amplio de este tipo de cambios o puede usarse en lugar de `ci-cd`, pero siempre de manera consistente.
* **`notice`:**       Cambios para anunciar/advertir sobre cualquier cosa relacionada con: archivos, bloques de código, etc.
* **`chore`:**        Para cualquier otra tarea repetitiva y periódica (como limpiar partes obsoletas o actualizar versiones de cosas). Si es algo que un bot podría haber hecho en lugar de los desarrolladores, probablemente sea una tarea repetitiva. Puede usarse también como un metatag para refactorizaciones repetitivas, por ejemplo, agregar un nuevo alias a un archivo `.aliases`.

* **`revert`:**       Si el commit revierte un commit anterior, debe comenzar con _"revert:"_, seguido del asunto del commit revertido. En el cuerpo debe decir: "Este reverte el commit `<hash>`.", donde el hash es el SHA del commit que se está revirtiendo y explicar el/los motivo(s), y en el pie de página decir: "Reverts `<hash>`". Por ejemplo:

    ```
    revert: incluir más detalles en el texto de ayuda de la línea de comandos:
    ---------- línea en blanco -----------
    Esto revierte el commit 5b233b5a debido a...
    --------- línea en blanco -----------
    Revierte 5b233b5a
    ```

El Encabezado del commit puede utilizar prefijos para fines de integración continua.

> Por ejemplo, [Jira](https://bigbrassband.com/git-for-jira.html)
> requiere un ticket al principio del mensaje del commit:<br><br>
> `[LHJ-16] fix(compile): agregar pruebas unitarias para Windows`
> <br><br>


Esta es una ontología personalizada. Para mayor información en una ontología más utilizada, consulta la especificación completa de [Commits Convencionales](https://www.conventionalcommits.org/en/v1.0.0/).

#### Alcance (Opcional)
Usualmente es conveniente mencionar exactamente qué parte de la base de código cambió. El **alcance** es la parte del encabezado responsable de proporcionar esa información. Aunque la granularidad del alcance puede variar, es importante que forme parte del "lenguaje común" hablado en el proyecto.

Ten en cuenta que en algunos casos el alcance es naturalmente demasiado amplio, por lo que no vale la pena mencionarlo. `<TIPO>` y `<ALCANCE>` pueden excluirse mutuamente.

```
feat(auth): introducir inicio de sesión a través de GitHub
```

#### Tema (Obligatorio)

El **tema** es la parte del encabezado que debe contener una descripción concisa de los cambios.

* Límite opcional: **50** caracteres. Límite estricto: **72** caracteres.
* Usa el tiempo infinitivo para describir principalmente el comportamiento del programa después del commit, por ejemplo: verbos como _“cambiar”_. Evita describir tu _comportamiento de codificación_.
* Puede ser prefijado para fines de CI/CD.
* Comienza el encabezado con minúsculas (excepto en nombres propios o acrónimos reconocidos). Esta práctica promueve un estilo uniforme y profesional.
* No termines la línea del tema con un punto.

```
refactor: mover la estructura de carpetas al layout del directorio src
```

#### Meta (Opcional)

Al final del encabezado se pueden agregar **hashtags** para facilitar la generación de changelogs y bisectación:

* **`#wip`**: La característica que se está implementando no está completa aún. No debe ser incluida en los changelogs (solo el último commit de una característica va al changelog).

* **`#irrelevant`**: El commit no agrega información útil. Se usa cuando se corrigen errores tipográficos por ejemplo. No debe ser incluido en los changelogs.

```
blob: add TODO markdown file #wip #irrelevant
```

### Explicación del Cuerpo (Opcional)

Incluye la motivación para el cambio y contrasta con el comportamiento anterior para ilustrar el impacto del cambio.

* Límite opcional: **72** caracteres. Límite estricto: **100** caracteres.
* Usa el tiempo infinitivo o presente: “cambiar”, no “cambió” ni “cambia”.
* Usa el cuerpo para explicar el _Qué_ y el _Por qué_, no el _Cómo_.
* Manténlo simple, a prueba de futuro y amigable para desarrolladores junior.
* Se puede aplicar sintaxis de marcado como Markdown.
    * Para encabezados simples, escribe un espacio (para que Git no lo interprete como comentario), luego usa `H5` o `H6`: ` ##### <header>`
* Consulta [1](#commit-link-1) y [2](#commit-link-2) para más información.

Directivas opcionales para escribir el cuerpo del commit son las siguientes.

* Por qué hiciste el cambio en primer lugar:
    * La forma en que funcionaban las cosas antes del cambio (lo qué estaba mal con eso),
    la forma en que funciona ahora, y por qué decidiste resolverlo de la forma que lo hiciste.<br>
    Consulta [3](#commit-link-3).
* También puedes explicar los mismos cambios desde 4 perspectivas diferentes:
    * Desde la perspectiva del usuario: Una descripción de cómo un usuario vería
    el comportamiento incorrecto del programa, pasos para reproducir un error, objetivos del usuario que
    aborda el commit, lo que pueden ver, quiénes están afectados.
    * Desde la perspectiva del gerente: Elecciones de diseño, tu creatividad, por qué
    hiciste los cambios.
    * Desde la perspectiva del código: Un resumen línea por línea, función por función o
    archivo por archivo.
    * Desde la perspectiva de git: Cualquier commit relacionado en este o en otro repositorio,
    especialmente si estás revirtiendo cambios anteriores; problemas relacionados de GitHub.<br>
    Consulta [4](#commit-link-4).

```
feat($browser): add onUrlChange event (popstate/hashchange/polling)

##### New $browser event:
 - forward popstate event if available
 - forward hashchange event if popstate not available
 - do polling when neither popstate nor hashchange available

Breaks $browser.onHashChange, which was removed (use onUrlChange instead)
```

### Explicación del Pie (Opcional)

* Todos los cambios que rompen la compatibilidad o las deprecaciones deben mencionarse en el pie de página con la
  descripción del cambio, justificación y notas de migración.

	```
  Rompe $browser.onHashChange, que fue eliminado (usa onUrlChange en su lugar)
	```

* Referencia a problemas: los errores cerrados deben listarse en una línea separada en el
pie de página precedidos por la palabra clave "Closes".

	```
	Cierra #123
	Cierra #123, #245, #992
	```

### Extras

#### Generar archivo CHANGELOG.md

Los changelogs pueden contener tres secciones: **nuevas características**, **correcciones de errores**, **cambios incompatibles**. Esta lista podría generarse mediante un script al hacer un lanzamiento, junto con los enlaces a los commits relacionados. Por supuesto, puedes editar este changelog antes del lanzamiento real, pero podría generar el esquema inicial.

* Lista de todos los encabezados (primeras líneas del mensaje de commit) desde la última li:

  ```
  git log <last tag> HEAD --pretty=format:%s
  ```

* Nuevas características en este lanzamiento:

  ```
  git log <last release> HEAD --grep feat
  ```

### <a name="commit-links"></a> Links

Estas directrices están basadas directamente en el [gist](https://gist.github.com/abravalheri/34aeb7b18d61392251a2) de @abravalheri, que a su vez es una versión extendida del [gist](https://gist.github.com/stephenparish/9941e89d80e2bc58a153) de @stephenparish.<br>
_(Los enlaces [1](#commit-link-1) y [2](#commit-link-2) son referenciados en ambos gists y tomé algunas cosas de [3](#commit-link-3) y [4](#commit-link-4))_

1. Artículo de @abizern (365git): <a name="commit-link-1"></a>[http://365git.tumblr.com/post/3308646748/writing-git-commit-messages](http://365git.tumblr.com/post/3308646748/writing-git-commit-messages)
2. Artículo de @tpope: <a name="commit-link-2"></a>[http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)
3. Artículo de @chris.beams: <a name="commit-link-3"></a>[https://chris.beams.io/posts/git-commit/#why-not-how](https://7chris.beams.io/posts/git-commit/#why-not-how)
4. Artículo de @joshuatauberer: <a name="commit-link-4"></a>[https://medium.com/@joshuatauberer/write-joyous-git-commit-messages-2f98891114c4](https://medium.com/@joshuatauberer/write-joyous-git-commit-messages-2f98891114c4).

Las directrices de @abravalheri y @stephenparish están basadas en las [Directrices de Commit del proyecto AngularJS](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md).<br>

---

Agradecemos profundamente tu disposición para mejorar **backend-vales-de-gasolina-v2**. Este proyecto es un esfuerzo colaborativo, y cada contribución, por pequeña que parezca, nos acerca a un producto más robusto, eficiente y valioso. Juntos podemos llevar este proyecto a nuevas alturas, beneficiando a una comunidad más amplia y diversificada.
