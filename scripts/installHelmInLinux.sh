
# curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null;
# sudo apt-get install apt-transport-https --yes;
# echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list;
# sudo apt-get update;
# sudo apt-get install helm;

case $(id -u) \
in
	0)
		curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg > /dev/null;
		apt-get install apt-transport-https --yes;

		# Add the repository to Apt sources
		case "$(cat /etc/os-release 2>/dev/null || cat /etc/lsb-release 2>/dev/null)" \
		in
			*debian*)
				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" \
				| tee /etc/apt/sources.list.d/helm-stable-debian.list;
			;;
			*ubuntu*)
				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/ubuntu/ all main" \
				| tee /etc/apt/sources.list.d/helm-stable-ubuntu.list;
			;;
			*) echo "Other" ;;
		esac

		apt-get update;
		apt-get install helm;
	;;
	*)
		curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null;
		sudo apt-get install apt-transport-https --yes;

		# Add the repository to Apt sources
		case "$(cat /etc/os-release 2>/dev/null || cat /etc/lsb-release 2>/dev/null)" \
		in
			*debian*)
				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" \
				| sudo tee /etc/apt/sources.list.d/helm-stable-debian.list;
			;;
			*ubuntu*)
				echo \
					"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/ubuntu/ all main" \
				| sudo tee /etc/apt/sources.list.d/helm-stable-ubuntu.list;
			;;
			*) echo "Other" ;;
		esac

		sudo apt-get update;
		sudo apt-get install helm;
	;;
esac
