#!/usr/bin/env sh

printf "\n%b" "\033[38;34m==> Installing Docker in Linux...\033[0m\n";

unset os_name

case $(id -u) \
in
	0)
		if [ ${INSTALL_DOCKER_IN_LINUX_UNINSTALL_PKGS:-0} -eq 1 ]; then
			printf "\n%b" "\033[38;36m==> Uninstalling any conflicting packages first...\033[0m\n";
			for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do
				apt remove $pkg;
			done
		fi

		{
		printf "\n%b" "\033[38;36m==> Adding Docker's official GPG key...\033[0m\n";

		find /var/lib/apt/periodic/update-success-stamp -mmin +60 -exec sudo apt update \;
		apt install -y ca-certificates curl gnupg;
		mkdir -vm 0755 /etc/apt/keyrings;
		# install -vm 0755 -d /etc/apt/keyrings;

		os_name=$(grep '^NAME=' /etc/os-release | cut -d= -f2 | sed 's/"//g' | tr '[:upper:]' '[:lower:]' || \
		grep '^DISTRIB_DESCRIPTION=' /etc/lsb-release | cut -d= -f2 | sed 's/"//g' | tr '[:upper:]' '[:lower:]')

		case "$os_name" in
			*debian*)
				curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
			;;
			*ubuntu*)
				curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
			;;
			*) echo "Other" ;;
		esac
		chmod -v a+r /etc/apt/keyrings/docker.gpg;

		printf "\n%b" "\033[38;36m==> Add the Official repository to apt sources...\033[0m\n";
		case "$os_name" in
			*debian*)
				echo \
					"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
					"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
					tee /etc/apt/sources.list.d/docker.list > /dev/null;
			;;
			*ubuntu*)
				echo \
					"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
					"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
					tee /etc/apt/sources.list.d/docker.list > /dev/null;
			;;
			*) echo "Other" ;;
		esac

		printf "\n%b" "\033[38;36m==> Installing Docker packages...\033[0m\n";
		apt update;
		apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin;

		printf "\n%b" "\033[38;36m==> Running the Hello-World container...\033[0m\n";
		docker run --name hello-world hello-world;
		}
	;;
	*)
		if [ ${INSTALL_DOCKER_IN_LINUX_UNINSTALL_PKGS:-0} -eq 1 ]; then
			printf "\n%b" "\033[38;36m==> Uninstalling any conflicting packages first...\033[0m\n";
			for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do
				sudo apt remove $pkg;
			done
		fi

		{
		printf "\n%b" "\033[38;36m==> Adding Docker's official GPG key...\033[0m\n";

		find /var/lib/apt/periodic/update-success-stamp -mmin +60 -exec sudo apt update \;
		sudo apt install -y ca-certificates curl gnupg;
		sudo mkdir -vm 0755 /etc/apt/keyrings;
		# sudo install -vm 0755 -d /etc/apt/keyrings;

		os_name=$(grep '^NAME=' /etc/os-release | cut -d= -f2 | sed 's/"//g' | tr '[:upper:]' '[:lower:]' || \
		grep '^DISTRIB_DESCRIPTION=' /etc/lsb-release | cut -d= -f2 | sed 's/"//g' | tr '[:upper:]' '[:lower:]')

		case "$os_name" in
			*debian*)
				curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
			;;
			*ubuntu*)
				curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg;
			;;
			*) echo "Other" ;;
		esac
		sudo chmod -v a+r /etc/apt/keyrings/docker.gpg;

		printf "\n%b" "\033[38;36m==> Add the Official repository to apt sources...\033[0m\n";
		case "$os_name" in
			*debian*)
				echo \
					"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
					"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
					sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;
			;;
			*ubuntu*)
				echo \
					"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
					"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
					sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;
			;;
			*) echo "Other" ;;
		esac

		printf "\n%b" "\033[38;36m==> Installing Docker packages...\033[0m\n";
		sudo apt update;
		sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin;

		printf "\n%b" "\033[38;36m==> Running the Hello-World container...\033[0m\n";
		sudo docker run --name hello-world hello-world;
		}
	;;
esac
