#!/bin/bash
# curl https://gitlab.com/dievilz/dotfiles/raw/master/scripts/artixBtrfs.sh -o ~/artixBtrfs.sh;

# 0 - Log in as root (password is artix)
# sudo su

artixBtrfs_unfunctns() \
{
	set -- artixBtrfs_InitializeScript artixBtrfs_FormatPartitions \
	artixBtrfs_MountPartitions artixBtrfs_JustMountPartitions \
	artixBtrfs_BootstrapDistro artixBtrfs_ChrootInitialConfig \
	artixBtrfs_ChrootBootloaderLimine artixBtrfs_ChrootBootloaderRefind \
	artixBtrfs_ChrootLastConfig;

	case "$(ps -p $$ | grep -E -m 1 -o '\b\w{0,6}sh|koi')" \
	in
		"bash")
			if [ "$#" -gt 0 ]; then
				while [ -n "$1" ]; do unset -f "$1"; shift; done
			fi
		;;
		"zsh")
			if [ "$#" -gt 0 ]; then
				while [ -n "$1" ]; do unfunction "$1"; shift; done
			fi
		;;
	esac
	echo;
}

artixBtrfs_InitializeScript() \
{
	loadkeys es;
	setfont lat2-16 -m 8859-2;

	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/home/zshrc -o ~/.zshrc;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/home/aliases -o ~/.aliases;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/home/profile -o ~/.profile;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/home/vimrc -o ~/.vimrc;

	mv /etc/pacman.conf /etc/og-pacman.conf;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/etc/pacman.d/artix/default-pacman.conf \
		-o /etc/pacman.conf;

	pacman -Sy tmux vim tree openssh;

	# Start the ssh daemon
	ln -s /etc/runit/sv/sshd/ /etc/runit/runsvdir/default/;
	sv start sshd;

	# 1 - SSH
	# This isn't necessary but if you ssh into the computer all the other steps are copy and paste
	# Get network access
	# connmanctl

	# """
	# # First, list all technologies:
	# connmanctl> technologies
	# # Then, enable technology if it is not enabled
	# connmanctl> enable <technology>
	# # Then, to scan for services:
	# connmanctl> scan <technology>
	# # You can then list all available services:
	# connmanctl> services
	# # Finally, to connect to a service (use tab completion):
	# connmanctl> connect <service>
	# """
}

artixBtrfs_FormatPartitions() \
{
	swapoff -a;
	umount -vR /mnt/artix;

	# 2 - Partitioning:
	# cfdisk /dev/nvme0n1
	# sda1 = /boot, sda2 = root
	# /dev/nvme0n1p1    512M          EFI System
	# /dev/nvme0n1p2    (the rest)    Linux Filesystem

	# 3 - Formatting the partitions:
	# the first one is our ESP partition, so for now we just need to format it
	mkfs.vfat -F 32 -n "ESP" /dev/nvme0n1p1;
	mkfs.btrfs -f -L "ROOT" /dev/nvme0n1p2;
}

artixBtrfs_MountPartitions() \
{
	[ ! -d /mnt/artix ] && mkdir -pv /mnt/artix;

	# 4 - Create and Mount Subvolumes
	# Create subvolumes for root, home, the package cache, snapshots and the entire Btrfs file system
	mount -v /dev/nvme0n1p2 /mnt/artix;

	btrfs sub create /mnt/artix/@artix;
	btrfs sub create /mnt/artix/@home;
	btrfs sub create /mnt/artix/@swap;
	btrfs sub create /mnt/artix/@btrfs;
	btrfs sub create /mnt/artix/@srv;
	btrfs sub create /mnt/artix/@log;
	btrfs sub create /mnt/artix/@tmp;
	btrfs sub create /mnt/artix/@abs;
	btrfs sub create /mnt/artix/@pkg;
	btrfs sub create /mnt/artix/@snapshots;

	umount -vR /mnt/artix;

	echo;

	# Mount the subvolumes
	# Iterate over each folder
	ABTRF_BTRFSFOLDERS=("/mnt/artix" "/mnt/artix/boot" "/mnt/artix/home" "/mnt/artix/swap" "/mnt/artix/btrfs" "/mnt/artix/srv" "/mnt/artix/tmp" "/mnt/artix/.snapshots" "/mnt/artix/var/log" "/mnt/artix/var/tmp" "/mnt/artix/var/abs" "/mnt/artix/var/cache/pacman/pkg")

	for abtrf_folder in "${ABTRF_BTRFSFOLDERS[@]}";
	do
	if mount | grep -q "$(df -P "$abtrf_folder" | awk 'NR==2 {print $1}') on $abtrf_folder";
	then
		echo "$abtrf_folder is already mounted."
	else
		echo "$abtrf_folder is not mounted. Mounting..."
		# Mount the folder here if needed
		# Change space_cache to space_cache=v2 if there are errors
		case "$abtrf_folder" \
		in
		"/mnt/artix") [ ! -d "/mnt/artix" ] && mkdir -pv /mnt/artix;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@artix /dev/nvme0n1p2 /mnt/artix
		;;
		"/mnt/artix/boot") [ ! -d "/mnt/artix/boot" ] && mkdir -pv /mnt/artix/boot;
			# Mount the EFI partition
			if mount | grep -q "/dev/nvme0n1p1 on /mnt/artix/boot"; then
				echo "Device is already mounted."
			else
				echo "Device is not mounted. Mounting..."
				mount -v /dev/nvme0n1p1 /mnt/artix/boot
			fi
		;;
		"/mnt/artix/home") [ ! -d "/mnt/artix/home" ] && mkdir -pv /mnt/artix/home;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@home /dev/nvme0n1p2 /mnt/artix/home
		;;
		"/mnt/artix/swap") [ ! -d "/mnt/artix/swap" ] && mkdir -pv /mnt/artix/swap;
			mount -vo compress=no,space_cache=v2,ssd,discard=async,subvol=@swap /dev/nvme0n1p2 /mnt/artix/swap

			# Create Swapfile
			ABTRF_SWAPFILE=/mnt/artix/swap/swapfile

			if [ -d /mnt/artix/swap ] && [ ! -f "$ABTRF_SWAPFILE" ]; then
				truncate -s 0 "$ABTRF_SWAPFILE"
				chattr +C "$ABTRF_SWAPFILE"
				btrfs property set "$ABTRF_SWAPFILE" compression none
				fallocate -l 16G "$ABTRF_SWAPFILE"
				chmod 600 "$ABTRF_SWAPFILE"
				mkswap "$ABTRF_SWAPFILE"
				swapon "$ABTRF_SWAPFILE"
			else
				swapon "$ABTRF_SWAPFILE"
			fi
		;;
		"/mnt/artix/btrfs") [ ! -d "/mnt/artix/btrfs" ] && mkdir -pv /mnt/artix/btrfs;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvolid=5 /dev/nvme0n1p2 /mnt/artix/btrfs
		;;
		"/mnt/artix/srv") [ ! -d "/mnt/artix/srv" ] && mkdir -pv /mnt/artix/srv;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@srv /dev/nvme0n1p2 /mnt/artix/srv
		;;
		"/mnt/artix/tmp") [ ! -d "/mnt/artix/tmp" ] && mkdir -pv /mnt/artix/tmp;
			mount -v -t tmpfs -o rw,nosuid,nodev,exec,auto,nouser,async,noatime,mode=1777,size=8G /dev/nvme0n1p2 /mnt/artix/tmp
		;;
		"/mnt/artix/.snapshots") [ ! -d "/mnt/artix/.snapshots" ] && mkdir -pv /mnt/artix/.snapshots;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@snapshots /dev/nvme0n1p2 /mnt/artix/.snapshots
		;;
		"/mnt/artix/var/log") [ ! -d "/mnt/artix/var/log" ] && mkdir -pv /mnt/artix/var/log;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@log /dev/nvme0n1p2 /mnt/artix/var/log
		;;
		"/mnt/artix/var/tmp") [ ! -d "/mnt/artix/var/tmp" ] && mkdir -pv /mnt/artix/var/tmp;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@tmp /dev/nvme0n1p2 /mnt/artix/var/tmp
		;;
		"/mnt/artix/var/abs") [ ! -d "/mnt/artix/var/abs" ] && mkdir -pv /mnt/artix/var/abs;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@abs /dev/nvme0n1p2 /mnt/artix/var/abs
		;;
		"/mnt/artix/var/cache/pacman/pkg") [ ! -d "/mnt/artix/var/cache/pacman/pkg" ] && mkdir -pv /mnt/artix/var/cache/pacman/pkg;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@pkg /dev/nvme0n1p2 /mnt/artix/var/cache/pacman/pkg
		;;
		esac
	fi
	echo
	done
}

artixBtrfs_JustMountPartitions() \
{
	# Mount the subvolumes
	# Iterate over each folder
	ABTRF_BTRFSFOLDERS=("/mnt/artix" "/mnt/artix/boot" "/mnt/artix/home" "/mnt/artix/swap" "/mnt/artix/btrfs" "/mnt/artix/srv" "/mnt/artix/tmp" "/mnt/artix/.snapshots" "/mnt/artix/var/log" "/mnt/artix/var/tmp" "/mnt/artix/var/abs" "/mnt/artix/var/cache/pacman/pkg")

	for abtrf_folder in "${ABTRF_BTRFSFOLDERS[@]}";
	do
	if mount | grep -q "$(df -P "$abtrf_folder" | awk 'NR==2 {print $1}') on $abtrf_folder";
	then
		echo "$abtrf_folder is already mounted."
	else
		echo "$abtrf_folder is not mounted. Mounting..."
		# Mount the folder here if needed
		# Change space_cache to space_cache=v2 if there are errors
		case "$abtrf_folder" \
		in
		"/mnt/artix") [ ! -d "/mnt/artix" ] && mkdir -pv /mnt/artix;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@artix /dev/nvme0n1p2 /mnt/artix
		;;
		"/mnt/artix/boot") [ ! -d "/mnt/artix/boot" ] && mkdir -pv /mnt/artix/boot;
			# Mount the EFI partition
			if mount | grep -q "/dev/nvme0n1p1 on /mnt/artix/boot"; then
				echo "Device is already mounted."
			else
				echo "Device is not mounted. Mounting..."
				mount -v /dev/nvme0n1p1 /mnt/artix/boot
			fi
		;;
		"/mnt/artix/home") [ ! -d "/mnt/artix/home" ] && mkdir -pv /mnt/artix/home;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@home /dev/nvme0n1p2 /mnt/artix/home
		;;
		"/mnt/artix/swap") [ ! -d "/mnt/artix/swap" ] && mkdir -pv /mnt/artix/swap;
			mount -vo compress=no,space_cache=v2,ssd,discard=async,subvol=@swap /dev/nvme0n1p2 /mnt/artix/swap

			# Create Swapfile
			ABTRF_SWAPFILE=/mnt/artix/swap/swapfile
			[ -f "$ABTRF_SWAPFILE" ] && swapon $ABTRF_SWAPFILE
		;;
		"/mnt/artix/btrfs") [ ! -d "/mnt/artix/btrfs" ] && mkdir -pv /mnt/artix/btrfs;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvolid=5 /dev/nvme0n1p2 /mnt/artix/btrfs
		;;
		"/mnt/artix/srv") [ ! -d "/mnt/artix/srv" ] && mkdir -pv /mnt/artix/srv;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@srv /dev/nvme0n1p2 /mnt/artix/srv
		;;
		"/mnt/artix/tmp") [ ! -d "/mnt/artix/tmp" ] && mkdir -pv /mnt/artix/tmp;
			mount -v -t tmpfs -o rw,nosuid,nodev,exec,auto,nouser,async,noatime,mode=1777,size=8G /dev/nvme0n1p2 /mnt/artix/tmp
		;;
		"/mnt/artix/.snapshots") [ ! -d "/mnt/artix/.snapshots" ] && mkdir -pv /mnt/artix/.snapshots;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@snapshots /dev/nvme0n1p2 /mnt/artix/.snapshots
		;;
		"/mnt/artix/var/log") [ ! -d "/mnt/artix/var/log" ] && mkdir -pv /mnt/artix/var/log;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@log /dev/nvme0n1p2 /mnt/artix/var/log
		;;
		"/mnt/artix/var/tmp") [ ! -d "/mnt/artix/var/tmp" ] && mkdir -pv /mnt/artix/var/tmp;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@tmp /dev/nvme0n1p2 /mnt/artix/var/tmp
		;;
		"/mnt/artix/var/abs") [ ! -d "/mnt/artix/var/abs" ] && mkdir -pv /mnt/artix/var/abs;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@abs /dev/nvme0n1p2 /mnt/artix/var/abs
		;;
		"/mnt/artix/var/cache/pacman/pkg") [ ! -d "/mnt/artix/var/cache/pacman/pkg" ] && mkdir -pv /mnt/artix/var/cache/pacman/pkg;
			mount -vo noatime,nodiratime,compress=zstd,commit=120,space_cache=v2,ssd,discard=async,autodefrag,subvol=@pkg /dev/nvme0n1p2 /mnt/artix/var/cache/pacman/pkg
		;;
		esac
	fi
	echo
	done
}

artixBtrfs_BootstrapDistro() \
{
	# 5 Base System and /etc/fstab
	# (this is the time where you change the mirrorlist, if that's your thing)
	# (Optional) Enable parallel downloads for basestrap
	sed -i 's/^#ParallelDownloads.*/ParallelDownloads = 5/' /etc/pacman.conf;

	# Base system packages
	basestrap /mnt/artix base base-devel runit seatd-runit linux-firmware btrfs-progs \
		zstd tree neovim micro zsh git man chrony-runit cronie-runit sudo opendoas;

	# Kernel - you can choose any (linux linux-lts linux-hardened linux-zen etc...)
	basestrap /mnt/artix linux-zen linux-zen-headers;

	# CPU Microcode (replace with intel-ucode if using intel CPU)
	basestrap /mnt/artix amd-ucode;

	# AMD GPU drivers
	basestrap /mnt/artix mesa vulkan-radeon libva-mesa-driver mesa-vdpau xf86-video-amdgpu;

	# Nvidia GPU drivers
	# Replace nvidia-dkms with:
	# nvidia      - if using linux kernel
	# nvidia-lts  - if using linux-lts kernel
	# Otherwise (using linux-zen), leave nvidia-dkms
	#
	# There is also lib32-nvidia-utils and lib32-nvidia-libgl packages
	# available in lib32/multilib repos. Also if you choose to install
	# other init system, there is nvidia-utils-initsystem available,
	# for example nvidia-utils-openrc
	basestrap /mnt/artix nvidia-dkms nvidia-settings nvidia-utils nvidia-libgl;

	# Arch repositories support
	basestrap /mnt/artix archlinux-mirrorlist artix-archlinux-support;

	# Bootloader, AUR packages and packages that are not in
	# Artix repositories will be installed later on

	# generate the fstab
	fstabgen -U /mnt/artix > /mnt/artix/etc/fstab;

	# add /tmp as tmpfs in fstab
	cat << EOF >> /mnt/artix/etc/fstab
tmpfs	/tmp	tmpfs	rw,nosuid,nodev,exec,auto,nouser,async,noatime,mode=1777	0 0
EOF

	cp -v $HOME/artixBtrfs.sh /mnt/artix/root;
}

artixBtrfs_ChrootInitialConfig() \
{
	# 6 Initial System Configuration #######################################################
	# chroot into the new system
	# artix-chroot /mnt/artix

	# - set locale #########################################################
	export LANG="en_US.UTF-8";
	export LC_COLLATE="C";
	# Replace Europe/London with your Region/City
	export TZ="America/Monterrey";
	# Replace hostname with the name for your host
	export HOSTNAME=artixPC;


	echo "KEYMAP=es" > /etc/vconsole.conf;
	# sed -i -E 's/^#?(KEYMAP.*)$/\1/g' /etc/vconsole.conf

	echo 'LANG="$LANG"' > /etc/locale.conf;
	# sed -i -E 's/^#?(LANG.*)$/\1/g' /etc/locale.conf

	# echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
	sed -i -E 's/^#?(en_US\.UTF-8.*)$/\1/g' /etc/locale.gen;
	locale-gen;

	# - set hostname #######################################################
	echo $HOSTNAME > /etc/hostname;

	# - set hosts ##########################################################
	cat << EOF >> /etc/hosts

127.0.0.1	localhost
::1		localhost
127.0.1.1	$HOSTNAME.localdomain	$HOSTNAME
EOF

	# - set root password ##################################################
	printf "%b" "Set root passwd below: ";
	passwd;

	chsh -s /usr/bin/zsh;
	echo;

	# - set timezone #######################################################
	ln -fsv /usr/share/zoneinfo/"$TZ" /etc/localtime;
	hwclock --systohc;

	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/etc/sudo/essentials \
		-o /etc/sudoers.d/essentials;

	# - Preventing snapshot slowdowns
	echo 'PRUNENAMES = ".snapshots"' >> /etc/updatedb.conf;

	# - fix the mkinitcpio.conf to contain what we actually need. ##########

	sed -i 's/^BINARIES=()/BINARIES=("\/usr\/bin\/btrfs")/' /etc/mkinitcpio.conf;
	# If using amdgpu and would like earlykms
	# sed -i 's/MODULES=()/MODULES=(amdgpu)/' /etc/mkinitcpio.conf
	# If using nvidia and would like earlykms
	sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf;
	sed -i 's/^#COMPRESSION="lz4"/COMPRESSION="lz4"/' /etc/mkinitcpio.conf;
	sed -i 's/^#COMPRESSION_OPTIONS=()/COMPRESSION_OPTIONS=(-9)/' /etc/mkinitcpio.conf;
	# if you have more than 1 btrfs drive
	# sed -i 's/^HOOKS/HOOKS=(base udev autodetect modconf block resume btrfs filesystems keyboard fsck)/' /etc/mkinitcpio.conf
	# else
	sed -i 's/^HOOKS.*/HOOKS=(base udev autodetect modconf block resume filesystems keyboard fsck)/' /etc/mkinitcpio.conf;

	# Replace with your kernel
	mkinitcpio -p linux-zen;

	# 7 Add Arch repositories
	mv /etc/pacman.conf /etc/og-pacman.conf;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/etc/pacman.d/artix/default-pacman.conf \
		-o /etc/pacman.conf;

	pacman -Sy dhcpcd-runit;
	ln -s /etc/runit/sv/dhcpcd/ /etc/runit/runsvdir/default/;
	sv start dhcpcd;

	mv /etc/pacman.conf /etc/default-pacman.conf;
	curl https://gitlab.com/dievilz/dotfiles/raw/master/unix/etc/pacman.d/artix/pacman.conf \
		-o /etc/pacman.conf;

	pacman -Sy connman-runit openssh-runit;
	ln -sv /etc/runit/sv/sshd/ /etc/runit/runsvdir/default/;
	sv start sshd;
	ln -sv /etc/runit/sv/connmand/ /etc/runit/runsvdir/default/;
	sv start connmand;

	## 8 Optimize Makepkg
	## Install compressors and bootloader
	pacman -Syy pigz pbzip2;

	## Configure Makepkg
	## Use these commands to configure /etc/makepkg.conf
	perl -i -0777 -pe 's/^CFLAGS=".*?"/CFLAGS="-march=native -mtune=native -O2 -pipe -fstack-protector-strong --param=ssp-buffer-size=4 -fno-plt"/sm' /etc/makepkg.conf;
	sed -i 's/^CXXFLAGS.*/CXXFLAGS="\$CFLAGS"/' /etc/makepkg.conf;
	sed -i 's/^#RUSTFLAGS.*/RUSTFLAGS="-C opt-level=2 -C target-cpu=native"/' /etc/makepkg.conf;
	sed -i 's/^#BUILDDIR.*/BUILDDIR=\/tmp\/makepkg/' /etc/makepkg.conf;
	sed -i "s/^#MAKEFLAGS.*/MAKEFLAGS=\"-j$(getconf _NPROCESSORS_ONLN) --quiet\"/" /etc/makepkg.conf;
	sed -i 's/^COMPRESSGZ=.*/COMPRESSGZ=(pigz -c -f -n)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSBZ2=.*/COMPRESSBZ2=(pbzip2 -c -f)/' /etc/makepkg.conf;
	sed -i "s/^COMPRESSXZ=.*/COMPRESSXZ=(xz -T \"$(getconf _NPROCESSORS_ONLN)\" -c -z --best -)/" /etc/makepkg.conf;
	sed -i 's/^COMPRESSZST=.*/COMPRESSZST=(zstd -c -z -q --ultra -T0 -22 -)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSLRZ=.*/COMPRESSLRZ=(lrzip -9 -q)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSLZO=.*/COMPRESSLZO=(lzop -q --best)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSZ=.*/COMPRESSZ=(compress -c -f)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSLZ4=.*/COMPRESSLZ4=(lz4 -q --best)/' /etc/makepkg.conf;
	sed -i 's/^COMPRESSLZ=.*/COMPRESSLZ=(lzip -c -f)/' /etc/makepkg.conf;

	## Here is what it should look like
	## Instead of $() statements there should be an output of the commands inside
	# '
	# CFLAGS="-march=native -mtune=native -O2 -pipe -fstack-protector-strong --param=ssp-buffer-size=4 -fno-plt"
	# CXXFLAGS="$CFLAGS"
	# RUSTFLAGS="-C opt-level=2 -C target-cpu=native"
	# BUILDDIR=/tmp/makepkg
	# MAKEFLAGS="-j$(getconf _NPROCESSORS_ONLN) --quiet"
	# COMPRESSGZ=(pigz -c -f -n)
	# COMPRESSBZ2=(pbzip2 -c -f)
	# COMPRESSXZ=(xz -T "$(getconf _NPROCESSORS_ONLN)" -c -z --best -)
	# COMPRESSZST=(zstd -c -z -q --ultra -T0 -22 -)
	# COMPRESSLRZ=(lrzip -9 -q)
	# COMPRESSLZO=(lzop -q --best)
	# COMPRESSZ=(compress -c -f)
	# COMPRESSLZ4=(lz4 -q --best)
	# COMPRESSLZ=(lzip -c -f)
	# '

	mkdir /etc/pacman.d/hooks;
}
################################################################################


artixBtrfs_ChrootBootloaderLimine() \
{
	# 10 Install bootloader
	pacman -S limine os-prober efibootmgr

	mkdir -pv /boot/EFI/BOOT

	cp -v /usr/share/limine/BOOTX64.EFI /boot/EFI/BOOT/

	cat << EOF > /boot/limine.cfg

:Artix Linux - Zen Kernel
    PROTOCOL=linux
    KERNEL_PATH=boot:///vmlinuz-linux-zen
    CMDLINE=root=PARTUUID=$(blkid -s PARTUUID -o value /dev/nvme0n1p2) rootflags=subvol=@artix rw loglevel=3 quiet console=tty1 nmi_watchdog=0 add_efi_memmap initrd=\amd-ucode.img
    MODULE_PATH=boot:///initramfs-linux-zen.img


:Artix Linux with Fallback Image - Zen Kernel
    PROTOCOL=linux
    KERNEL_PATH=boot:///vmlinuz-linux-zen
    CMDLINE=root=PARTUUID=$(blkid -s PARTUUID -o value /dev/nvme0n1p2) rootflags=subvol=@artix rw loglevel=3 quiet console=tty1 nmi_watchdog=0 add_efi_memmap initrd=\amd-ucode.img
    MODULE_PATH=boot:///initramfs-linux-zen-fallback.img


:Microsoft Windows
    PROTOCOL=efi_chainload
    IMAGE_PATH=boot:///EFI/Microsoft/Boot/bootmgfw.efi

EOF
	os-prober

	efibootmgr --create --disk /dev/nvme0n1p1 --loader '\EFI\BOOT\BOOTX64.EFI' --label 'Limine Boot Loader' --unicode "initrd=\amd-ucode.img"

	## 11 Pacman Hooks
	cat << EOF > /etc/pacman.d/hooks/liminedeploy.hook
[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = limine

[Action]
Description = Deploying Limine after upgrade...
When = PostTransaction
Exec = /usr/bin/cp /usr/share/limine/BOOTX64.EFI /boot/EFI/BOOT/
EOF
}

artixBtrfs_ChrootBootloaderRefind() \
{
	pacman -S refind

	refind-install

	## 11 Pacman Hooks
	cat << EOF > /etc/pacman.d/hooks/refind.hook
[Trigger]
Operation=Upgrade
Type=Package
Target=refind

[Action]
Description = Updating rEFInd on ESP
When=PostTransaction
Exec=/usr/bin/refind-install
EOF

	cat << EOF > /etc/pacman.d/hooks/zsh.hook
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Path
Target=/usr/bin/*

[Action]
Depends = zsh
When=PostTransaction
Exec=/usr/bin/install -Dm644 /dev/null /var/cache/zsh/pacman
EOF

	# If using Nvidia GPU
	# Change the linux part in Target and Exec lines if a different kernel is used.
	# Also change the nvidia-dkms part if using a different kernel.
	cat << EOF > /etc/pacman.d/hooks/nvidia.hook
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia-dkms
Target=linux-zen

[Action]
Description=Update Nvidia module in initcpio
Depends=mkinitcpio
When=PostTransaction
NeedsTargets
Exec=/bin/sh -c 'while read -r trg; do case \$trg in linux-zen) exit 0; esac; done; /usr/bin/mkinitcpio linux-zen'
EOF

	cat << EOF > /etc/udev/rules.d/60-ioschedulers.rules
# set scheduler for NVMe
ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"
# set scheduler for SSD and eMMC
ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
# set scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
EOF

	# 12 Configure bootloader
	# Replace 1920 1080 with your monitors resolution
	sed -i 's/^#resolution 3/resolution 1920 1080/' /boot/EFI/refind/refind.conf
	sed -i 's/^#use_graphics_for.*/use_graphics_for linux/' /boot/EFI/refind/refind.conf
	sed -i 's/^#scanfor.*/scanfor manual,external,internal/' /boot/EFI/refind/refind.conf
	sed -i 's/^#dont_scan_files.*/dont_scan_files vmlinuz-linux-zen/' /boot/EFI/refind/refind.conf

	# add the PARTUUID of the root partition to the "root=" option (example below)
	cat << EOF >> /boot/EFI/refind/refind.conf
menuentry "Artix Linux" {
	icon     /EFI/refind/icons/os_arch.png
	volume   ROOT
	loader   /vmlinuz-linux-zen
	initrd   /initramfs-linux-zen.img
	options  "root=PARTUUID=$(blkid -s PARTUUID -o value /dev/nvme0n1p2) rootflags=subvol=@artix rw loglevel=3 quiet console=tty1 nmi_watchdog=0 add_efi_memmap initrd=\amd-ucode.img"
	submenuentry "Boot - fallback" {
		initrd /boot/initramfs-linux-zen-fallback.img
	}
}
EOF
}

artixBtrfs_ChrootLastConfig() \
{
	# - add user ###########################################################
	# useradd -m  -s /usr/bin/zsh -G sudo,wheel,admin,storage,power,docker,libvirt,kvm $USER
	# passwd $USER
	# echo "$USER ALL=(ALL) ALL" >> /etc/sudoers

	# 13 Continue installing packages
	# su $USER
	# cd ~
	# git clone https://aur.archlinux.org/paru.git
	# cd paru
	# makepkg -si
	# cd ..
	# sudo rm -dR paru

	# At this point you may install any packages you need from AUR or official repositories.
	# sudo pacman -S snapper networkmanager-runit network-manager-applet openssh-runit \
	#     zsh-autosuggestions zsh-history-substring-search zsh-syntax-highlighting \
	#     libimobiledevice usbutils xorg xorg-xinit awesome pipewire pipewire-alsa \
	#     pipewire-pulse pipewire-jack pipewire-media-session pasystray pavucontrol \
	# sudo paru -S kitty-git dashbinsh

	# If you use a bare git to store dotfiles install them now
	# git clone --bare https://github.com/user/repo.git $HOME/.repo
	# exit

	# 14 - reboot into your new install
	exit

	swapoff -a
	umount -R /mnt/artix/boot
	umount -R /mnt/artix/tmp
	umount -R /mnt/artix/

	reboot
}

artixBtrfs_AfterInstalation() \
{
	# 15 - After instalation
	sudo ln -s /etc/runit/sv/{NetworkManager,sshd,chrony,cronie} /etc/runit/runsvdir/default
	sudo sv start NetworkManager sshd chrony cronie

	sudo umount /.snapshots
	sudo rm -r /.snapshots

	sudo snapper -c root create-config /
	sudo mount -a
	sudo chmod 750 -R /.snapshots
}
