#
# Shell How-Tos
#

## To test if you are in an interactive session
[ -t 1 ]

## For a CSV file with a single value per line:
while IFS=, read -r extension; do
    echo "Extension: $extension"
done < "$DOTFILES/unix/home/config/VSCodium/extensions.csv"
## Why This is Better
## Fully POSIX: No need for manual FD management.
## Cleaner and Simpler: while ... done < file is the standard way to read a
## file line by line.
## Automatically Closes File: The < file approach ensures the file descriptor
## is closed when the loop exits.

## When Would Using 9< file Make Sense?
## If you're reading multiple files simultaneously (e.g., one FD per file), you
## might need explicit FD management: To read values from .csv files, use an
## available file descriptor to do the job
while IFS=, read -r extension <&9;
do
	# bla bla bla
done \
9< "$DOTFILES/unix/home/config/VSCodium/extensions.csv"
exec 9<&-


# =========================== STRING OPERATIONS #1 =============================

##### How to extract the last part of a line/string separated by only one space.
# EXAMPLE: dscl . -read /Users/"$(id -un)" UserShell
#
echo "UserShell: /bin/zsh" \
| cut -d ' ' -f 2  ## Choose the 2nd part of the line after the first delimiter
| awk '{print $2}' ## Print the second part (only w/two clearly separated substrings)
| sed 's/UserShell: //' ## Substitute the substring for nothing and use the resulting string
##  RESULT: "/bin/zsh"



##### How to extract a known substring with a space from a line with <awk>.
# EXAMPLE: dscl . -read /Users/"$(id -un)" RealName
#
echo "ThisText Then RealName: First Last" \
| awk -F "RealName: " '{print $2}' ## Use this command with a more complicated string
## RESULT: "First Last"



##### How to extract a known substring with <grep> REGEX.
## EXAMPLE: sudo dscl . -list "/SharePoints"
#
printf "%s\n" \
"First Last's Public Folder" \
"Wirst Sast's Public Folder" \
"Zirst Oast's Public Folder" \
| grep -Eo '^.*?\First Last'   ## With -o, it prints only the substring in the REGEX
## RESULT: "First Last"

| grep -E '^.*?\First Last' ## Without -o, it prints any complete line that matches the REGEX
## RESULT: " First Last's Public Folder"

| grep -E '^.*?\Public' ## Without -o, it prints any complete line that matches the REGEX
## RESULT:
## First Last's Public Folder
## Wirst Sast's Public Folder
## Zirst Oast's Public Folder



##### How to extract a known substring that has a newline.
## EXAMPLE= "RealName:\n
##  First Last"
dscl . -read /Users/dievilz RealName \
| tr -d '\n' | sed 's/RealName: //g'    ## Based, removes both eol and space
| tr '\n' ' ' | sed 's/RealName:  //g'. ## Cringe, replaces the newline for a space and the delete de two spaces
## RESULT: "First Last"

# ----------------------------------------------------------------------


# =========================== STRING OPERATIONS #2 =============================

##### How to print a string in lowercase and delete whitespace.
uname -rs | tr '[:upper:]' '[:lower:]' # | tr -d ' '


##### How to delete the last character.
echo "/path/undesirables/last/char*" | perl -ple 'chop'
## RESULT:  /path/undesirable/last/char


##### How to delete ANSI Escape Codes with REGEX.
echo "\033[4m\033[38;34mNetBSD\033[0m" \
| sed 's/\\033\[[0-9;]*[a-zA-Z]//g'
| sed 's/\x1b\[[0-9;]*[a-zA-Z]//g'
| sed 's/\e\[[0-9;]*[a-zA-Z]//g'

echo "\033[4m\033[38;34mNetBSD\033[0m" \
| perl -pe 's/\\033\[[0-9;]*[a-zA-Z]//g'
| perl -pe 's/\x1b\[[0-9;]*[a-zA-Z]//g'
| perl -pe 's/\e\[[0-9;]*[a-zA-Z]//g'

### REGEX to find Special Chars in Sublime
[^\x00-\x7F]

# ----------------------------------------------------------------------


# ======================= STRING OPERATIONS with Files =========================

##### How to modify known (sub)strings to a line with "sed substitute".
## EXAMPLE: VARIABLE="value text"
sed -i -E 's|^?(VARIABLE=).*|\1new value|g' file.ext
## RESULT:  VARIABLE="new value"
#
## EXAMPLE: [by:somechineseuser]
sed -i -E 's|^(\[by:).*|\1dievilz\]|g' You\'re\ Going\ Down\ -\ Sick\ Puppies.lrcx
## RESULT:  [by:dievilz]
#
##### How to (un)comment a known VARIABLE with a value inside a file.
## EXAMPLE: # VARIABLE="value text"
sed -i -E 's|^#?(VARIABLE=).*|\1"new value"|g' file.ext
## RESULT:  VARIABLE="new value"


##### How to delete any character: i.e. a <whitespace>.
uname -rs | tr -d ' '
tr -d ' ' < input.txt > output.txt
tr --delete ' ' < input.txt > output.txt


##### How to replace <a> for <b>, i.e. a <li+ne feed> for <whitespace>.
tr '\n' ' ' < input.txt


##### How to search for an unknown value of a known key inside a file.
echo "name = FirstName" |
grep -Po '(?<=^name = )\w*$' file.ext
awk -v FS="name = " 'NF>1{print $2}' file.ext
awk -F= -v key="name" '$1==key {print $2}' file.ext
sed -n 's/^name = //p' file.ext


##### How to search for a known (sub)string inside a file.
echo "name = ..." |
grep -Fqx "${STRING}" /usr/local/etc/my.cnf.default; # -F: fixed,-q: quiet,-x:entire line
grep -R "name = " file.ext


##### How to (un)comment a known line inside a file.
sed -i -E 's/^#?(VARIABLE.*)$/\1/g' file.ext ## To uncomment, ? to match preceding expr once or none
sed -i -E 's/^?(VARIABLE.*)$/#\1/g' file.ext ## To comment, note the adding of #


##### How to reference a file extension with glob pattern
## Below does not work if the file doesn't have an extension, so the      (BASH)
## whole file name is taken.
ln -fsv "$file" "$HOME/.${file##*/}"

# ----------------------------------------------------------------------



# ======================= GET [USER,GROUP,ROOT] NAME ===========================

##### How to print current user username                               (POSIX 1)
## https://stackoverflow.com/questions/19306771/how-can-i-get-the-current-users-username-in-bash/23931327#23931327
id -un
id -gn

##### How to print current user username                               (POSIX 2)
## https://stackoverflow.com/questions/1104972/how-do-i-get-the-name-of-the-active-user-via-the-command-line-in-os-x/52435808#52435808
logname
crontab -u $(logname)

##### How to print current user username [with the current EUID]       (POSIX 3)
## https://stackoverflow.com/questions/19306771/how-can-i-get-the-current-users-username-in-bash/25118029#25118029
ps -o user= -p $$  # | awk '{print $1}'            # (For Solaris use this pipe)

##### How to print current user username              (Not scriptable) (POSIX 4)
## https://stackoverflow.com/questions/1104972/how-do-i-get-the-name-of-the-active-user-via-the-command-line-in-os-x/1106135#1106135
w

##### How to print current user username                                   (ZSH)
## OMZ/PWRLVL10K scripts
echo"${(%):-%n}"

##### How to print current user username                                  (BASH)
## https://stackoverflow.com/questions/19306771/how-can-i-get-the-current-users-username-in-bash/54265211#54265211
printf '%s\n' "${_@P}"
# : \\u                    ## (didn't work last time I tried)

# whoami   ##### [only for a user attached to stdin]                (DEPRECATED)

##### How to know if the user is root or not (0:root, >0:user)
id -u

##### Print disk free space in current device
df -h --output=pcent . | tail -1 | tr -d ' '

##### Print last segment of disk free space in current device
df -P . | sed -n '$s/[[:blank:]].*//p'


# ================================ FIND [*] ====================================

##### Print out a list of all the files whose names do not end in .c
find / \! -name "*.c" -print





# ================================= FD [*] =====================================
##
## fd <search_mode> <path>
## <fd>: L=follow symlinks; H=show hidden; I= show .ignored files;
##
## $(fd) uses regex as default search mode, the other
## mode being glob-based (exact-match), in other words:
##
## REGEX 1 (Pattern)
##   $ fd conf "$DOTFILES/unix/home/local/share" --type file
##     > /<users_folder>/<username>/.dotfiles/unix/home/local/share/gnupg/gpg-agent.conf
##     > /<users_folder>/<username>/.dotfiles/unix/home/local/share/gnupg/gpg.conf
##     > /<users_folder>/<username>/.dotfiles/unix/home/local/share/ssh/config
##
## GLOB (Exact-match)
##   $ fd -g config "$DOTFILES/unix/home/local/share" --type file
##     > /<users_folder>/<username>/.dotfiles/unix/home/local/share/ssh/config
##
##   $ fd -g conf "$DOTFILES/unix/home/local/share" --type file
##            ***Wont be output, didn't match any***
##
## REGEX 2 (Match-all)
## If you want to search for all files inside the '<path>' directory,
## use a match-all pattern:
##   $ fd . "$DOTFILES/unix/home/local/share" --type file
##     > "/<users_folder>/<username>/.dotfiles/unix/home/local/share/<folder>/<file>
##     > ...
##
## Instead, if you want your pattern to match the file path relative
## from where you are searching, use:
##   $ fd --full-path "$DOTFILES/unix/home/local/share"
##     > unix/home/local/share/<folder>/<file>
##

## Execute a command after find it and grep it
find "$DOTFILES/unix/home/local/share" -type f -exec basename {} \; | grep -E '^[.]'
# > .DS_Store

fd . "$DOTFILES/unix/home/local/share" -t file -x basename | grep -E '^[.]'
# > .DS_Store


## Find files with print0 and print only the filename
find "$DOTFILES/unix/home/local/share" -type f -print0 | xargs -0 basename -a | tr '\n' '\0'

find "$DOTFILES/unix/home/local/share" -type f | grep -Po "[^/]*$" | tr '\n' '\0'


## APPEND to filenames
find . -type f -name '*.config' -print0 | xargs --null -I{} mv -v {} {}-bkp.config


# ================================= GREP [*] ===================================
## grep: -m=Maxcount,-E=ExtendedREGEX,-o=output(print only matching part),-q=quiet





##                                   (POSIX)
# =============================== SHELL'S PATH =================================
## ps. -h: finishing all options with = for not showing any header,
## -p: <PID> list only process whith PID form list supplied,
## -o <column>: for showing only the value of the specified column

## On non-Linux, you should be able to use ps, e.g., ps -o args= $$ but that may
## wind up giving you the script name instead (POSIX allows either behavior).
ps -o comm= $$;
ps -o comm= -p $$;
ps -h -o comm= -p $$;
ps -o pid,args | awk '$1=='"$$"'{print $2}'
## > /usr/local/bin/zsh

## Shows arguments, don't use
ps -o args= $$;
## > /usr/local/bin/zsh -il
ps -p $$ | awk '{if(NR>1)print}' | awk '$0=$NF' | tr -d -
## > il


##                                   (POSIX)
# =============================== SHELL'S NAME =================================

basename $(ps -o comm= $$);
ps -o comm= $$ | sed 's|.*/||';
ps -o comm= $$ | awk -F/ '{print $NF}';
## > zsh

################################################################################


################################################################################

# =============================== SHELL'S NAME =================================

############ WITH O U T REGEX --------------------------------------------------
## -------------------- For scripts/terminal ------------------->
##                                                                      (Works in macOS)
lsof -p $$  | tr ' ()' '\012\012\012' | grep -i "sh$" | grep -v "$0" | tail -1
## > zsh

############ WITH REGEX --------------------------------------------------------
## -------------------- For scripts/terminal ------------------->
## Look for the shell running this script that: matches the singlewords bash OR sh
##                                                                      (POSIX I guess?)
ps -p $$ | grep -m 1 -Eo '\b(bash|sh)'
## > bash
## Look for the shell running this script that: matches the singlewords zsh
ps -p $$ | grep -m 1 -Eo '\b(zsh)'
ps -p $$ | grep -m 1 -Eo '(zsh)'
ps -p $$ | grep -m 1 -Eo 'zsh'
## > zsh

## -------------------- For scripts/terminal ------------------->
## Look for the shell running this script and:
##   Match just one single word with (a quantifier range of, in this
##   case) 0 to 6 word characters preceding 'sh' or the word 'koi'.     (\w not quite POSIX)
ps -p $$ | grep -m 1 -Eo '\b\w{0,6}sh|koi'
## > 123456sh

##   Match a single word with (a quantifier range of, in this
##   case) 0 to any qty. preceding 'sh'.                                (\w not quite POSIX)
ps -p $$ | grep -m 1 -Eo '\w{0,}sh'
## > zsh


# =============================== SHELL'S PATH =================================

############ WITH O U T REGEX --------------------------------------------------
## -------------------- For scripts/terminal ------------------->
lsof -p "$$" | grep -m 1 txt | xargs -n 1 | tail -n 1
## > /usr/local/Cellar/zsh/5.9/bin/zsh

## -------------------- For scripts/terminal ------------------->
## If you need to allow for possible space characters in the shell's path, use:
lsof -p "$$" | grep -m 1 txt | xargs -n 1 | tail -n +9 | xargs
## > /usr/local/Cellar/zsh/5.9/bin/zsh

##                                                                      (Works only in Linux)
readlink -f /proc/$$/exe
## > /usr/local/bin/zsh

############ WITH REGEX --------------------------------------------------------
## -------------------- For scripts/terminal ------------------->

## -------------------- For scripts/terminal ------------------->
## Depends on 3rd party command $(pstree) and its variants              (POSIX I guess?)
pstree -p $$  | tr ' ()' '\012\012\012' | grep -i "sh$" | grep -v "$0" | tail -1
## > /usr/local/bin/zsh

################################################################################




# ============================== MISCELLANEOUS =================================

## Delete any remaining precmd theme hooks
add-zsh-hook -L | grep -Eio 'prompt_.*_precmd'
add-zsh-hook -L | grep -Eio '\bprompt_[a-zA-Z0-9_]+'


### curl: -f=fail silently;-s=silent or quiet mode;-S=show errors;-L=follow HTTP redirects;-v=verbose mode.

# So, in summary, the curl -fsSLv --output <file> <url> command performs the following actions:
# Fetches the content of the specified URL.
# Follows redirects if there are any.
# Suppresses progress information.
# Displays verbose information about the request and response, including headers and SSL certificate details.
# Saves the fetched content to the specified file.

##### How to download a file using curl
curl -fSL --output "$HOME/Downloads" "https://raw.githubusercontent.com/dievilz/dotfiles/master/README.md"

##### How to download a file using wget (<curl> is preferable)
wget -qO "$HOME/Downloads" "https://raw.githubusercontent.com/dievilz/dotfiles/master/README.md"

##### How to clear a file
: > file.txt
truncate -s

##### How to copy a file with new permissions
install -C -m 600 "$localShareFile" "$HOME/.local/share$localSharePath"


##### Verify SHA integrity - way 1
shasum -c --ignore-missing shasum.ext file.ext
## The file contents should be: "<checksum>  <file.extension>"

##### Verify SHA integrity - way 2
echo "<checksum>  <file.extension>" | shasum -c-


##### Check {PGP,Signify,Minisign} signature
gpg --verify "${BOOTSTRAP_TAR_IN_PATH}{.asc,}"
gpg --verify file.ext signature.sig/asc

signify -V -p /etc/signify/void-release-20210930.pub -x sha256sum.sig -m sha256sum.txt
## Signature Verified

minisign -V -p /etc/signify/void-release-20210930.pub -x sha256sum.sig -m sha256sum.txt
## Signature and comment signature verified
## Trusted comment: timestamp:1634597366	file:sha256sum.txt

##### Append data to a file from stdin when root privilege is necessary.
echo "..." | sudo tee -a /etc/hosts < /tmp/hosts               # When stdout is needed

##### Append data to a file from another one when root privilege is necessary.
sudo tee -a /etc/hosts < /tmp/hosts               # When stdout is needed
sudo tee -a /etc/hosts < /tmp/hosts > /dev/null   # When stdout is not needed

## Above is preferred (because of speed and perfomance) over:
cat /tmp/hosts | sudo tee -a /etc/hosts
curl -fsSL ... | sudo tee -a /etc/hosts





# ============================== SUBLIME TEXT ==================================

##### How to change font per window - Open Console (Ctrl+`), type:
view.settings().set("font_size", <size>)
