#!/bin/bash

# Ensure you have root privileges
if [ "$(id -u)" != "0" ]; then
	echo "This script must be run as root or with sudo."
	exit 1
fi

function changeShellToUsers() \
{
	grep ":$1"'$' /etc/passwd | cut -d: -f1 > "$HOME/zsh_users.txt"

	# Loop through the list of usernames in the file
	while IFS= read -r username;
	do
		chsh -s "$2" "$username";

	done < "$HOME/zsh_users.txt"
}

# grep 'zsh$' /etc/passwd | awk -F: '{print $1, $7}'
# > user1 /bin/zsh
# > user2 /usr/bin/zsh
# > user3 /bin/zsh
# > user4 /bin/zsh
# > user5 /bin/zsh
# > user6 /usr/bin/zsh

echo "Enter the path of the shell to replace and verify it before pressing Enter:"
read oldShell

echo "Enter the path of the new shell to set and verify it before pressing Enter:"
read newShell

changeShellToUsers "$oldShell" "$newShell"; exit
