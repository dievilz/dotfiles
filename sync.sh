#!/bin/bash
#
# Dotfiles Syncing & Management Script
# Main script for dotfiles symlinking & rsyncing
# Influenced by @mavam's and @ajmalsiddiqui's sync script
#
# By Diego Villarreal, @dievilz in GitHub (https://github.com/dievilz)

####################################################################################################

runSyncingCommand() \
{
	case "$DOTFILES_SYNC_MODE" \
	in
		"hard")
			if [ "${DOTFILES_FORCE:-0}" -eq 1 ]; then
				ln -fv "$1" "$2"
			else
				ln -v "$1" "$2"
			fi
		;;
		"syml")
			if [ "${DOTFILES_FORCE:-0}" -eq 1 ]; then
				ln -fsv "$1" "$2"
			else
				ln -sv "$1" "$2"
			fi
		;;
		"cp"|"copy")
			if [ "${DOTFILES_FORCE:-0}" -eq 1 ]; then
				cp -fv "$1" "$2"
			else
				cp -v "$1" "$2"
			fi
		;;
		"rsync")
			if_bin_exists rsync

			## a:archive mode, equivalent to -rlptgoD
			## r:recursive; l:symls as symls; p:preserve perms;
			## t:preserve modtime; g:preserve groups (sudo-only);
			## o:preserve owner (sudo-only); D:devices&specials;
			##
			## -a implies no -AXUNH For versions >= 3.2.4
			## A:preserve ACLs (implies -p,--perms); X:preserve xattrs;
			## U:preserve access times; N:preserve create times;
			##
			## -a implies no   -H   For versions >= 2.6.9
			## H:hardlinks as hardls;
			##
			## c:skip files based on checksum; h:human readable; L:follow symlinks;
			## P:--progress & --partial; v:verbose; z:compress during transfer;
			##
			## A single -v will give you information about what files are being transferred
			## and a brief summary at the end. Two -v flags will give you information
			## on what files are being skipped and slightly more information at the end.

			## rsync -rLptgoD -UN -hPvvz if >= 3.2.4
			## rsync -rLptgoD     -hPvvz if >= 2.6.9
			if [ "${DOTFILES_FORCE:-0}" -eq 1 ];
			then
				rsync -hPvv --exclude=".*" "$1" "$2"
			else
				rsync -hcPvv --exclude=".*" "$1" "$2"
			fi
			echo
		;;
	esac
}

runFindingCommandTest() \
{
	echo "$1 --max-depth $2 --type $3 "$4" --print0"
}

runFindingCommand() \
{
	if if_bin_exists_notexit fd; then
		fd . $1 --max-depth $2 --type $3 "$4" --print0

	elif if_bin_exists_notexit fd-find; then
		fd-find . $1 --max-depth $2 --type $3 "$4" --print0

	elif if_bin_exists_notexit fdfind; then
		fdfind . $1 --max-depth $2 --type $3 "$4" --print0

	else
		find $1 -maxdepth $2 -type $3 -print0
	fi
}

############################################# DOTFILES #############################################

syncRootDirs() \
{
	subopt "Making some folders in superuser space first..."

	case "$(uname -s)" in
		"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*) true ;;
		*) enter_sudo_mode || true ;;
	esac

	echo "Enter the username for chown'ing Unix root/administrative folders (/)"
	echo "Verify it before pressing Enter key: "
	read usrName
	echo "Enter your Unix group for chown'ing Unix root/administrative folders"
	echo "Verify it before pressing Enter key: "
	read usrGrp
	echo

	# for rootdir in /opt /usr/local/src;
	# do
	# 	if [ ! -d "$rootdir" ];
	# 	then
	# 		mkdir -m 775 -pv "$rootdir" || \
	# 		doas mkdir -m 775 -pv "$rootdir" || \
	# 		sudo mkdir -m 775 -pv "$rootdir"

	# 	elif [ ! -w "$rootdir" ];
	# 	then
	# 		chown -v "$usrName":"$usrGrp" "$rootdir" || \
	# 		doas chown -v "$usrName":"$usrGrp" "$rootdir" || \
	# 		sudo chown -v "$usrName":"$usrGrp" "$rootdir"
	# 	fi

	# 	chmod -v 775 "$rootdir" || \
	# 	doas chmod -v 775 "$rootdir" || \
	# 	sudo chmod -v 775 "$rootdir"
	# done
	# echo
}

syncXDGDirs() \
{
	subopt "Making XDG folders in \$HOME..."

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo

	#============================================================================

	trdopt "Verifying \$XDG_CONFIG_HOME = ~/.config"
	[ ! -d "$HOME/.config" ] && mkdir -m 700 -pv "$HOME"/.config
	chmod -v 700 "$HOME"/.config
	[ ! -d "$HOME/.config/bash" ] && mkdir -pv "$HOME"/.config/bash
	chmod -v 700 "$HOME"/.config/bash
	[ ! -d "$HOME/.config/zsh" ] && mkdir -pv "$HOME"/.config/zsh
	chmod -v 700 "$HOME"/.config/zsh
	[ ! -d "$HOME/.config/shell" ] && mkdir -pv "$HOME"/.config/shell
	chmod -v 700 "$HOME"/.config/shell
	echo

	trdopt "Verifying \$XDG_CACHE_HOME = ~/.cache"
	[ ! -d "$HOME/.cache" ] && mkdir -m 700 -pv "$HOME"/.cache
	chmod -v 700 "$HOME"/.cache
	[ ! -d "$HOME/.cache/bash" ] && mkdir -pv "$HOME"/.cache/bash
	chmod -v 700 "$HOME"/.cache/bash
	[ ! -d "$HOME/.cache/zsh" ] && mkdir -pv "$HOME"/.cache/zsh
	chmod -v 700 "$HOME"/.cache/zsh
	[ ! -d "$HOME/.cache/shell" ] && mkdir -pv "$HOME"/.cache/shell
	chmod -v 700 "$HOME"/.cache/shell
	echo

	trdopt "Verifying \$XDG_DATA_HOME = ~/.local/share"
	[ ! -d "$HOME/.local" ] && mkdir -m 700 -pv "$HOME"/.local
	chmod -v 700 "$HOME"/.local
	[ ! -d "$HOME/.local/bin" ] && mkdir -m 700 -pv "$HOME"/.local/bin
	chmod -v 700 "$HOME"/.local/bin
	[ ! -d "$HOME/.local/lib" ] && mkdir -m 700 -pv "$HOME"/.local/lib
	chmod -v 700 "$HOME"/.local/lib
	[ ! -d "$HOME/.local/src" ] && mkdir -m 700 -pv "$HOME"/.local/src
	chmod -v 700 "$HOME"/.local/src
	[ ! -d "$HOME/.local/src/zsh" ] && mkdir -pv "$HOME"/.local/src/zsh
	chmod -v 700 "$HOME"/.local/src/zsh
	[ ! -d "$HOME/.local/share" ] && mkdir -m 700 -pv "$HOME"/.local/share
	chmod -v 700 "$HOME"/.local/share
	[ ! -d "$HOME/.local/share/bash" ] && mkdir -pv "$HOME"/.local/share/bash
	chmod -v 700 "$HOME"/.local/share/bash
	[ ! -d "$HOME/.local/share/zsh" ] && mkdir -pv "$HOME"/.local/share/zsh
	chmod -v 700 "$HOME"/.local/share/zsh
	[ ! -d "$HOME/.local/share/shell" ] && mkdir -pv "$HOME"/.local/share/shell
	chmod -v 700 "$HOME"/.local/share/shell
	echo

	trdopt "Verifying \$XDG_STATE_HOME = ~/.local/state"
	[ ! -d "$HOME/.state" ] && mkdir -m 700 -pv "$HOME"/.state
	chmod -v 700 "$HOME"/.state
	[ ! -d "$HOME/.local/state" ] && ln -fsv "$HOME"/.state "$HOME"/.local/state
	chmod -v 700 "$HOME"/.local/state
	[ ! -d "$HOME/.local/state/bash" ] && mkdir -pv "$HOME"/.local/state/bash
	chmod -v 700 "$HOME"/.local/state/bash
	[ ! -d "$HOME/.local/state/zsh" ] && mkdir -pv "$HOME"/.local/state/zsh
	chmod -v 700 "$HOME"/.local/state/zsh
	[ ! -d "$HOME/.local/state/shell" ] && mkdir -pv "$HOME"/.local/state/shell
	chmod -v 700 "$HOME"/.local/state/shell
	echo

	trdopt "Verifying \$XDG_OPT_HOME = ~/.opt"
	[ ! -d "$HOME/.opt" ] && mkdir -m 700 -pv "$HOME"/.opt
	chmod -v 700 "$HOME"/.opt
	echo

	#============================================================================

	magenta "Symlinking folders on \$XDG_DATA_HOME to \$HOME"

	for localsharedir in gnupg m2 node-gyp rage ssh;
	do
		[ ! -d "$HOME/.local/share/$localsharedir" ] && \
		mkdir -m 700 -pv "$HOME/.local/share/$localsharedir"
		chmod -v 700 "$HOME/.local/share/$localsharedir"

		[ -e "$HOME/.$localsharedir" ] && rmdir "$HOME/.$localsharedir"
		[ ! -h "$HOME/.$localsharedir" ] && \
		ln -fsv "$HOME/.local/share/$localsharedir" "$HOME/.$localsharedir"
		chmod -v 700 "$HOME/.$localsharedir"
	done
	echo
	find $HOME/.local/share/gnupg -type f -exec chmod 600 {} \;
	find $HOME/.local/share/gnupg -type d -exec chmod 700 {} \;
	find $HOME/.local/share/ssh -type f -exec chmod 600 {} \;
	find $HOME/.local/share/ssh -type d -exec chmod 700 {} \;
	find $HOME/.local/share/rage -type f -exec chmod 600 {} \;
	find $HOME/.local/share/rage -type d -exec chmod 700 {} \;

	#============================================================================

	if [ -w "$HOME" ];
	then
		printf "\n%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
		printf "\n"
	fi
	echo
}

syncHomeDirs() \
{
	subopt "Making normal folders in \$HOME..."

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo

	#============================================================================

	for homedir in "$HOME/SysAdmin" "$HOME/Desktop" "$HOME/Dev" \
	"$HOME/Documents" "$HOME/Downloads" "$HOME/Movies" "$HOME/Music" \
	"$HOME/Pictures" "$HOME/Videos";
	do
		[ ! -d "$homedir" ] && mkdir -m 700 -pv "$homedir"
		chmod -v 700 "$homedir"
	done

	for homedir in "$HOME/Public" "$HOME/Sites";
	do
		[ ! -d "$homedir" ] && mkdir -m 755 -pv "$homedir"
		chmod -v 755 "$homedir"
	done

	for homedir in "$HOME/Dev/Repos" "$HOME/Music/DownloadedMusic" \
	"$HOME/Music/DownloadedSpotify" "$HOME/Pictures/Wallpapers";
	do
		[ ! -d "$homedir" ] && mkdir -pv "$homedir"
	done

	if [ "$(uname -s)" = "Darwin" ]; then
		[ ! -d "$HOME/Dev/Xcode" ] && mkdir -pv "$HOME"/Dev/Xcode
		[ ! -d "$HOME/Library/QuickLook" ] && mkdir -pv "$HOME"/Library/QuickLook
		[ ! -d "$HOME/Music/LyricsX" ] && mkdir -pv "$HOME"/Music/LyricsX
		# [ ! -d "$HOME/Public/LyricsX" ] && mkdir -pv "$HOME"/Public/LyricsX
		# [ ! -h "$HOME/Music/LyricsX" ] && ln -fsv "$HOME"/Public/LyricsX "$HOME"/Music/LyricsX

		[ ! -d "/Library/Services" ] && sudo mkdir -pv /Library/Services
		[ ! -d "/Library/Sounds" ] && sudo mkdir -pv /Library/Sounds
		[ ! -d "/Library/Fonts" ] && sudo mkdir -pv /Library/Fonts
	fi

	#============================================================================

	if [ -w "$HOME" ];
	then
		printf "\n%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
		printf "\n"
	fi
	echo
}

syncCustomHomeDirs() \
{
	magenta "Symlinking folders on \$XDG_OPT_HOME to \$HOME"

	[ ! -d "${DOTFILES:-$HOME/.dotfiles}" ] && DOTFILES="$HOME/.dotfiles"
	[ ! -d "${PUNTO_SH:-$HOME/.punto-sh}" ] && PUNTO_SH="$HOME/.punto-sh"
	[ ! -d "${PUNTO:-$HOME/.punto}" ] && PUNTO="$HOME/.punto"

	for homedir in "$DOTFILES" "$PUNTO_SH" "$PUNTO";
	do
		[ ! -d "$homedir" ] && mkdir -m 775 -pv "$homedir"
		chmod -v 775 "$homedir"
	done

	for optdir in anydesk fleet kube minikube sqldeveloper vnc;
	do
		[ ! -d "$HOME/.opt/$optdir" ] && mkdir -m 700 -pv "$HOME/.opt/$optdir"
		chmod -v 700 "$HOME/.opt/$optdir"

		[ -e "$HOME/.$optdir" ] && rmdir "$HOME/.$optdir"
		[ ! -h "$HOME/.$optdir" ] && ln -fsv "$HOME/.opt/$optdir" "$HOME/.$optdir"
		chmod -v 700 "$HOME/.$optdir"
	done
	echo

	#### Docker #################################################################
	[ ! -d "$HOME/.opt/docker" ] && mkdir -m 700 -pv "$HOME/.opt/docker"
	chmod -v 700 "$HOME/.opt/docker"

	# if [ ! $(uname -s) == "Darwin" ]; then
	[ -e "$HOME/.docker" ] && rmdir "$HOME/.docker"
	[ ! -h "$HOME/.docker" ] && ln -fsv "$HOME/.opt/docker" "$HOME/.docker"
	chmod -v 700 "$HOME/.docker"
	# fi
	echo

	#### Visual Studio Cod* #####################################################
	[ ! -d "$HOME/.opt/vscod" ] && mkdir -m 700 -pv "$HOME/.opt/vscod"
	chmod -v 700 "$HOME/.opt/vscod"
	[ ! -d "$HOME/.opt/vscod-oss" ] && mkdir -m 700 -pv "$HOME/.opt/vscod-oss"
	chmod -v 700 "$HOME/.opt/vscod-oss"

	[ ! -d "$HOME/.opt/vscod-cli" ] && mkdir -m 700 -pv "$HOME/.opt/vscod-cli"
	chmod -v 700 "$HOME/.opt/vscod-cli"
	[ ! -d "$HOME/.opt/vscod-extensions" ] && mkdir -m 700 -pv "$HOME/.opt/vscod-extensions"
	chmod -v 700 "$HOME/.opt/vscod-extensions"

	[ -e "$HOME/.code" ]       && rmdir "$HOME/.code"
	[ -e "$HOME/.vscode" ]     && rmdir "$HOME/.vscode"
	[ -e "$HOME/.code-oss" ]   && rmdir "$HOME/.code-oss"
	[ -e "$HOME/.Codium" ]     && rmdir "$HOME/.Codium"
	[ -e "$HOME/.vscode-oss" ] && rmdir "$HOME/.vscode-oss"
	[ -e "$HOME/.VSCodium" ]   && rmdir "$HOME/.VSCodium"

	[ ! -h "$HOME/.code" ]       && ln -fsv "$HOME/.opt/vscod" "$HOME/.code"
	[ ! -h "$HOME/.vscode" ]     && ln -fsv "$HOME/.opt/vscod" "$HOME/.vscode"
	[ ! -h "$HOME/.code-oss" ]   && ln -fsv "$HOME/.opt/vscod-oss" "$HOME/.code-oss"
	[ ! -h "$HOME/.Codium" ]     && ln -fsv "$HOME/.opt/vscod-oss" "$HOME/.Codium"
	[ ! -h "$HOME/.vscode-oss" ] && ln -fsv "$HOME/.opt/vscod-oss" "$HOME/.vscode-oss"
	[ ! -h "$HOME/.VSCodium" ]   && ln -fsv "$HOME/.opt/vscod-oss" "$HOME/.VSCodium"

	[ ! -h "$HOME/.opt/vscod/cli" ]            && ln -fsv "$HOME/.opt/vscod-cli" "$HOME/.opt/vscod/cli"
	[ ! -h "$HOME/.opt/vscod-oss/cli" ]        && ln -fsv "$HOME/.opt/vscod-cli" "$HOME/.opt/vscod-oss/cli"
	[ ! -h "$HOME/.opt/vscod/extensions" ]     && ln -fsv "$HOME/.opt/vscod-extensions" "$HOME/.opt/vscod/extensions"
	[ ! -h "$HOME/.opt/vscod-oss/extensions" ] && ln -fsv "$HOME/.opt/vscod-extensions" "$HOME/.opt/vscod-oss/extensions"

	chmod -v 700 "$HOME"/.{code,vscode,code-oss,Codium,vscode-oss,VSCodium}

	#============================================================================

	if [ -w "$HOME" ];
	then
		printf "\n%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
		printf "\n"
	fi
	echo
}

syncBin() \
{
	subopt "Symlinking Dotfiles scripts to $HOME/.local/bin"
	# cp -v "$DOTFILES"/bootstrap.sh "$HOME"/.local/bin/bootstrap
	# cp -v "$DOTFILES"/sync.sh "$HOME"/.local/bin/sync
	echo
}

syncPkgEtc() \
{
	subopt "Copying your system-wide config files (pkg manager apps) to their" \	"/etc/<folders>..."

	case "$(uname -s)" in
		"MINGW"*|"MSYS"*|*"WIN"*|"Win"*|*"DOS"*) true ;;
		*) enter_sudo_mode ;;
	esac

	case "$(uname -s)" \
	in
		"Darwin")
			if command -v pkgin > /dev/null 2>&1;
			then
				[ ! -d /opt/pkg/etc/defaults.d ] && mkdir -m 700 /opt/pkg/etc/defaults.d

				if [ -e /opt/pkg/bin/dnsmasq ];
				then
					[ ! -e /opt/pkg/etc/rc.d/dnsmasq ] && sudo install -C -m 0755 \
					/opt/pkg/share/examples/rc.d/dnsmasq /opt/pkg/etc/rc.d/dnsmasq

					[ ! -e /opt/pkg/etc/defaults.d/dnsmasq.conf_default ] \
					&& sudo mv -v /opt/pkg/etc/dnsmasq.conf \
					/opt/pkg/etc/defaults.d/dnsmasq.conf_default

					runSyncingCommand "$DOTFILES/unix/etc/dnsmasq/dnsmasq.conf" \
					/opt/pkg/etc/dnsmasq.conf
				fi


				if [ -e /opt/pkg/bin/privoxy ];
				then
					[ ! -e /opt/pkg/etc/rc.d/privoxy ] && sudo install -C -m 0755 \
					/opt/pkg/share/examples/rc.d/privoxy /opt/pkg/etc/rc.d/privoxy
					while IFS= read -r -d '' privoxyFile;
					do
						[ ! -e /opt/pkg/etc/defaults.d/"$(basename "$privoxyFile")"_default ] && \
						sudo mv -v /opt/pkg/etc/"$(basename "$privoxyFile")" \
						/opt/pkg/etc/defaults.d/"$(basename "$privoxyFile")"_default

						runSyncingCommand "$privoxyFile" \
						"/opt/pkg/etc/privoxy/$(basename "$privoxyFile")"
					done \
					< <(find "$DOTFILES/unix/etc/privoxy" -maxdepth 1 -type f -print0)
				fi


				[ ! -e /opt/pkg/etc/rc.d/dbus ] && sudo install -C -m 0755 \
				/opt/pkg/share/examples/rc.d/dbus /opt/pkg/etc/rc.d/dbus
				if ! sudo grep -Fqx "dbus=YES" /opt/pkg/etc/rc.d/dbus; then
					echo "dbus=YES" | sudo tee -a /opt/pkg/etc/rc.d/dbus
				fi

				runSyncingCommand "$DOTFILES/unix/etc/doas.conf" /opt/pkg/etc/doas.conf
				echo
			fi

			if command -v brew > /dev/null 2>&1;
			then
				[ ! -d "$(brew --prefix)/etc/defaults.d" ] \
				&& mkdir -m 700 "$(brew --prefix)/etc/defaults.d"

				if [ -e "$(brew --prefix)/bin/dnsmasq" ];
				then
					[ ! -e "$(brew --prefix)/etc/defaults.d/dnsmasq.conf_default" ] \
					&& sudo mv -v "$(brew --prefix)/etc/dnsmasq.conf" \
					"$(brew --prefix)/etc/defaults.d/dnsmasq.conf_default"

					runSyncingCommand "$DOTFILES/unix/etc/dnsmasq.conf" \
					"$(brew --prefix)/etc/dnsmasq.conf"
				fi

				if [ -e "$(brew --prefix)/bin/dnscrypt-proxy" ];
				then
					[ ! -e "$(brew --prefix)/etc/defaults.d/dnscrypt-proxy.toml_default" ] \
					&& sudo mv -v "$(brew --prefix)/etc/dnscrypt-proxy.toml" \
					"$(brew --prefix)/etc/defaults.d/dnscrypt-proxy.toml_default"

					runSyncingCommand "$DOTFILES/unix/etc/dnscrypt-proxy.toml" \
					"$(brew --prefix)/etc/dnscrypt-proxy.toml"
				fi

				if [ -e "$(brew --prefix)/bin/privoxy" ];
				then
					while IFS= read -r -d '' privoxyFile;
					do
						[ ! -e "$(brew --prefix)/etc/defaults.d/$(basename "$privoxyFile")"_default ] \
						&& \
						sudo mv -v "$(brew --prefix)/etc/privoxy/$(basename "$privoxyFile")" \
						"$(brew --prefix)/etc/defaults.d/$(basename "$privoxyFile")_default"

						runSyncingCommand "$privoxyFile" \
						"$(brew --prefix)/etc/privoxy/$(basename "$privoxyFile")"
					done \
					< <(find "$DOTFILES/unix/etc/privoxy" -maxdepth 1 -type f -print0)
				fi
			fi
			echo
		;;
	esac
}
# --------------------------------------------------------------------------------------------------

syncHome() \
{
	subopt "Symlinking Dotfiles to $HOME..."

	if [ ! -w "$HOME" ]; then
		echo; printf "%b" "Changing \$HOME permissions: " && chmod -v 751 "$HOME"
	fi
	echo

	while IFS= read -r -d '' dotfile;
	do
		if basename "$dotfile" | grep -Eq '^[.]'; then continue ; fi

		runSyncingCommand "$dotfile" "$HOME/.$(basename "$dotfile")"

		printf "%b" "Changing owner/group: " && \
		chown -hRv "$(id -un)":"$(id -gn)" "$HOME/.$(basename "$dotfile")"

		printf "%b" "Changing permissions: " && \
		chmod -v 700 "$HOME/.$(basename "$dotfile")"
	done \
	< <(find "$DOTFILES/unix/home" -maxdepth 1 -type f -print0)

	touch "$HOME"/.hushlogin

	echo; printf "%b" "Defending \$HOME: " && chmod -v 551 "$HOME"
	echo
}

syncConfig() \
{
	subopt "Symlinking your local config files to their ~/.config folders..."

	trdopt "Recreating the tree structure inside ~/.config..."
	rsync -aPvv -f "+ */" -f "- *" "$DOTFILES/unix/home/config/" "${XDG_CONFIG_HOME:-$HOME/.config}/"
	echo

	trdopt "Syncing files..."
	case "${DOTFILES_SYNC_MODE:?}" \
	in
		"syml")
			while IFS= read -d '' configFile;
			do
				case "$(basename "$configFile")" in
					.*) continue ;;
				esac

				configFilePrefix="$DOTFILES/unix/home/config/"
				configFilePath="${configFile#$configFilePrefix}"
				# echo $configFilePath

				if [ ! -d "$(dirname "$configFilePath")" ];
				then
					mkdir -m 700 -pv "$HOME/.config/$(dirname "$configFilePath")"
				fi

				targetDir="$HOME/.config/$(dirname "$configFilePath")"
				[ ! -d "$targetDir" ] && mkdir -p "$targetDir"

				runSyncingCommand "$configFile" "$HOME/.config/$configFilePath"
			done \
			< <(find "$DOTFILES/unix/home/config" -type f -print0)
			echo
		;;
		"rsync") rsync -hPvv "$DOTFILES/unix/home/config/" "${XDG_CONFIG_HOME:-$HOME/.config}/"
		;;
	esac
}

syncShell() \
{
	subopt "Symlinking Shell files to their ~/.config/<shell> folders..."

	ln -fsv "$DOTFILES/unix/home/config/zsh/omz.zshrc" "$ZDOTDIR/"
	ln -fsv "$DOTFILES/unix/home/config/zsh/zinit.zshrc" "$ZDOTDIR/"
}

syncData() \
{
	subopt "Copying your local data/share files to their ~/.local/share folders..."

	trdopt "Recreating the tree structure inside ~/.config..."
	rsync -aPv -f "+ */" -f "- *" "$DOTFILES/unix/home/local/share/" "${XDG_DATA_HOME:-$HOME/.local/share}/"
	echo

	trdopt "Syncing files..."
	rsync -hcPvv --exclude=".*" "$DOTFILES/unix/home/local/share/" "${XDG_DATA_HOME:-$HOME/.local/share}/"
	echo
}

syncHomeOpt() \
{
	subopt "Copying your local custom files (3rd-party apps) to their" \
	"~/.opt folders..."

	trdopt "Recreating the tree structure inside $SHARED_HOME..."
	rsync -aPv -f "+ */" -f "- *" "$DOTFILES/unix/home/opt/" "${XDG_OPT_HOME:-$HOME/.opt}/"
	echo

	trdopt "Syncing files..."
	rsync -hcPvv --exclude=".*" "$DOTFILES/unix/home/opt/" "${XDG_OPT_HOME:-$HOME/.opt}/"
	echo
}

syncShared() \
{
	subopt "Copying your system-wide custom files (3rd party apps) to their" \
	"$SHARED_HOME/<folders>..."

	trdopt "Recreating the tree structure inside $SHARED_HOME..."
	rsync -aPv -f "+ */" -f "- *" "$DOTFILES/unix/home/shared/" "${SHARED_HOME:?}/"
	echo

	trdopt "Syncing files..."
	rsync -ahcPvv --exclude=".*" --exclude="asdf.sh" --exclude="oh-my-zsh.sh" "$DOTFILES/unix/home/shared/" "${SHARED_HOME:?}/"
	echo

	if [ -n "$(ls -A "$SHARED_HOME/asdf-vm")" ];
	then
		runSyncingCommand "$DOTFILES/unix/home/shared/asdf-vm/asdf.sh" "$SHARED_HOME/asdf-vm/asdf.sh"
	fi

	if [ -n "$(ls -A "$SHARED_HOME/oh-my-zsh")" ];
	then
		runSyncingCommand "$DOTFILES/unix/home/shared/oh-my-zsh/oh-my-zsh.sh" "$SHARED_HOME/oh-my-zsh/oh-my-zsh.sh"
	fi
}


####################################################################################################




########################################### TEXT EDITORS ###########################################

textedSublimeText() \
{
	option "Copying Sublime Text custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "$textedbinLocation" ] && [ ! -e /usr/local/bin/subl ];
			then
				subopt "Symlinking <subl> binary to /usr/local/bin..."
				runSyncingCommand \
				"/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" \
				/usr/local/bin/subl
				echo
			fi
			textedAppLocation="$HOME/Library/Application Support/Sublime Text/Packages/"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo
		;;
		"Linux") textedAppLocation="$HOME/.config/sublime-text/Packages/" ;;
		*) return ;;
	esac

	case "$1" \
	in
		"basic")
			warn "There is an error when downloading Package Control using" \
			"Homebrew's OpenSSL, refer to: https://stackoverflow.com/questions/65202385/openssl-libcrypto-dylib-problem-with-package-control-in-sublime-text-3" \
			"or this GitHub issue: https://github.com/wbond/package_control/issues/1612#issuecomment-1643609833"

			command subl --new-window
			oldPATH="$PATH"
			export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin"
			# command subl --command install_package_control
			command subl --command $SUBL_INSTALL_PKGCONTROL_CMD
			SUBL_INSTALL_PKGCONTROL_CMD="from urllib.request import urlretrieve;urlretrieve(url="https://github.com/wbond/package_control/releases/latest/download/Package.Control.sublime-package", filename=sublime.installed_packages_path() + '/Package Control.sublime-package')"
			export PATH="$oldPATH"
			echo

			# [ ! -r "$textedAppLocation/User/Package Control.sublime-settings" ] && {
			# 	printf "%b" '{\n\t"bootstrapped": true,\n\t"in_process_packages": [],\n\t"installed_packages": [\n\t\t"Package Control"\n\t]\n}\n' \
			# 	> "$textedAppLocation/User/Package Control.sublime-settings"
			# 	echo
			# }

			info "Execute '--text-editors stext-full' when you make sure you" \
			"have Package Control installed in ST."
			echo
		;;
		"full")
			echo
			info "Before doing so, remember to first install Package Control, that way," \
			"every package listed inside Package Control.sublime-settings will be installed."
			subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
			suboptq "" && read -r skipper

			[ -z "$skipper" ] && unset $skipper && return
			unset $skipper && echo

			case $(command subl --version | cut -d ' ' -f 4) in
			4*)
				runSyncingCommand "$DOTFILES/unix/home/config/sublime-text/Preferences.sublime-settings" \
				"$textedAppLocation/User/Preferences.sublime-settings"
			;;
			3*)
				runSyncingCommand "$DOTFILES/unix/home/config/sublime-text/Preferences3.sublime-settings" \
				"$textedAppLocation/User/Preferences.sublime-settings"
			;;
			esac
			echo
			subopt "Hard-linking recursively the files..."
			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -ahPvv --link-dest="$DOTFILES/unix/home/config/sublime-text/Packages" \
			"$DOTFILES/unix/home/config/sublime-text/Packages/" "$textedAppLocation"
		;;
	esac
	echo
}

textedSublimeMerge() \
{
	option "Copying Sublime Merge custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "/Applications/Sublime Merge.app/Contents/SharedSupport/bin/smerge" ] \
			&& [ ! -e /usr/local/bin/smerge ];
			then
				subopt "Symlinking smerge binary to /usr/local/bin..."
				runSyncingCommand \
				"/Applications/Sublime Merge.app/Contents/SharedSupport/bin/smerge" \
				/usr/local/bin/smerge
				echo
			fi

			subopt "Hard-linking recursively the files..."
			textedAppLocation="$HOME/Library/Application Support/Sublime Merge/Packages"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo

			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -ahPvv --link-dest="$DOTFILES/unix/home/config/sublime-merge/Packages" \
			"$DOTFILES/unix/home/config/sublime-merge/Packages/" "$textedAppLocation"
			echo
		;;
		"Linux") true ;;
		*) return ;;
	esac
}

textedVSCodium() \
{
	option "Copying Visual Studio Code custom files..."

	case "$(uname -s)" \
	in
		"Darwin")
			if [ -e "/Applications/VSCodium.app/Contents/Resources/app/bin/codium" ] \
			&& [ ! -e /usr/local/bin/codium ];
			then
				subopt "Symlinking codium binary to /usr/local/bin..."
				ln -fsv \
				"/Applications/VSCodium.app/Contents/Resources/app/bin/codium" \
				/usr/local/bin/codium
				echo
			fi

			subopt "Hard-linking recursively the files..."
			textedAppLocation="$HOME/Library/Application Support/VSCodium/User"
			[ ! -d "$textedAppLocation" ] && mkdir -pv "$textedAppLocation" && echo

			## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
			## H:hardlinks as hardls; h:human readable; --partial: ..transfers
			## link-dest=DIR: hardlink to files in DIR when unchanged; v:verbose
			rsync -ahPvv --link-dest="$DOTFILES/unix/home/config/VSCodium/User/" \
			"$DOTFILES/unix/home/config/VSCodium/User/" "$textedAppLocation"
			echo
		;;
		"Linux") true ;;
		*) return ;;
	esac

	subopt "Installing Visual Studio Code extensions..."
	while IFS=, read -r extension <&9;
	do
		if [ -z "$extension" ]; then continue; fi
		codium --install-extension "$extension"
	done \
	9< "$DOTFILES/unix/home/config/VSCodium/extensions.csv"
	exec 9<&-

	echo
}

####################################################################################################




########################################### PREFERENCES ############################################

setup_preferences() \
{
	option "Preferences: Copying preferences files for apps (like Spotify, iTerm2, etc)..."

	case "$(uname -s)" \
	in
	"Darwin")
		subopt "Enter your Spotify Unique ID/Account ID to sync your Spotify" \
		"Desktop preferences"
		suboptq "[Type 'skip'/'none'/press 'Enter' to halt]: "
		read -r spotifyUID
		case "$spotifyUID" \
		in
			n|N|no|No|"skip"|"none"|"")
				warn "Skipping this step!"
				echo
			;;
			*)
			mkdir -pv "$HOME/Library/Application Support/Spotify/Users/$spotifyUID-user"
			trdopt "Copying Spotify preferences"
			cp -v "$DOTFILES/unix/home/config/spotify/prefs" \
			"$HOME/Library/Application Support/Spotify/Users/$spotifyUID-user/prefs"
			;;
		esac

		if [ -d "$HOME/Library/Containers/com.aone.keka/Data/Library/Preferences" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/lyricsx/com.aone.keka.plist" \
			"$HOME"/Library/Containers/com.aone.keka/Data/Library/Preferences/
		fi

		if [ -d "$HOME/Library/Containers/ddddxxx.LyricsX/Data/Library/Preferences" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/lyricsx/ddddxxx.LyricsX.plist" \
			"$HOME"/Library/Containers/ddddxxx.LyricsX/Data/Library/Preferences/
		fi

		if [ -d "$HOME/Library/Containers/com.fiplab.memoryclean2/Data/Library/Preferences/" ];
		then
			cp -v "$DOTFILES/unix/home/Library/Containers/memoryclean2/com.fiplab.memoryclean2.plist" \
			"$HOME"/Library/Containers/com.fiplab.memoryclean2/Data/Library/Preferences/
		fi

		while IFS= read -r -d '' prefFile;
		do
			# echo "$prefFile" # "$DOTFILES/unix/home/Library/Preferences/<file>"
			prefFilePath="$(echo "$prefFile" | \
				awk -v a="$DOTFILES/unix/home/Library/Preferences//" \
				'{len=length(a)}{print substr($0,len)}')"
			# echo "$prefFilePath" # "/<file>"

			cp -Rv "$prefFile" "$HOME/Library/Preferences/$prefFilePath"
		done \
		< <(fd . "$DOTFILES/unix/home/Library/Preferences" -d "1" -t "f" -0)
		# < <(runFindingCommand "$DOTFILES/unix/home/Library/Preferences" "1" "f" "")
		echo
	;;
	"Linux")
		warn "Nothing to add yet..."
	;;
	esac
	echo
}

############################################# LIBRARY ##############################################

setup_library_macos() \
{
	option "Library: Copying Library files for apps (like LyricsX, TimeOut, etc)..."
	isMacos

	subopt "Setting up ${IT}Renamer6${NIT} data (renamerlets)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	if [ -n "$skipper" ]; then
		[ ! -d "$HOME/Library/Application Support/Renamer" ] \
		&& mkdir -pv "$HOME/Library/Application Support/Renamer"

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; --partial: ..transfers; v:verbose;
		rsync -ahPvv \
		"$DOTFILES/unix/home/Library/Application Support/Renamer/Renamerlets.renamerlets" \
		"$HOME/Library/Application Support/Renamer/Renamerlets.renamerlets"
		echo
	fi
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Time Out${NIT} data (settings and breaks)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_timeout
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}yEd Editor${NIT} data (settings and bundles)"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_yed
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}QuickLook${NIT} generators"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_quicklook
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Automator${NIT} Services and Quick Actions"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_automator
	[ -z "$skipper" ] && echo
	unset $skipper

	subopt "Setting up ${IT}Sounds${NIT} for System Preferences"
	subopt "Do you wish to continue? [Type anything or press Enter key to skip]"
	suboptq "" && read -r skipper
	[ -n "$skipper" ] && setup_library_sounds
	[ -z "$skipper" ] && echo
	unset $skipper
}

setup_library_timeout() \
{
	while IFS= read -r -d '' timeOutFile;
	do
		# echo "$timeOutFile" # "$DOTFILES/unix/home/Library/Group Containers/timeout/<folder1>/<file>"
		timeOutFilePath="$(echo "$timeOutFile" | \
			awk -v a="$DOTFILES/unix/home/Library/Group Containers/timeout//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$timeOutFilePath" # "<folder1>/<file>"

		timeOutFolder="$(find "$HOME/Library/Group Containers" \
			-name "*dejal.timeout.free" -type d -print0 -quit | \
			awk -F "Containers/" '{print $2}' | tr -d '\0')"
		# echo "$timeOutFolder" # "xxxxxxxxxx.com.dejal.timeout.free"

		if [ ! -d "$HOME/Library/Group Containers/$timeOutFolder/$(dirname "$timeOutFilePath")" ];
		then
			mkdir -pv "$HOME/Library/Group Containers/$timeOutFolder/$(dirname "$timeOutFilePath")"
		fi

		if [ ! -e "$HOME/Library/Group Containers/$timeOutFolder/$timeOutFilePath" ];
		then
			cp -Rv "$timeOutFile" \
			"$HOME/Library/Group Containers/$timeOutFolder/$timeOutFilePath"
		fi
	done \
	< <(fd . "$DOTFILES/unix/home/Library/Group Containers/timeout" -d "5" -t "f" -0)
	# < <(runFindingCommand "$DOTFILES/unix/home/Library/Group Containers/timeout" "5" "f" "")
	echo
}

setup_library_yed() \
{
	while IFS= read -r -d '' yEdFile;
	do
		# echo "$yEdFile" # "$DOTFILES/unix/home/Library/yWorks/yEd/<folder1>/<file>"
		yEdFilePath="$(echo "$yEdFile" | \
			awk -v a="$DOTFILES/unix/home/Library/yWorks//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$yEdFilePath" # "yEd/<folder1>/<file>"

		if [ ! -d "$(dirname "$yEdFilePath")" ];
		then
			mkdir -pv "$HOME/Library/yWorks/$(dirname "$yEdFilePath")"
		fi

		if [ ! -e "$HOME/Library/yWorks/$yEdFilePath" ];
		then
			cp -Rv "$yEdFile" "$HOME/Library/yWorks/$yEdFilePath"
		fi
	done < <(fd . "$DOTFILES/unix/home/Library/yWorks/yEd" -d "5" -t "f" -0)
	# done < <(runFindingCommand "$DOTFILES/unix/home/Library/yWorks/yEd" "5" "f" "")
	echo
}

setup_library_quicklook() \
{
	trdopt "Copying QuickLook Generators from iCloud Drive..."

	[ ! -d "$HOME/Library/QuickLook" ] && mkdir -pv "$HOME"/Library/QuickLook
	while IFS= read -r -d '' qlgen;
	do
		[ ! -d "$HOME/Library/QuickLook/$(basename $qlgen)" ] && \
		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; --partial: ..transfers; v:verbose
		rsync -ahPvv --exclude=".*" "$qlgen" "$HOME/Library/QuickLook/"
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~CloudDocs/QuickLook" \
		-maxdepth 1 -name "*.qlgenerator" -type d -print0)
	echo

	trdopt "Copying QuickLook Generators config files..."
	[ -d "$HOME/Library/QuickLook/QLColorCode.qlgenerator/Contents" ] && \
	cp -v "$DOTFILES/unix/home/Library/QuickLook/QLColorCode.ql/Info.plist" \
	"$HOME"/Library/QuickLook/QLColorCode.qlgenerator/Contents/Info.plist

	[ -d "$HOME/Library/QuickLook/QLStephen.qlgenerator/Contents" ] && \
	cp -v "$DOTFILES/unix/home/Library/QuickLook/QLStephen.ql/Info.plist" \
	"$HOME"/Library/QuickLook/QLStephen.qlgenerator/Contents/Info.plist

	echo
}

setup_library_automator() \
{
	trdopt "Copying the custom QuickActions and Services from iCloud Drive..."

	[ ! -d "/Library/Services" ] && sudo mkdir -pv /Library/Services
	while IFS=$'\n' read -r -d '' workf;
	do
		# echo "$workf" # "$HOME/Library/Mobile Documents/com~apple~Automator/Documents/<dir>"
		workfPath="$(echo "$workf" | \
			awk -v a="$HOME/Library/Mobile Documents/com~apple~Automator/Documents//" \
			'{len=length(a)}{print substr($0,len)}')"
		# echo "$workfPath" # "<folder>"

		if [ ! -e "/Library/Services/$workfPath" ];
		then
			sudo cp -Rv "$workf" "/Library/Services/$workfPath"
			sudo chown -Rv root:staff "/Library/Services/$workfPath"
		fi
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~Automator/Documents" \
		-maxdepth 1 -name "*.workflow" -type d -print0)
	echo
}

setup_library_sounds() \
{
	trdopt "Copying Sounds audio files from iCloud Drive..."

	[ ! -d "/Library/Sounds" ] && sudo mkdir -pv /Library/Sounds
	while IFS= read -r -d '' snds;
	do
		case "$snds" in
			*".icloud") continue ;;
			*"._"*) continue ;;
		esac

		if [ ! -e "/Library/Sounds/$(basename "$snds")" ];
		then
			sudo cp -v "$snds" "/Library/Sounds/"
			sudo chown -Rv root:staff "/Library/Sounds/$(basename "$snds")"
		fi
	done \
	< <(find "$HOME/Library/Mobile Documents/com~apple~QuickTimePlayerX/Documents/Alerts" \
		-maxdepth 1 -type f -print0)
	echo
}

####################################################################################################




#################################### WALLPAPERS, LYRICS & FONTS ####################################

setup_wallpapers() \
{
	option "Wallpapers: R-Syncing your favorite wallpapers"

	info "If your wallpapers are on an external drive, halt this command," \
	"plug the drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"

	local firstDir
	local secondDir

	echo; subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) you will sync the wallpapers from: "
	read -r firstDir

	[ -z "$firstDir" ] && warn "Skipping this step!" && return

	echo; subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) where the wallpapers will go: "
	read -r secondDir

	[ -z "$secondDir" ] && warn "R-Syncing wallpapers halted" && return

	## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
	## H:hardlinks as hardls; h:human readable; P:--progress & --partial
	## v:verbose; z:compress during transfer
	echo; rsync -aHhPvz --exclude=".*" "$firstDir/" $secondDir

	echo; success "Wallpapers successfully rsynced!"
	echo
}

setup_lyrics() \
{
	option "Lyrics: R-Syncing your Lyrics files (.lrc) folder content"

	targetLyricsDir=""
	case "$(uname -s)" \
	in
		"Darwin") targetLyricsDir="/Users/Shared/LyricsX" ;;
		"Linux") targetLyricsDir="/usr/share/lyrics/" ;;
		*)
			error "$(uname -s) not supported! Skipping..."
			echo; return 126
		;;
	esac

	sourceLyricsDir=""

	info "If your lyrics are on an external drive, halt this command, plug the" \
	"drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"
	echo
	subopt "Write the Full Path of the Directory (without final"
	subopt "trailing slash) you will sync the lyrics from: "
	read -r lyricsDir

	[ -z "$sourceLyricsDir" ] && warn "R-Syncing lyrics halted" && return

	if [ -d "$sourceLyricsDir" ];
	then
		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		echo; rsync -aHhPvz --exclude=".*" "$sourceLyricsDir/" "$targetLyricsDir"
	else
		error "Source directory not found! Aborting..."
		echo; exit 126
	fi

	echo; success "Lyrics successfully rsynced!"
	echo
}

setup_fonts() \
{
	option "Fonts: Syncing your .ttf's, .otf's and zip files"

	local localDir
	case "$(uname -s)" \
	in
		"Darwin") localDir="$HOME/Library/Fonts/" ;;
		"Linux") localDir="/usr/share/fonts/" ;;
		*)
			error "$(uname -s) not supported! Skipping..."
			echo; return 126
		;;
	esac

	info "If your fonts are on an external drive, halt this command, plug the" \
	"drive in, open a new terminal and rerun this script"
	warn "If you want to halt this setup, just type Enter to exit"
	echo
	subopt "Write the Full Path of the Directory (without final"
	suboptq "trailing slash) you will copy the fonts from: "
	read -r fontsDir

	if [ -z "$fontsDir" ];
	then
		warn "Copying fonts halted"
		echo; exit 126
	fi

	if [ -d "$fontsDir" ];
	then
		echo
		# while IFS= read -r -d '' file;
		# do
		# 	cp -v "$file" "$localDir"
		# done \
		# < <(find "$fontsDir" -name "*.ttf" -type f -print0)
		# echo

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		rsync -aHhPvz --include="*.ttf" "$fontsDir/" "$localDir"

		# while IFS= read -r -d '' file;
		# do
		# 	cp -v "$file" "$localDir"
		# 	chmod -v 644 "$file"
		# done \
		# < <(find "$fontsDir" -name "*.otf" -type f -print0)
		# echo

		## a:recursive,l:symls as symls,perms,t:modtime,groups,owner,D:devices&specials
		## H:hardlinks as hardls; h:human readable; P:--progress & --partial
		## v:verbose; z:compress during transfer
		rsync -aHhPvz --include="*.otf" "$fontsDir/" "$localDir"

		while IFS= read -r -d '' file;
		do
			cp -v "$file" "$localDir"
			chmod -v 644 "$file"

			if command -v minizip > /dev/null 2>&1;
			then
				minizip extract "$localDir/$file"
				rm "$localDir/$file"

			elif command -v minizip > /dev/null 2>&1;
			then
				unzip "$localDir/$file"
				rm "$localDir/$file"
			fi
		done \
		< <(find "$fontsDir" -name "*.zip" -type f -print0)
		echo

		success "Fonts successfully copied!"
	else
		error "Directory not found! Aborting..."
		echo; exit 126
	fi
	echo
}

####################################################################################################




######################################### REMOVE DOTFILES ##########################################

removeFiles() \
{
	subopt "Unlinking at $HOME"
	if_bin_exists trash || if_bin_exists macos-trash

	while IFS= read -r -d '' file;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$file"
			;;
			*)
				trash -v "$file"
			;;
		esac
	done \
	< <(fd . "$HOME" -d 1 -t l -0)
	echo;
}

removeBins() \
{
	subopt "Unlinking at $HOME/.local/bin"
	if_bin_exists trash || if_bin_exists macos-trash

	while IFS= read -r -d '' binary;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$binary"
			;;
			*)
				trash -v "$binary"
			;;
		esac
	done \
	< <(fd . "$HOME"/.local/bin -t l -0)
	echo
}

removeFolders() \
{
	subopt "Unlinking at $HOME/.config and $HOME/.local"
	if_bin_exists trash || if_bin_exists macos-trash

	while IFS= read -r -d '' configFolder;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$configFolder"
			;;
			*)
				trash -v "$configFolder"
			;;
		esac
	done \
	< <(fd . "$HOME"/.config -t l -0)

	while IFS= read -r -d '' localFolder;
	do
		case "$(uname -s)" \
		in
			"Darwin")
				trash -v "$localFolder"
			;;
			*)
				trash -v "$localFolder"
			;;
		esac
	done \
	< <(fd . "$HOME"/.local -t l -0)
	echo
}

removeTextEditors() \
{
	option "Text Editors: Unlinking existing settings"
	if_bin_exists trash || if_bin_exists macos-trash

	case "$(uname -s)" \
	in
		"Darwin")
			appSTLocation="$HOME/Library/Application Support/Sublime Text/Packages/"
			appSMLocation="$HOME/Library/Application Support/Sublime Merge/Packages/"
			appVSCLocation="$HOME/Library/Application Support/VSCodium/User"
		;;
		"Linux")
			appSTLocation="$HOME/.config/sublime-text/Packages/"
			appSMLocation="$HOME/.config/sublime-merge/Packages/"
			appVSCLocation="$HOME/.config/VSCodium/User"
		;;
	esac

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appSTLocation" -maxdepth 3 -type l -print0)

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appSMLocation" -type l -print0)

	while IFS= read -r -d '' file;
	do
		trash -v "$file"
	done \
	< <(find "$appVSCLocation" -maxdepth 3 -type l -print0)

	echo
}

####################################################################################################



# ==============================================================================

usage_sync() \
{
echo
echo "@dievilz' Dotfiles Syncing & Management Script"
echo "Main script for dotfiles symlinking & rsyncing."
echo "Influenced by @mavam's and @ajmalsiddiqui's sync script"
echo
printf "SYNOPSIS: ./%s [hard,syml,rsync] [-b][-d][-f][-h][-l][-p][-r][-t][-w] \n" "$(basename "$0")"
printf "./%s [hard,syml,rsync] [--dotfiles [dirs|bins|etc|...]] \n" "$(basename "$0")"
printf "./%s [hard,syml,rsync] [--text-editors [stext-full|smerge|...]] \n" "$(basename "$0")"
printf "./%s [hard,syml,rsync] [--preferences|--library|--wallpapers|...] \n" "$(basename "$0")"
printf "./%s [--remove [files|bins|folders|text-editors]] \n" "$(basename "$0")"
cat <<-'EOF'

ENVIRONMENTAL VARIABLES:
    DOTFILES_FORCE      values: [0/1] - Enable force flag for syncing modes
    DOTFILES_SYNC_MODE  values: [hard,syml,rsync] - If you want to always use a syncing mode, set it with this env var

POSITIONAL ARGUMENTS (SYNCING MODE):
    hard                Sync files via hardlinking       $(ln)
    syml                Sync files via sym(bolic)linking $(ln -s)
    cp, copy            Sync files via copying           $(cp)
    rsync               Sync files via rsync'ing         $(rsync)

If the user only specifies the syncing mode, the script is going to enter in
loop mode, allowing the user to continue to use the script without exiting it
and re-entering new arguments.

OPTIONS:
    -d,--dotfiles       Sync main Dotfiles to various locations with:
       rootdirs            Makedir essential dirs in superuser space
       xdgdirs             Makedir XDG Standard dirs inside $HOME
       homedirs            Makedir essential dirs inside $HOME
       customhomedirs      Makedir custom dirs inside $HOME
       bin                 Copy binaries to $XDG_BIN_HOME, /usr/local/bin, etc.
       etc                 Sync system-wide config files to /etc
       pkg-etc             Sync 3rd-party packages' config files to their /etc/<folders>
       home                Sync main files to $HOME
       config              Sync custom config files to $XDG_CONFIG_HOME
       shell               Sync <Shell> init files to $XDG_CONFIG_HOME/<shells>
       data                Sync custom share files to $XDG_DATA_HOME
       homeopt             Sync custom 3rd-party files to $HOME/.opt
       shared              Sync 3rd-party files to a $SHARED_HOME/<folders>

    -t,--text-editors   Configure Text Editors/Dev Apps settings with:
       stext-basic         Open S-Text to install Package Control by script
       stext-full          Sync S-Text full-featured files, Once Package Control is installed
       smerge              Sync S-Merge full-featured files for a complete git usage
       vscodium            Sync VSCodium full-featured files for a complete dev usage

    -p,--preferences    Sync (copy) preferences files for apps (like Spotify, iTerm2, etc)
    -b,--library        Sync macOS Library files (Preferences, Services, Containers, etc).
    -w,--wallpapers     Sync Wallpapers from any external drive/local folder
    -l,--lyrics         Sync Lyrics from any external drive/local folder
    -f,--fonts          Sync any Fonts files from any external drive/local folder

    -r,--remove         Delete all the symlinks deployed by this script, or:
       files               Remove only symlinked files in $HOME
       bins                Remove only symlinked dotfiles binaries in $XDG_BIN_HOME
       folders             Remove only symlinked files in $XDG_CONFIG_HOME
       text-editors        Remove only symlinked text editors settings

    -h,--help           Show this menu

Note: Execute '--text-editors <app>' when you have finished the Fresh Install
Bootstrapping, and you have synced all your plugins on the text editors.

For full documentation, see: https://github.com/dievilz/punto.sh#readme

"You can't trust code that you did not totally
 create yourself."
                                - Ken Thompson

"Code is like a joke. If you have to explain it,
 then it's bad."
            - Coworker whose code was not a joke

EOF
}

menu_sync() \
{
	case "$1" in
		"-h"|"--help")
			usage_sync
			exit 0
		;;
		"-d"|"--dotfiles")
			case "$2" in
				"rootdirs")
					echo; syncRootDirs
				;;
				"xdgdirs")
					echo; syncXDGDirs
				;;
				"homedirs")
					echo; syncHomeDirs
				;;
				"customhomedirs")
					echo; syncCustomHomeDirs
				;;
				"bin")
					echo; syncBin
				;;
				"etc")
					echo; syncEtc
				;;
				"pkg-etc")
					echo; syncPkgEtc
				;;
				"home")
					echo; syncHome
				;;
				"config")
					echo; syncConfig
				;;
				"shell")
					echo; syncShell
				;;
				"data")
					echo; syncData
				;;
				"homeopt")
					echo; syncHomeOpt
				;;
				"shared")
					echo; syncShared
				;;
				*)
					error "Unknown option-argument for --dotfiles! Use -h/--help"
					return 127
				;;
			esac
		;;
		"-t"|"--text-editors")
			case "$2" in
				"stext-basic")
					echo; textedSublimeText basic
				;;
				"smerge")
					echo; textedSublimeMerge
				;;
				"vscodium")
					echo; textedVSCodium
				;;
				"stext-full")
					echo; textedSublimeText full
				;;
				*)
					error "Unknown option-argument for --text-editors! Use -h/--help"
					return 127
				;;
			esac
		;;
		"-p"|"--preferences")
			echo; setup_preferences
		;;
		"-b"|"--library")
			echo; setup_library_macos
		;;
		"-w"|"--wallpapers")
			echo; setup_wallpapers
		;;
		"-l"|"--lyrics")
			echo; setup_lyrics
		;;
		"-f"|"--fonts")
			echo; setup_fonts
		;;
		"-r"|"--remove")
			case "$2" in
				"files")
					echo; removeFiles
				;;
				"bins")
					echo; removeBins
				;;
				"folders")
					echo; removeFolders
				;;
				"text-editors")
					# echo; removeTextEditors
				;;
				""|"all")
					echo; red "3, 2, 1... Dropping the nukes! "

					echo
					removeFiles
					removeBins
					removeFolders
					# removeTextEditors

					success "Mission accomplished! Bravo Six, going bark..."
					echo
				;;
				*)
					error "Unknown option-argument for --remove! Use -h/--help"
					return 127
				;;
			esac
		;;
		*)
			error "Unknown option! Use -h/--help"
			return 127
		;;
	esac
}

main_case_sync() \
{
	option "We are going to install a concept one by one."
	until false;
	do
		echo
		subopt "Below are the options:"
		usage_sync

		subopt "Choose one option from the usage..."
		subopt "[${BO}'skip'${NBO}|${BO}'none'${NBO}][press ${BO}Enter key${NBO}] to halt"
		suboptq "" && read -r caseSyncOption

		case "$caseSyncOption" in
			"none"|"None"|"skip"|"Skip"|"")
				warn "Skipping this step!"
				return 1
				break
			;;
			*) menu_sync $caseSyncOption
			;;
		esac
	done
}

main_sync() \
{
	if [ "$#" -eq 0 ];
	then
		main_case_sync
	else
		menu_sync "$@"
	fi
}

### Exporting DOTFILES path and source helper functions so the script can work
unset DFILES ## unset is POSIX
if [ -e "$HOME/.dotfiles/sync.sh" ]; then
	DFILES="$HOME/.dotfiles"
else
	if command -v realpath > /dev/null 2>&1;
	then
		printf "%b" "\n\033[38;32mrealpath found!\033[0m\n"
	else
		printf "%b" "\n\033[38;31mError: realpath not found! Aborting...\033[0m\n"
		echo; exit 126
	fi
fi

export DOTFILES=${DFILES:-"$(realpath "$0" | grep -Eo '^.*?dotfiles')"} ## -o: only matching

if [ -e "$HOME/.helpers" ];
then
	. "$HOME/.helpers" && printf "%b" "\n\033[38;32mHelper script found!\033[0m\n"
else
	ln -fsv "$DOTFILES/unix/home/helpers" "$HOME/.helpers";
	if [ -r "$HOME/.helpers" ];
	then
		. "$HOME/.helpers" && printf "%b" "\n\033[38;32mHelper script found!\033[0m\n"
	else
		printf "%b" "\n\033[38;31mError: helper functions not found! Aborting...\033[0m\n"
		echo; exit 1
	fi
fi

case $(uname -s) in
	Darwin) SHARED_HOME="/Users/Shared"
	;;
	*) SHARED_HOME="/home/shared"
	;;
esac

case "$1" \
in
	"hard"|"syml"|"cp"|"copy"|"rsync")
		export DOTFILES_SYNC_MODE="$1"
		shift
	;;
	"-h"|"help"|"--help"|"-r"|"--remove")
		true
	;;
	*)
		if [ -n "$DOTFILES_SYNC_MODE" ];
		then
			case "$DOTFILES_SYNC_MODE" \
			in
				"hard"|"syml"|"rsync")
					true
				;;
				*)
					error "Invalid option set in \$DOTFILES_SYNC_MODE!" \
					"The value of the variables must be the type of syncing" \
					"you want to use to sync your dotfiles: hard links," \
					"symbolic links (symlinks), or rsync. Use -h/--help."
					echo
					exit 127
				;;
			esac
		else
			error "Invalid option!" \
			"The first argument must be the type of syncing you want to use" \
			"to sync your dotfiles: hard links, symbolic links (symlinks)," \
			"or rsync. Use -h/--help."
			echo
			exit 127
		fi
	;;
esac

main_sync "$@"; exit
